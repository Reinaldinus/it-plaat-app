import React from "react";
import Cell from "./Components/Cell";
import "./OperationDomain.module.css";

// tr = Server
// th = Businessdomain

export default function OperationDomain(props) {
  console.log(props);
  const serversElements = props.servers.map((server) => {
    return (
      <tr key={server.id}>
        <th>{server.name}</th>
        {props.businessDomains.map((businessDomain) => {
          return (
            <Cell
              key={businessDomain.id}
              businessDomainId={businessDomain.id}
              serverId={server.id}
              servers={props.servers}
              businessDomains={props.businessDomains}
              product_owners={props.product_owners}
              beheerder={props.beheerder}
              owners={props.owners}
              sLAs={props.sLAs}
              toggleArray={props.toggleArray}
              toggleArraySLA={props.toggleArraySLA}
              toggleArrayOwner={props.toggleArrayOwner}
              toggleArrayBeheerder={props.toggleArrayBeheerder}
              beheerderRelationInfo={props.beheerderRelationInfo}
              allApps={props.allApps}
            />
          );
        })}
      </tr>
    );
  });

  const domainElements = props.businessDomains.map((businessDomain) => {
    return <th key={businessDomain.id}>{businessDomain.name}</th>;
  });

  return (
    <div className="operationalDomain">
      <table className="table">
        <thead>
          <tr>
            <th></th>
            {domainElements}
          </tr>
        </thead>
        <tbody>
          {/* tr, th within, void td's with server key */}
          {serversElements}
        </tbody>
      </table>
    </div>
  );
}
