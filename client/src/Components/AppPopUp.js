import React, { useEffect, useState } from "react";
import "./Popup.css";
import InputUpdatePopUp from "./InputUpdatePopUp";
import Popup from "reactjs-popup";
import "reactjs-popup/dist/index.css";
import { nanoid } from "nanoid";
import ToDoPopUp from "./ToDoPopUp/ToDoPopUp";
import AppInfo from "./AppInfo";

export default function AppPopUp(props) {
  const [updateMode, setUpdateMode] = useState(false);
  const [toDoMode, setToDoMode] = useState(false);
  const token = sessionStorage.getItem("token");

  const [thereIsAnExpiredToDo, setThereIsAnExpiredToDo] = useState(false);

  useEffect(() => {
    for (let i = 0; i < props.data.to_dos.length; i++) {
      if (
        Date.parse(props.data.to_dos[i].deadline) - Date.parse(new Date()) <
        0
      ) {
        setThereIsAnExpiredToDo(true);
        return;
      }
    }
    setThereIsAnExpiredToDo(false);
  }, [props.data]);

  const visualBeheerders = props.data.beheerders.map((beheerder, index) => (
    <span key={nanoid()}>
      {beheerder.name}
      {index < props.data.beheerders.length - 1 ? ", " : ""}
    </span>
  ));

  function handleUpdateButton() {
    setUpdateMode((prevValue) => !prevValue);
  }
  function handleInfoButton() {
    setToDoMode(false);
  }
  function handleToDoButton() {
    setToDoMode(true);
  }
  function deleteApp() {
    fetch("http://localhost:8080/api/v1/application/" + props.data.id, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }).then((res) => console.log(res.status));
    window.location.reload(false);
  }

  function updateStateModus() {
    setUpdateMode(false);
  }

  return (
    <div className="popup-container">
      {!updateMode && !toDoMode && (
        <div className="popup-container">
          <div className="popup-tabs-header">
            <div className="popup-tab" onClick={handleInfoButton}>
              Info
            </div>
            <div className="popup-tab" onClick={handleToDoButton}>
              Actions
              {thereIsAnExpiredToDo && (
                <div className="todo-expired-exclamation">!</div>
              )}
            </div>
          </div>
          <div className="popup-header">
            <img src={require(`../assets/${props.data.logo}`)} />
            <h1>{props.data.app_name}</h1>
          </div>

          <p className="app-description">{props.data.app_description}</p>
          <AppInfo
            title="Product Owner:"
            data={props.data.product_owner.name}
          />
          <AppInfo title="Beheerder:" data={visualBeheerders} />
          <AppInfo title="Owner:" data={props.data.owner.name} />
          <AppInfo title="IP Address:" data={props.data.ip_address} />
          <AppInfo title="Link:" data={props.data.link} />
          <AppInfo title="SLA:" data={props.data.sla.name} />
          <AppInfo title="Comments:" data={props.data.comments} />
          <AppInfo
            title="Documentation:"
            data={props.data.link_to_documentation}
          />

          <div className="popup-buttons">
            {/* //TODO: link this to the PUT call of the API */}
            <button onClick={handleUpdateButton}>Update</button>

            <button onClick={deleteApp}>Delete</button>
          </div>
        </div>
      )}

      {toDoMode && (
        <ToDoPopUp
          handleInfoButton={handleInfoButton}
          handleToDoButton={handleToDoButton}
          logo={props.data.logo}
          app_name={props.data.app_name}
          app_id={props.data.id}
          appBeherders={props.data.beheerders}
          beheerders={props.beheerder}
        />
      )}

      {updateMode && (
        <InputUpdatePopUp
          popUpState={"update"}
          method={"put"}
          data={props.data}
          servers={props.servers}
          businessDomains={props.businessDomains}
          product_owners={props.product_owners}
          beheerders={props.beheerder}
          sLAs={props.sLAs}
          owners={props.owners}
          handleUpdateButton={handleUpdateButton}
          updateAppState={props.updateAppState}
          updateStateModus={updateStateModus}
        />
      )}
    </div>
  );
}
