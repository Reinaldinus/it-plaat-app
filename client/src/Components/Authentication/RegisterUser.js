import { useState, useEffect } from "react";
import "./Login.css";

const RegisterUser = (props) => {
  const [usernameRegister, setUsernameRegister] = useState("");
  const [emailRegister, setEmailRegister] = useState("");
  const [passwordRegister, setPasswordRegister] = useState("");
  const [registering, setRegistering] = useState(false);
  const [registerActionDone, setRegisterActionDone] = useState(false);
  const [message, setMessage] = useState("");

  const handleUsernameInput = (event) => {
    setUsernameRegister(event.target.value);
  };

  const handleEmailInput = (event) => {
    setEmailRegister(event.target.value);
  };

  const handlePasswordInput = (event) => {
    setPasswordRegister(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setRegisterActionDone(false);
    setRegistering(true);
  };

  const usernameEmailAndPassword = {
    username: usernameRegister,
    email: emailRegister,
    password: passwordRegister,
  };

  const options = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(usernameEmailAndPassword),
  };

  useEffect(() => {
    if (registering) {
      fetch("http://localhost:8080/api/auth/register", options)
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          if (data.token) {
            setRegistering(false);
            setUsernameRegister("");
            setPasswordRegister("");
            setEmailRegister("");
            setMessage("Successfully registered");
            setRegisterActionDone(true);
          } else if (data.message) {
            setRegisterActionDone(true);
            setMessage(data.message);
            setRegistering(false);
          }
        });
    }
  }, [registering]);

  return (
    <>
      <section className="login-container">
        <h1>Register user</h1>

        <form className="login-form-register" onSubmit={handleSubmit}>
          <div className="form-group">
            <label htmlFor="email">Username</label>
            <input
              type="text"
              id="username"
              autoComplete="off"
              onChange={handleUsernameInput}
              value={usernameRegister}
              required
            />
          </div>

          <div className="form-group">
            <label htmlFor="email">Email</label>
            <input
              type="text"
              id="email"
              autoComplete="off"
              onChange={handleEmailInput}
              value={emailRegister}
              required
            />
          </div>

          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              id="password"
              onChange={handlePasswordInput}
              value={passwordRegister}
              required
            />
          </div>

          <div className="form-group">
            {registering ? (
              <p className="register-button">Registering...</p>
            ) : (
              <button className="register-button">Register</button>
            )}
          </div>
          <u onClick={props.handleRegister} className="login-register">
            Return to login screen
          </u>
        </form>
        {registerActionDone && <p className="error-message">{message}</p>}
      </section>
    </>
  );
};

export default RegisterUser;
