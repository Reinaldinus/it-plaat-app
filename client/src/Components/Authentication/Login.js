import { useState, useEffect } from "react";
import RegisterUser from "./RegisterUser";
import "../Popup.css";
import "./Login.css";

const Login = (props) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [registerMode, setRegistermode] = useState(false);
  const [loggingIn, setLoggingIn] = useState(false);
  const [loginFailed, setLoginFailed] = useState(false);
  const token = sessionStorage.getItem("token");

  const handleEmailInput = (event) => {
    setEmail(event.target.value);
  };
  const handlePasswordInput = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setLoginFailed(false);
    setLoggingIn(true);
  };

  const emailAndPassword = {
    email: email,
    password: password,
  };

  const options = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(emailAndPassword),
  };

  useEffect(() => {
    if (loggingIn) {
      fetch("http://localhost:8080/api/auth/login", options)
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          if (data.token) {
            sessionStorage.setItem("token", data.token);
            setLoggingIn(false);
            props.handleLogin();
          } else {
            setLoggingIn(false);
            setLoginFailed(true);
          }
        });
    }
  }, [loggingIn]);

  const handleRegister = () => {
    setRegistermode(!registerMode);
  };

  return (
    <>
      {!token && !registerMode && (
        <section className="login-container">
          <>
            <h1>Enter email and password to login</h1>

            <form className="login-form" onSubmit={handleSubmit}>
              <label htmlFor="username">Email</label>
              <input
                type="text"
                id="email"
                autoComplete="off"
                onChange={handleEmailInput}
                value={email}
                required
              />

              <label htmlFor="password">Password</label>
              <input
                type="password"
                id="password"
                onChange={handlePasswordInput}
                value={password}
                required
              />

              <div className="login-button-container">
                <button>Login</button>
                {loggingIn && <p>Attempting to log in...</p>}
                {loginFailed && <p>Your login attempt was unsuccessful</p>}
                <u onClick={handleRegister} className="login-register">
                  Register new user
                </u>
              </div>
            </form>
          </>
        </section>
      )}
      {!token && registerMode && (
        <RegisterUser handleRegister={handleRegister} />
      )}
    </>
  );
};

export default Login;
