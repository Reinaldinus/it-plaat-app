import React, { useEffect, useState } from "react";
import Popup from "reactjs-popup";
import "reactjs-popup/dist/index.css";
import AppPopUp from "./AppPopUp";
import styles from "./Cell.module.css";

export default function Cell(props) {
  const [apps, setApps] = useState([]);

  useEffect(() => {
    //We can maybe render the apps directly from props.allApps, but then we need to refactor update popup for autoververs
    setApps(
      props.allApps.filter(
        (app) =>
          app.business_domain.id === props.businessDomainId &&
          app.server.id === props.serverId
      )
    );
  }, [props.allApps, props.businessDomainId, props.serverId]);

  function updateAppState(
    appId,
    app_name,
    product_owner_id,
    logo,
    app_description,
    sla_id,
    ip_address,
    server_id,
    business_domain_id,
    link_to_code,
    comments,
    link,
    owner_id,
    link_to_documentation,
    beheerders
  ) {
    console.log(app_name);
    setApps((prevValue) =>
      prevValue.map((app) => {
        console.log("we do this");
        return app.id === appId
          ? {
              id: appId,
              app_name: app_name,
              product_owner: props.product_owners.filter(
                (po) => po.id === Number(product_owner_id)
              )[0],
              logo: logo,
              app_description: app_description,
              sla: props.sLAs.filter((sla) => sla.id === Number(sla_id))[0],
              ip_address: ip_address,
              server: props.servers.filter(
                (server) => server.id === Number(server_id)[0]
              ),
              business_domain: props.businessDomains.filter(
                (bd) => bd.id === Number(business_domain_id)
              )[0],
              link_to_code: link_to_code,
              comments: comments,
              link: link,
              owner: props.owners.filter(
                (owner) => owner.id === Number(owner_id)
              )[0],
              link_to_documentation: link_to_documentation,
              beheerders: beheerders,
              to_dos: app.to_dos,
            }
          : app;
      })
    );
  }

  let appsToBeFilteredPO = props.toggleArray.map((po) => {
    if (po.toggled === false) {
      return po.id;
    } else return "";
  });

  let appsToBeFilteredSLA = props.toggleArraySLA.map((sla) => {
    if (sla.toggled === false) {
      return sla.id;
    } else return "";
  });

  let appsToBeFilteredOwner = props.toggleArrayOwner.map((owner) => {
    if (owner.toggled === false) {
      return owner.id;
    } else return "";
  });

  let appsToBeFilteredBeheerder = props.toggleArrayBeheerder.map((beh) => {
    if (beh.toggled === false) {
      return beh.id;
    } else return "";
  });

  const appElements = apps.map((app) => {
    let isNotInAppsToBeFilteredPO = !appsToBeFilteredPO.includes(
      app.product_owner.id
    );
    let isNotInAppsToBeFilteredSLA = !appsToBeFilteredSLA.includes(app.sla.id);
    let isNotInAppsToBeFilteredOwner = !appsToBeFilteredOwner.includes(
      app.owner.id
    );

    let beheerderIdArray = app.beheerders.map((beheerder) => beheerder.id);
    beheerderIdArray = beheerderIdArray.filter((id) => id !== undefined);

    let isNotInAppsToBeFilteredBeheerder = beheerderIdArray.map((behId) => {
      return !appsToBeFilteredBeheerder.includes(behId);
    });
    isNotInAppsToBeFilteredBeheerder =
      isNotInAppsToBeFilteredBeheerder.includes(true);

    if (
      isNotInAppsToBeFilteredPO ||
      isNotInAppsToBeFilteredSLA ||
      isNotInAppsToBeFilteredOwner ||
      isNotInAppsToBeFilteredBeheerder
    ) {
      return (
        <Popup
          key={app.id}
          trigger={
            <div className={styles["cellAppElement"]}>
              <img src={require(`../assets/${app.logo}`)} alt="" />
              <h4>{app.app_name}</h4>
              <p>{app.product_owner.name}</p>
            </div>
          }
          modal
        >
          <AppPopUp
            data={app}
            servers={props.servers}
            businessDomains={props.businessDomains}
            product_owners={props.product_owners}
            beheerder={props.beheerder}
            owners={props.owners}
            sLAs={props.sLAs}
            updateAppState={updateAppState}
          />
        </Popup>
      );
    } else if (isNotInAppsToBeFilteredPO || isNotInAppsToBeFilteredSLA) {
      return (
        <Popup
          key={app.id}
          trigger={
            <div>
              <img src={require(`../assets/${app.logo}`)} />
              <div>{app.app_name}</div>
              <div>{app.product_owner.name}</div>
            </div>
          }
          modal
        >
          <AppPopUp
            data={app}
            servers={props.servers}
            businessDomains={props.businessDomains}
            product_owners={props.product_owners}
            beheerder={props.beheerder}
            owners={props.owners}
            sLAs={props.sLAs}
            updateAppState={updateAppState}
          />
        </Popup>
      );
    }
  });

  return (
    <td>
      <div className={styles["cell-container"]}>{appElements}</div>
    </td>
  );
}
