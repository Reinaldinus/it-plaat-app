const InputFieldText = ({ label, onChange, name, value }) => (
  <div className="input-field">
    <label>{label}</label>
    <input
      type="text"
      placeholder={label}
      onChange={onChange}
      name={name}
      value={value}
    />
  </div>
);

export default InputFieldText;
