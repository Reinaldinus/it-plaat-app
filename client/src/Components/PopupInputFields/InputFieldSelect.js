const InputFieldSelect = ({ label, onChange, name, dropdownItems, value }) => (
  <div className="input-field">
    <label>{label}</label>
    <select name={name} id="selectList" onChange={onChange} value={value}>
      <option>--Choose--</option>
      {dropdownItems}
    </select>
  </div>
);

export default InputFieldSelect;
