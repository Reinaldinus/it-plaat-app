function returnAllButtonsWithToggledTrue(toggleArrays, updatedArrays) {
  for (let prop in toggleArrays) {
    updatedArrays[prop] = toggleArrays[prop].map((item) => {
      return { ...item, toggled: true };
    });
  }
}

function returnAllButtonsWithToggledFalseExceptForButtonClicked(
  toggleArrays,
  updatedArrays,
  type,
  id
) {
  for (let prop in toggleArrays) {
    if (prop === type) {
      updatedArrays[type] = toggleArrays[type].map((item) => {
        if (item.id === id) {
          return { ...item };
        } else return { ...item, toggled: false };
      });
    } else {
      updatedArrays[prop] = toggleArrays[prop].map((item) => {
        return { ...item, toggled: false };
      });
    }
  }
}

function returnButtonWithOpositeToggledValue(
  toggleArrays,
  updatedArrays,
  type,
  id
) {
  for (let prop in toggleArrays) {
    updatedArrays[prop] = toggleArrays[prop].map((item) => {
      if (prop === type) {
        if (item.id === id) {
          return { ...item, toggled: !item.toggled };
        } else return { ...item };
      } else {
        return { ...item };
      }
    });
  }
}

export {
  returnAllButtonsWithToggledTrue,
  returnAllButtonsWithToggledFalseExceptForButtonClicked,
  returnButtonWithOpositeToggledValue,
};
