import React from "react";
import styles from "./FilterPopup.module.css";

export default function FilterButton(props) {


  let appsToBeFiltered = props.toggledInfo.map((po) => {
    if (po.toggled === false) {
      return po.id;
    } else return "";
  });

  // console.log(appsToBeFiltered)

  let style = styles["filterbutton-clicked"];

  if (appsToBeFiltered.includes(props.id)) {
    style = styles["filterbutton-unclicked"];
  }

  return (
    <button className={style} onClick={props.onClick}>
      {props.name}
    </button>
  );
}
