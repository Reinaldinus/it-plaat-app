import React from "react";
import styles from "./FilterPopup.module.css";
import FilterButton from "./FilterButton";
import FilterButtonContainer from "./FilterButtonContainer";
import filterHelpers from "./filterHelpers";
import { nanoid } from "nanoid";
import {
  returnAllButtonsWithToggledTrue,
  returnAllButtonsWithToggledFalseExceptForButtonClicked,
  returnButtonWithOpositeToggledValue,
} from "./filterHelpers";

export default function FilterPopup(props) {
  function toggleFilter(id, toggled, type) {
    let updatedArrayForPO;
    let updatedArrayForSLA;
    let updatedArrayForOwner;
    let updatedArrayForBeheerder;

    let toggleArrays = {
      PO: props.toggleArrayPO,
      SLA: props.toggleArraySLA,
      Owner: props.toggleArrayOwner,
      Beheerder: props.toggleArrayBeheerder,
    };

    let updatedArrays = {
      PO: updatedArrayForPO,
      SLA: updatedArrayForSLA,
      Owner: updatedArrayForOwner,
      Beheerder: updatedArrayForBeheerder,
    };

    // If only one button is toggled and the button clicked is the one toggled, return all the buttons with toggled true
    if (props.howManyToggled === 1 && toggled === true) {
      returnAllButtonsWithToggledTrue(toggleArrays, updatedArrays);

      setAllToggleArray(
        updatedArrays["Beheerder"],
        updatedArrays["PO"],
        updatedArrays["SLA"],
        updatedArrays["Owner"]
      );
      props.setOneFilterButtonIsClicked(false);

      // If there is no button clicked, return all buttons as false, accept for the button clicked. This button returns
      // the opposite of its last toggled state
    } else if (!props.oneFilterButtonIsClicked) {
      returnAllButtonsWithToggledFalseExceptForButtonClicked(
        toggleArrays,
        updatedArrays,
        type,
        id
      );

      setAllToggleArray(
        updatedArrays["Beheerder"],
        updatedArrays["PO"],
        updatedArrays["SLA"],
        updatedArrays["Owner"]
      );
      props.setOneFilterButtonIsClicked(true);

      // Lastly, if none of the above is the case, just return the opposite value of the button that is clicked
      // All other buttons return their previous value.
    } else {
      returnButtonWithOpositeToggledValue(
        toggleArrays,
        updatedArrays,
        type,
        id
      );

      setAllToggleArray(
        updatedArrays["Beheerder"],
        updatedArrays["PO"],
        updatedArrays["SLA"],
        updatedArrays["Owner"]
      );
    }
  }

  // And then set all the states
  function setAllToggleArray(
    updatedArrayForBeheerder,
    updatedArrayForPO,
    updatedArrayForSLA,
    updatedArrayForOwner
  ) {
    props.setToggleArrayBeheerder(updatedArrayForBeheerder);
    props.setToggleArrayPO(updatedArrayForPO);
    props.setToggleArraySLA(updatedArrayForSLA);
    props.setToggleArrayOwner(updatedArrayForOwner);
  }

  // Template for filterbuttons JSX
  function returnFilterButton(item, type, toggleArray) {
    return (
      <FilterButton
        name={type === "Beheerder" ? item.name : item.name}
        onClick={() => toggleFilter(item.id, item.toggled, type)}
        id={item.id}
        toggledInfo={toggleArray}
        key={nanoid()}
      />
    );
  }

  // Filterbutton beheerders code
  const filterButtonBeheerders = props.toggleArrayBeheerder.map((beh) => {
    const type = "Beheerder";

    return returnFilterButton(beh, type, props.toggleArrayBeheerder);
  });

  // Filterbutton owners code
  const filterButtonOwners = props.toggleArrayOwner.map((owner) => {
    const type = "Owner";

    return returnFilterButton(owner, type, props.toggleArrayOwner);
  });

  // Filterbuttons SLA code
  const filterButtonSLAs = props.toggleArraySLA.map((sla) => {
    const type = "SLA";

    return returnFilterButton(sla, type, props.toggleArraySLA);
  });

  // Filterbutton PO code
  const filterButtonProductOwners = props.toggleArrayPO.map((po) => {
    const type = "PO";

    return returnFilterButton(po, type, props.toggleArrayPO);
  });

  return props.trigger ? (
    <div className={styles["filter-popup"]}>
      <div className={styles["filter-popup-inner"]}>
        <button className={styles["close-button"]} onClick={props.close}>
          Close
        </button>

        <div className={styles["filterButtonsTotalContainer"]}>
          <FilterButtonContainer
            labelText="Product owners"
            filterButtons={filterButtonProductOwners}
          />

          <FilterButtonContainer
            labelText="Service level agreements"
            filterButtons={filterButtonSLAs}
          />

          <FilterButtonContainer
            labelText="Owners"
            filterButtons={filterButtonOwners}
          />

          <FilterButtonContainer
            labelText="Administrators"
            filterButtons={filterButtonBeheerders}
          />
        </div>
      </div>
    </div>
  ) : (
    ""
  );
}
