import styles from "./FilterPopup.module.css";

export default function FilterButtonContainer(props) {
  return (
    <div>
      <label>{props.labelText}</label>
      <div className={styles["filterBtns"]}>{props.filterButtons}</div>
    </div>
  );
}
