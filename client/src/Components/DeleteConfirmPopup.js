import React from 'react'
import styles from "./FilterPopup/FilterPopup.module.css";


export default function DeleteConfirmPopup(props) {
    return props.trigger ? (
        <div className={styles["filter-popup"]}>
          <div className={styles["filter-popup-inner"]}>
            <button className={styles["close-button"]} onClick={props.close}>
              Close
            </button>
            {props.children}
          </div>
        </div>
      ) : (
        ""
      );
  
}

