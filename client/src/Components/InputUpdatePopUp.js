import React, { useState } from "react";
import "./Popup.css";
import Select from "react-select";
import InputFieldText from "./PopupInputFields/InputFieldText";
import InputFieldSelect from "./PopupInputFields/InputFieldSelect";
import {
  makeLogoNameUnique,
  uploadImage,
  sendInfoToTheApi,
} from "./Util/helpers";

export default function InputUpdatePopUp(props) {
  const [invalidSubmit, setInvalidSubmit] = useState(false);
  const [imageChanged, setImageChanged] = useState(false);
  console.log(imageChanged);

  const [formData, setFormData] = useState(
    props.popUpState === "update"
      ? {
          app_name: props.data.app_name,
          product_owner_id: props.data.product_owner.id,
          logo: [{ name: props.data.logo }],
          app_description: props.data.app_description,
          sla_id: props.data.sla.id,
          ip_address: props.data.ip_address,
          server_id: props.data.server.id,
          business_domain_id: props.data.business_domain.id,
          link_to_code: props.data.link_to_code,
          comments: props.data.comments,
          link: props.data.link,
          owner_id: props.data.owner.id,
          link_to_documentation: props.data.link_to_documentation,
        }
      : {
          app_name: "",
          product_owner_id: "",
          logo: "",
          app_description: "",
          sla_id: "",
          ip_address: "",
          server_id: "",
          business_domain_id: "",
          link_to_code: "",
          comments: "",
          link: "",
          owner_id: "",
          link_to_documentation: "",
        }
  );

  //This is not a good practice. Saving props in state
  const [selectedBeheerders, setSelectedBeheerders] = useState(
    props.popUpState === "update"
      ? props.data.beheerders.map((beheerder) => ({
          value: beheerder.id,
          label: beheerder.name,
        }))
      : []
  );

  function handleChange(event) {
    setFormData((prevData) => {
      return {
        ...prevData,
        [event.target.name]: event.target.value,
      };
    });
  }
  function onImageChange(event) {
    setFormData((prevData) => {
      return {
        ...prevData,
        [event.target.name]: event.target.files,
      };
    });
    setImageChanged(true);
    console.log(imageChanged);
  }

  const optionsServers = props.servers.map((option) => (
    <option key={option.id} value={option.id}>
      {option.name}
    </option>
  ));

  const optionsBusinessDomains = props.businessDomains.map((option) => (
    <option key={option.id} value={option.id}>
      {option.name}
    </option>
  ));

  const optionProductOwners = props.product_owners.map((option) => (
    <option key={option.id} value={option.id}>
      {option.name}
    </option>
  ));

  const optionBeheerders = props.beheerders.map((option) => ({
    value: option.id,
    label: option.name,
  }));

  const optionOwners = props.owners.map((option) => (
    <option key={option.id} value={option.id}>
      {option.name}
    </option>
  ));

  const optionSLA = props.sLAs.map((option) => (
    <option key={option.id} value={option.id}>
      {option.name}
    </option>
  ));

  async function submit(event) {
    console.log(imageChanged);
    let newLogoName;
    if (props.popUpState === "update" && !imageChanged) {
      newLogoName = formData.logo[0];
    } else if (imageChanged) {
      if (formData.logo[0] != null) {
        newLogoName = makeLogoNameUnique(formData.logo[0]);
        uploadImage(event, newLogoName);
      }
    }
    console.log(newLogoName);
    if (
      formData.app_name !== "" &&
      // !props.appNames.includes(formData.app_name) &&
      formData.product_owner_id !== "" &&
      newLogoName.name !== "" &&
      formData.sla_id !== "" &&
      formData.ip_address !== "" &&
      formData.server_id !== "" &&
      formData.business_domain_id !== "" &&
      selectedBeheerders.length > 0 &&
      formData.link !== "" &&
      formData.owner_id !== ""
    ) {
      let id = await sendInfoToTheApi(
        props.method,
        props.data ? props.data.id : 0,
        formData.app_name,
        formData.product_owner_id,
        newLogoName?.name,
        formData.app_description,
        formData.sla_id,
        formData.ip_address,
        formData.server_id,
        formData.business_domain_id,
        formData.link_to_code,
        formData.comments,
        formData.link,
        formData.owner_id,
        formData.link_to_documentation,
        selectedBeheerders
      );
      if (props.method === "post") {
        setTimeout(() => {
          props.handleAppAddition(
            id,
            formData.app_name,
            formData.product_owner_id,
            newLogoName?.name,
            formData.app_description,
            formData.sla_id,
            formData.ip_address,
            formData.server_id,
            formData.business_domain_id,
            formData.link_to_code,
            formData.comments,
            formData.link,
            formData.owner_id,
            formData.link_to_documentation,
            selectedBeheerders
          );
        }, 1200);
        window.location.reload();
      } else {
        props.updateAppState(
          Number(props.data.id),
          formData.app_name,
          Number(formData.product_owner_id),
          newLogoName?.name,
          formData.app_description,
          Number(formData.sla_id),
          formData.ip_address,
          Number(formData.server_id),
          Number(formData.business_domain_id),
          formData.link_to_code,
          formData.comments,
          formData.link,
          Number(formData.owner_id),
          formData.link_to_documentation,
          selectedBeheerders
        );
        props.updateStateModus();
      }
    }
  }

  return (
    <div className="popup-container">
      <h1>{props.popUpState === "update" ? "Update App" : "Add an app"}</h1>

      {props.popUpState !== "update" && (
        <button className="return-button" onClick={props.return}>
          Return
        </button>
      )}

      <InputFieldText
        label={"App Name"}
        onChange={handleChange}
        name={"app_name"}
        value={formData.app_name}
      />

      <InputFieldSelect
        label={"Product Owner"}
        onChange={handleChange}
        name={"product_owner_id"}
        dropdownItems={optionProductOwners}
        value={formData.product_owner_id}
      />

      <InputFieldText
        label={"App Description"}
        onChange={handleChange}
        name={"app_description"}
        value={formData.app_description}
      />

      <InputFieldText
        label={"IP Address"}
        onChange={handleChange}
        name={"ip_address"}
        value={formData.ip_address}
      />

      <InputFieldText
        label={"Link"}
        onChange={handleChange}
        name={"link"}
        value={formData.link}
      />

      <InputFieldText
        label={"Link to code"}
        onChange={handleChange}
        name={"link_to_code"}
        value={formData.link_to_code}
      />

      <InputFieldText
        label={"Link to documentation"}
        onChange={handleChange}
        name={"link_to_documentation"}
        value={formData.link_to_documentation}
      />

      <InputFieldText
        label={"Comments"}
        onChange={handleChange}
        name={"comments"}
        value={formData.comments}
      />

      <div className="input-field">
        {props.popUpState === "update" && (
          <img src={require(`../assets/${props.data.logo}`)} />
        )}
        <label>Change the logo</label>
        <input
          type="file"
          multiple
          accept="image/*"
          name="logo"
          onChange={onImageChange}
        />
      </div>

      <InputFieldSelect
        label={"Server"}
        onChange={handleChange}
        name={"server_id"}
        dropdownItems={optionsServers}
        value={formData.server_id}
      />

      <InputFieldSelect
        label={"Business Domain"}
        onChange={handleChange}
        name={"business_domain_id"}
        dropdownItems={optionsBusinessDomains}
        value={formData.business_domain_id}
      />

      <div className="input-field">
        <label>Beheerder</label>
        <Select
          isClearable={false}
          unstyled={true}
          defaultValue={selectedBeheerders}
          onChange={setSelectedBeheerders}
          options={optionBeheerders}
          isMulti={true}
          placeholder={"--Choose--"}
        />
      </div>

      <InputFieldSelect
        label={"Owner"}
        onChange={handleChange}
        name={"owner_id"}
        dropdownItems={optionOwners}
        value={formData.owner_id}
      />

      <InputFieldSelect
        label={"Service Level Agreement"}
        onChange={handleChange}
        name={"sla_id"}
        dropdownItems={optionSLA}
        value={formData.sla_id}
      />

      <button className="form-submit-btn" onClick={submit}>
        Submit
      </button>
      {invalidSubmit && <p className="invalid-text">Invalid submission</p>}
    </div>
  );
}
