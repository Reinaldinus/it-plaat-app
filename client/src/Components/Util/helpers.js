import axios from "axios";

export const makeLogoNameUnique = (logo) => {
  // Make copy of image file entered
  const logoFile = logo;
  const date = new Date();
  // Add 1 to month, as JS sees january as 0, feb as 1, etc.
  const curDateAndTime =
    date.getDate() +
    "" +
    (date.getMonth() + 1) +
    "" +
    date.getFullYear() +
    " " +
    date.getHours() +
    "" +
    date.getMinutes() +
    "" +
    date.getSeconds();

  // Make new file using old file, add current time and date to filename
  const newLogoFile = new File(
    [logoFile],
    curDateAndTime + " - " + logoFile.name,
    {
      type: logoFile.type,
    }
  );
  return newLogoFile;
};

export const uploadImage = (event, logoName) => {
  event.preventDefault();
  const data = new FormData();
  data.append("file", logoName);
  axios.post("http://localhost:8000/upload", data).then((res) => {});
};

export async function sendInfoToTheApi(
  method,
  id,
  app_name,
  product_owner_id,
  logo,
  app_description,
  sla_id,
  ip_address,
  server_id,
  business_domain_id,
  link_to_code,
  comments,
  link,
  owner_id,
  link_to_documentation,
  beheerders
) {
  beheerders = beheerders.map((beheerder) => ({
    id: beheerder.value,
    name: beheerder.label,
  }));
  const text = method === "put" ? `/${id}` : "";
  const token = sessionStorage.getItem("token");
  const response = await fetch(
    `http://localhost:8080/api/v1/application${text}`,
    {
      method: method,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        app_name,
        product_owner_id,
        logo,
        app_description,
        sla_id,
        ip_address,
        server_id,
        business_domain_id,
        link_to_code,
        comments,
        link,
        owner_id,
        link_to_documentation,
        beheerders,
      }),
    }
  );
  if (method === "post") {
    const data = await response.json();
    console.log(data);
    return data;
  }
}
