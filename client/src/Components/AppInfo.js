import "./Popup.css";

export default function AppInfo(props) {
  return (
    <div className="app-information">
      <strong>{props.title}</strong>
      <p>{props.data}</p>
    </div>
  );
}
