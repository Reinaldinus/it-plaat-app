import React, { useState, useEffect } from "react";
import "./Popup.css";
import "reactjs-popup/dist/index.css";
import DeleteConfirmPopup from "./DeleteConfirmPopup";
import Draggable from "react-draggable";

export default function AddServerBusinessDomainPopUp(props) {
  const [formData, setFormData] = useState({ name: "", id: "" });
  const [invalidSubmit, setInvalidSubmit] = useState("None");
  const [associatedApps, setAssociatedApps] = useState([]);
  const [deleteConfirmIsOpen, setDeleteConfirmIsOpen] = useState(false);

  const token = sessionStorage.getItem("token");

  let nameOfElementToBeDeleted = props.arrayOfBDOrServer.map((BDorServer) => {
    if (BDorServer.id === parseInt(formData.id)) {
      return BDorServer.name;
    }
  });
  nameOfElementToBeDeleted = nameOfElementToBeDeleted.filter(
    (name) => name !== undefined
  );

  useEffect(() => {
    for (let i = 0; i < props.allApps.length; i++) {
      if (parseInt(formData.id) === props.arrayOfAppsIds[i]) {
        setAssociatedApps((prevData) => [...prevData, props.allApps[i]]);
      }
    }
  }, [formData]);

  function deleteElement(id) {
    fetch(`http://localhost:8080/api/v1/${props.fetchRoute}/${id}`, {
      method: "DELETE",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    window.location.reload(false);
  }

  function handleChange(event) {
    setAssociatedApps([]);
    setInvalidSubmit("None");

    setFormData((prevData) => {
      return {
        ...prevData,
        [event.target.name]: event.target.value,
      };
    });
  }

  function sendInfoToTheApi(name) {
    fetch(`http://localhost:8080/api/v1/${props.fetchRoute}`, {
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({ name }),
    });
  }

  function submit() {
    if (formData.name != "" && formData.id === "") {
      sendInfoToTheApi(formData.name);
      window.location.reload(false);
    } else if (formData.name === "" && formData.id != "") {
      if (associatedApps.length > 0) {
        setInvalidSubmit("Apps");
      } else {
        setDeleteConfirmIsOpen(true);
      }
    } else {
      setInvalidSubmit("Both");
    }
  }

  const visualAssociatedApps = associatedApps.map((app) => (
    <li className="invalid-text" key={app.id}>
      {app.app_name}
    </li>
  ));

  const optionsBusinessDomains = props.arrayOfBDOrServer.map((option) => (
    <option key={option.id} value={option.id}>
      {option.name}
    </option>
  ));

  function closeDeleteConfirm() {
    setDeleteConfirmIsOpen(false);
  }

  return (
    <div>
      <Draggable bounds={{ left: -48, top: -45, right: 46, bottom: 336 }}>
        <div>
          <DeleteConfirmPopup
            trigger={deleteConfirmIsOpen}
            close={() => closeDeleteConfirm()}
          >
            <p>
              Are you sure you want to delete {nameOfElementToBeDeleted[0]}?
              This cannot be undone
            </p>
            <div className="filterBtns">
              <button
                onClick={() => deleteElement(formData.id)}
                className="yesNoBtn"
              >
                Yes
              </button>
              <button onClick={closeDeleteConfirm} className="yesNoBtn">
                No
              </button>
            </div>
          </DeleteConfirmPopup>
        </div>
      </Draggable>
      <div className="popup-container">
        <div className="popup-header-return">
          <h1>Add a {props.name}</h1>
          <button className="return-button" onClick={props.return}>
            Return
          </button>
        </div>
        <div className="input-field">
          <label>Add {props.name}</label>
          <input
            type="text"
            placeholder={`${props.name} name`}
            onChange={handleChange}
            name="name"
            value={formData.name}
          />
        </div>

        <div className="input-field">
          <label>Delete {props.name}</label>
          <select name="id" id="selectList" onChange={handleChange}>
            <option value="">--Choose--</option>
            {optionsBusinessDomains}
          </select>
        </div>

        <div className="checkbox-field">
          <input type="checkbox"></input>
          <label>Also add as a product owner</label>
          <input type="checkbox"></input>
          <label>Also add as an administrator</label>
          <input type="checkbox"></input>
          <label>Also add as an owner</label>
          <input type="checkbox"></input>
        </div>

        <button className="form-submit-btn" onClick={submit}>
          Submit
        </button>
        {invalidSubmit === "Both" && (
          <p className="invalid-text">
            You can't add and remove a {props.name} at the same time.
          </p>
        )}
        {invalidSubmit === "Apps" && (
          <div>
            <p className="invalid-text">
              The following apps are linked to this {props.name}. Please modify
              them before deleting the {props.name}.
            </p>
            <ul className="invalid-text">{visualAssociatedApps}</ul>
          </div>
        )}
      </div>
    </div>
  );
}
