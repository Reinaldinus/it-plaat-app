import React, { useEffect, useState } from "react";
import ToDoItem from "./ToDoItem";
import Select from "react-select";

export default function ToDoPopUp(props) {
  const [toDos, setToDos] = useState([]);
  const [addToDoStatus, setAddToDoStatus] = useState(false);
  const [historyMode, setHistoryMode] = useState(false);
  const [formData, setFormData] = useState({
    title: "",
    comment: "",
    deadline: "",
    isDone: false,
  });
  const [selectedBeheerders, setSelectedBeheerders] = useState([]);
  const [invalidSubmissionMode, setInvalidSubmissionMode] = useState(false);
  const [thereIsAnExpiredToDo, setThereIsAnExpiredToDo] = useState(false);
  console.log(props);
  const token = sessionStorage.getItem("token");

  const optionBeheerders = props.beheerders.map((option) => ({
    value: option.id,
    label: option.name,
  }));

  useEffect(() => {
    fetch(`http://localhost:8080/api/v1/todo/${props.app_id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setToDos(data);
      });
  }, []);

  useEffect(() => {
    for (let i = 0; i < toDos.length; i++) {
      if (Date.parse(toDos[i].deadline) - Date.parse(new Date()) < 0) {
        setThereIsAnExpiredToDo(true);
        return;
      }
    }
    setThereIsAnExpiredToDo(false);
  }, [toDos]);

  useEffect(() => {
    setAddToDoStatus(false);
  }, [toDos]);

  useEffect(() => {
    setSelectedBeheerders([]);
  }, [toDos]);

  useEffect(() => {
    setInvalidSubmissionMode(false);
  }, [formData]);

  function handleChange(event) {
    setFormData((prevData) => {
      return {
        ...prevData,
        [event.target.name]: event.target.value,
      };
    });
  }

  function updateToDoStateToDelete(toDoId) {
    setToDos((prevToDo) => prevToDo.filter((todo) => todo.id != toDoId));
  }

  function updateToDoStateToUpdate(
    toDoId,
    title,
    comment,
    deadline,
    beheerders
  ) {
    setToDos((prevToDo) =>
      prevToDo.map((todo) =>
        todo.id === toDoId
          ? {
              ...todo,
              title: title,
              comment: comment,
              deadline: deadline,
              beheerders: beheerders,
            }
          : todo
      )
    );
  }

  const visualToDos = toDos.map((todo) => {
    console.log(todo);
    return todo.isDone === false ? (
      <ToDoItem
        id={todo.id}
        key={todo.id}
        title={todo.title}
        comment={todo.comment}
        extraClassName=""
        deadline={todo.deadline}
        handleClick={toggleDone}
        buttonText="Mark as Done"
        isDone={todo.isDone}
        category="normal"
        updateToDoStateToDelete={() => updateToDoStateToDelete(todo.id)}
        updateToDoStateToUpdate={updateToDoStateToUpdate}
        beheerders={props.beheerders}
        myBeheerders={todo.beheerders}
      />
    ) : (
      <></>
    );
  });

  const historyToDos = toDos.map((todo) => {
    return todo.isDone === true ? (
      <ToDoItem
        id={todo.id}
        key={todo.id}
        title={todo.title}
        comment={todo.comment}
        extraClassName="done"
        deadline={todo.deadline}
        handleClick={toggleDone}
        isDone={todo.isDone}
        buttonText="Done"
        category="history"
        myBeheerders={todo.beheerders}
        updateToDoStateToDelete={() => updateToDoStateToDelete(todo.id)}
        updateToDoStateToUpdate={updateToDoStateToUpdate}
        beheerders={props.beheerders}
      />
    ) : (
      <></>
    );
  });

  function addToDo() {
    setAddToDoStatus(true);
  }

  function closeNewToDo() {
    setAddToDoStatus(false);
    setFormData({ title: "", comment: "", deadline: "", isDone: false });
  }

  function handleSubmitClick(event) {
    if (
      formData.title != "" &&
      (formData.deadline === "" ||
        Date.parse(formData.deadline) - Date.parse(new Date()) > 0)
    ) {
      event.preventDefault();
      sendToDoToApi(
        formData.title,
        formData.comment,
        formData.deadline,
        selectedBeheerders
      );
      setFormData({
        title: "",
        comment: "",
        deadline: "",
      });
    } else if (formData.title === "") {
      setInvalidSubmissionMode("title");
    } else {
      setInvalidSubmissionMode("date");
    }
  }

  function sendToDoToApi(title, comment, deadline, beheerders) {
    //If you don't select a beheerder, you get automatically assigned the beheerders of the app.
    if (beheerders.length === 0) {
      props.appBeherders.forEach((beheerder) => {
        beheerders.push(beheerder);
      });
    } else {
      //prepare beheerders with the json the api expects
      beheerders = beheerders.map((beheerder) => ({
        id: beheerder.value,
        name: beheerder.label,
      }));
    }

    fetch(`http://localhost:8080/api/v1/todo/${props.app_id}`, {
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        title,
        comment,
        deadline,
        beheerders,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        setToDos((prevData) => [
          ...prevData,
          {
            id: data,
            title: title,
            comment: comment,
            deadline: deadline,
            beheerders: beheerders,
            isDone: false,
          },
        ]);
      });
  }

  function toggleDone(toDoId, isDone) {
    if (isDone === false) {
      fetch(`http://localhost:8080/api/v1/todo/done/${toDoId}`, {
        method: "put",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }).then(
        setToDos((prevData) =>
          prevData.map((todo) =>
            todo.id === toDoId ? { ...todo, isDone: !todo.isDone } : todo
          )
        )
      );
    } else {
      fetch(`http://localhost:8080/api/v1/todo/undo/${toDoId}`, {
        method: "put",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }).then(
        setToDos((prevData) =>
          prevData.map((todo) =>
            todo.id === toDoId ? { ...todo, isDone: !todo.isDone } : todo
          )
        )
      );
    }
  }

  function handleHistoryClick() {
    setHistoryMode((prevValue) => !prevValue);
  }

  return (
    <div className="popup-container">
      <div className="popup-tabs-header">
        <div className="popup-tab" onClick={props.handleInfoButton}>
          Info
        </div>
        <div className="popup-tab" onClick={props.handleToDoButton}>
          Actions
          {thereIsAnExpiredToDo && (
            <div className="todo-expired-exclamation">!</div>
          )}
        </div>
      </div>
      <div className="popup-header">
        <img src={require(`../../assets/${props.logo}`)} />
        <h1>{props.app_name}</h1>
      </div>
      <div className="toDoList">
        {addToDoStatus && (
          <div className="todo-item">
            <div className="input-field darker">
              <label>Title</label>
              <input
                type="text"
                placeholder="Title"
                onChange={handleChange}
                name="title"
                value={formData.title}
              />
            </div>

            <div className="input-field darker">
              <label>Description</label>
              <textarea
                type="text"
                placeholder="Description"
                onChange={handleChange}
                name="comment"
                value={formData.comment}
              />
            </div>

            <div className="input-field darker">
              <label>Deadline</label>
              <input
                type="date"
                placeholder="Deadline"
                onChange={handleChange}
                name="deadline"
                value={formData.deadline}
              />
            </div>
            <div className="input-field darker">
              <label>Assign the to do </label>
              <Select
                isClearable={false}
                unstyled={true}
                defaultValue={selectedBeheerders}
                onChange={setSelectedBeheerders}
                options={optionBeheerders}
                isMulti={true}
              />
            </div>
            {invalidSubmissionMode === "date" && (
              <div className="invalid-text">
                You can't add a deadline in the past
              </div>
            )}
            {invalidSubmissionMode === "title" && (
              <div className="invalid-text">A to do needs a title</div>
            )}
            <div className="submit-todo-button-div">
              <button className="saveButton" onClick={handleSubmitClick}>
                Save
              </button>
              <button className="saveButton" onClick={closeNewToDo}>
                Cancel
              </button>
            </div>
          </div>
        )}
        {historyMode && <h3>History</h3>}
        {!historyMode ? visualToDos : historyToDos}
      </div>

      {/* ToDO: way of showing all to dos */}
      {!historyMode ? (
        <div className="todo-popup-historyLink" onClick={handleHistoryClick}>
          See all completed to dos
        </div>
      ) : (
        <div className="todo-popup-historyLink" onClick={handleHistoryClick}>
          See uncompleted to dos
        </div>
      )}
      {!historyMode && <button onClick={addToDo}>Add a to do</button>}
    </div>
  );
}
