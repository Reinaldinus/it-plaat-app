import React, { useEffect, useState } from "react";
import Select from "react-select";
import { nanoid } from "nanoid";

export default function ToDoItem(props) {
  const [updateMode, setUpdateMode] = useState(false);
  const [formData, setFormData] = useState({
    title: props.title,
    comment: props.comment,
    deadline: props.deadline,
    itDone: props.isDone,
  });
  const [confirmationMode, setConfirmationMode] = useState(false);
  const [selectedBeheerders, setSelectedBeheerders] = useState([]);
  const [myOldBeheerders, setMyOldBeheerders] = useState(
    props.myBeheerders ? props.myBeheerders : []
  );
  const [expiredStyle, setExpiredStyle] = useState("");
  const token = sessionStorage.getItem("token");

  useEffect(() => {
    setSelectedBeheerders(
      myOldBeheerders.map((beheerder) => ({
        value: beheerder.id,
        label: beheerder.name,
      }))
    );
  }, [myOldBeheerders]);

  useEffect(() => {
    Date.parse(formData.deadline) - Date.parse(new Date()) < 0
      ? setExpiredStyle("expired")
      : setExpiredStyle("");
  }, [formData]);

  function deleteToDo() {
    fetch("http://localhost:8080/api/v1/todo/" + props.id, {
      method: "DELETE",
      headers: { Authorization: `Bearer ${token}` },
    }).then(props.updateToDoStateToDelete);
  }

  function handleUpdateClick() {
    setFormData({
      title: props.title,
      comment: props.comment,
      deadline: props.deadline,
      itDone: props.isDone,
    });
    setUpdateMode((prevValue) => !prevValue);
  }

  function handleChange(event) {
    setFormData((prevData) => {
      return {
        ...prevData,
        [event.target.name]: event.target.value,
      };
    });
  }

  const optionBeheerders = props.beheerders.map((option) => ({
    value: option.id,
    label: option.name,
  }));

  function handleSubmitClick(title, comment, deadline, beheerders) {
    beheerders = beheerders.map((beheerder) => ({
      id: beheerder.value,
      name: beheerder.label,
    }));

    fetch(`http://localhost:8080/api/v1/todo/${props.id}`, {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        title,
        comment,
        deadline,
        beheerders,
      }),
    }).then(
      props.updateToDoStateToUpdate(
        props.id,
        title,
        comment,
        deadline,
        beheerders
      )
    );
    setUpdateMode(false);
  }

  function handleConfirmationClick() {
    setConfirmationMode((prevValue) => !prevValue);
  }

  const visualBeheerders = selectedBeheerders.map((beheerder, index) => (
    <span key={nanoid()}>
      {beheerder.label}
      {index < selectedBeheerders.length - 1 ? ", " : ""}
    </span>
  ));

  return (
    <div className={`todo-item ${props.extraClassName}`} key={props.id}>
      {!updateMode ? (
        <div>
          <div className="todo-item-header">
            <h3>{props.title}</h3>
            <div className="todo-item-edit-button-div">
              <button className="todo-delete-edit-button" onClick={deleteToDo}>
                🗑️
              </button>
              <button
                className="todo-delete-edit-button"
                onClick={handleUpdateClick}
              >
                ✏️
              </button>
            </div>
          </div>
          <div className="todo-info">{props.comment}</div>
          <div className={`todo-info ${expiredStyle}`}>{props.deadline}</div>
          <div className="todo-info">{visualBeheerders}</div>
          {props.category === "normal" && (
            <button
              className="doneButton"
              onClick={() => props.handleClick(props.id, false)}
            >
              {props.buttonText}
            </button>
          )}

          {props.category === "history" && !confirmationMode && (
            <button className="doneButton" onClick={handleConfirmationClick}>
              {props.buttonText}
            </button>
          )}
          {confirmationMode && (
            <div>
              <p>Are you sure you want to restore this to do?</p>
              <button
                onClick={() => props.handleClick(props.id, true)}
                className="yesNoButton"
              >
                Yes
              </button>
              <button onClick={handleConfirmationClick} className="yesNoButton">
                No
              </button>
            </div>
          )}
        </div>
      ) : (
        <div>
          <div className="todo-item">
            <div className="input-field darker">
              <label>Title</label>
              <input
                type="text"
                placeholder="Title"
                onChange={handleChange}
                name="title"
                value={formData.title}
              />
            </div>

            <div className="input-field darker">
              <label>Description</label>
              <textarea
                type="text"
                placeholder="Description"
                onChange={handleChange}
                name="comment"
                value={formData.comment}
              />
            </div>

            <div className="input-field darker">
              <label>Deadline</label>
              <input
                type="date"
                placeholder="Deadline"
                onChange={handleChange}
                name="deadline"
                value={formData.deadline}
              />
            </div>
            <div className="input-field darker">
              <label>Assign the to do to an administrator </label>
              <Select
                isClearable={false}
                unstyled={true}
                defaultValue={selectedBeheerders}
                onChange={setSelectedBeheerders}
                options={optionBeheerders}
                isMulti={true}
              />
            </div>
            <div className="todo-item-updatemode-buttons">
              <button
                className="doneButton"
                onClick={() =>
                  handleSubmitClick(
                    formData.title,
                    formData.comment,
                    formData.deadline,
                    selectedBeheerders
                  )
                }
              >
                Save
              </button>
              <button className="doneButton" onClick={handleUpdateClick}>
                Cancel
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
