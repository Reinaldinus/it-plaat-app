import React, { useState, useEffect } from "react";
import InputUpdatePopUp from "./InputUpdatePopUp";
import AddDeleteMaintenancePopUp from "./AddDeleteMaintenancePopUp";
import Login from "./Authentication/Login";

export default function AdminPanelPopUp(props) {
  const [popUpStatus, setPopUpStatus] = useState("None");
  const [appNames, setAppNames] = useState(
    props.allApps.map((app) => app.app_name)
  );

  function changeMenu(menu) {
    setPopUpStatus(menu);
  }

  function handleClickBeheerder() {
    setPopUpStatus("Beheerder");
  }

  function handleAppNamesAddition(appName) {
    setAppNames((prevState) => {
      return [...prevState, appName];
    });
  }

  return (
    <div>
      {popUpStatus === "None" && (
        <button
          onClick={() => {
            changeMenu("App");
          }}
        >
          Add app
        </button>
      )}
      {popUpStatus === "None" && (
        <button
          onClick={() => {
            changeMenu("Server");
          }}
        >
          Manage servers
        </button>
      )}
      {popUpStatus === "None" && (
        <button
          onClick={() => {
            changeMenu("BusinessDomain");
          }}
        >
          Manage business domains
        </button>
      )}
      {popUpStatus === "None" && (
        <button
          onClick={() => {
            changeMenu("ProductOwner");
          }}
        >
          Manage Product Owners
        </button>
      )}
      {popUpStatus === "None" && (
        <button onClick={handleClickBeheerder}>Manage Beheerders</button>
      )}
      {popUpStatus === "None" && (
        <button
          onClick={() => {
            changeMenu("Owner");
          }}
        >
          Manage Owners
        </button>
      )}
      {popUpStatus === "None" && (
        <button
          onClick={() => {
            changeMenu("SLA");
          }}
        >
          Manage Service Level Agreements
        </button>
      )}

      {
        /* Add app popup */
        popUpStatus === "App" && (
          <InputUpdatePopUp
            handleAppAddition={props.handleAppAddition}
            popUpState={"add"}
            method={"post"}
            servers={props.servers}
            businessDomains={props.businessDomains}
            product_owners={props.product_owners}
            beheerders={props.beheerder}
            owners={props.owners}
            sLAs={props.sLAs}
            appNames={appNames}
            updateAppNames={handleAppNamesAddition}
            allApps={props.allApps}
            return={() => {
              changeMenu("None");
            }}
          />
        )
      }

      {popUpStatus === "Server" && (
        <AddDeleteMaintenancePopUp
          fetchRoute="server"
          arrayOfAppsIds={props.allApps.map((app) => app.serverId)}
          arrayOfBDOrServer={props.servers}
          name="Server"
          allApps={props.allApps}
          return={() => {
            changeMenu("None");
          }}
        />
      )}

      {
        /* Add business domain popup */
        popUpStatus === "BusinessDomain" && (
          <AddDeleteMaintenancePopUp
            fetchRoute="business_domain"
            arrayOfAppsIds={props.allApps.map((app) => app.business_domain_id)}
            arrayOfBDOrServer={props.businessDomains}
            name="Business Domain"
            allApps={props.allApps}
            return={() => {
              changeMenu("None");
            }}
          />
        )
      }

      {popUpStatus === "ProductOwner" && (
        <AddDeleteMaintenancePopUp
          fetchRoute="product_owner"
          arrayOfAppsIds={props.allApps.map((app) => app.product_owner_id)}
          arrayOfBDOrServer={props.product_owners}
          name="Product Owner"
          allApps={props.allApps}
          return={() => {
            changeMenu("None");
          }}
        />
      )}

      {popUpStatus === "Beheerder" && (
        <AddDeleteMaintenancePopUp
          fetchRoute="beheerder"
          arrayOfAppsIds={props.allApps.map((app) => {
            return app.beheerders.map((beheerder) => beheerder.id);
          })}
          arrayOfBDOrServer={props.beheerder.map((beheerder) => ({
            id: beheerder.id,
            name: beheerder.name,
          }))}
          name="Beheerder"
          allApps={props.allApps}
          return={() => {
            changeMenu("None");
          }}
        />
      )}

      {popUpStatus === "Owner" && (
        <AddDeleteMaintenancePopUp
          fetchRoute="owner"
          arrayOfAppsIds={props.allApps.map((app) => app.owner_id)}
          arrayOfBDOrServer={props.owners}
          name="Owner"
          allApps={props.allApps}
          return={() => {
            changeMenu("None");
          }}
        />
      )}

      {popUpStatus === "SLA" && (
        <AddDeleteMaintenancePopUp
          fetchRoute="sla"
          arrayOfAppsIds={props.allApps.map((app) => app.sla_id)}
          arrayOfBDOrServer={props.sLAs}
          name="Service Level Agreement"
          allApps={props.allApps}
          return={() => {
            changeMenu("None");
          }}
        />
      )}
      {/* {popUpStatus === "Login" && <Login return={handleClickNone} />} */}
    </div>
  );
}
