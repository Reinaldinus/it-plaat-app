import React, { useState, useEffect } from "react";
import OperationDomain from "./OperationDomain";
import Popup from "reactjs-popup";
import Draggable from "react-draggable";
import FilterPopup from "./Components/FilterPopup/FilterPopup";
import "reactjs-popup/dist/index.css";
import AdminPanelPopUp from "./Components/AdminPanelPopUp";
import Login from "./Components/Authentication/Login";

export default function App() {
  const [servers, setServers] = useState([]);
  const [businessDomains, setBusinessDomains] = useState([]);
  const [product_owners, setProduct_owners] = useState([]);
  const [beheerders, setBeheerders] = useState([]);
  const [owners, setOwners] = useState([]);
  const [sLAs, setSLAs] = useState([]);
  const [allApps, setAllApps] = useState([]);
  // const [allRelationsAppBeheerder, setRelationsAppBeheerder] = useState([]);

  const [toggleArrayOwner, setToggleArrayOwner] = useState([]);
  const [toggleArrayPO, setToggleArrayPO] = useState([]);
  const [toggleArraySLA, setToggleArraySLA] = useState([]);
  const [toggleArrayBeheerder, setToggleArrayBeheerder] = useState([]);
  const [oneFilterButtonIsClicked, setOneFilterButtonIsClicked] =
    useState(false);
  const [login, setLogin] = useState(false);

  const token = sessionStorage.getItem("token");

  useEffect(() => {
    if (token) {
      fetch("http://localhost:8080/api/v1/business_domain", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => setBusinessDomains(data));
    }
  }, [login]);

  useEffect(() => {
    if (token) {
      fetch("http://localhost:8080/api/v1/application", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => setAllApps(data));
    }
  }, [login]);

  useEffect(() => {
    if (token) {
      fetch("http://localhost:8080/api/v1/server", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => setServers(data));
    }
  }, [login]);

  useEffect(() => {
    if (token) {
      fetch("http://localhost:8080/api/v1/product_owner", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => setProduct_owners(data));
    }
  }, [login]);

  useEffect(() => {
    if (token) {
      fetch("http://localhost:8080/api/v1/beheerder", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => setBeheerders(data));
    }
  }, [login]);

  useEffect(() => {
    if (token) {
      fetch("http://localhost:8080/api/v1/owner", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => setOwners(data));
    }
  }, [login]);

  useEffect(() => {
    if (token) {
      fetch("http://localhost:8080/api/v1/sla", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => setSLAs(data));
    }
  }, [login]);

  function handleAppAddition(
    appId,
    app_name,
    product_owner_id,
    logo,
    app_description,
    sla_id,
    ip_address,
    server_id,
    business_domain_id,
    link_to_code,
    comments,
    link,
    owner_id,
    link_to_documentation,
    beheerders
  ) {
    setAllApps((prevValue) => [
      ...prevValue,
      {
        id: appId,
        app_name: app_name,
        product_owner: product_owners.filter(
          (po) => po.id === Number(product_owner_id)
        )[0],
        logo: logo,
        app_description: app_description,
        sla: sLAs.filter((sla) => sla.id === Number(sla_id))[0],
        ip_address: ip_address,
        server: servers.filter((server) => server.id === Number(server_id))[0],
        business_domain: businessDomains.filter(
          (bd) => bd.id === Number(business_domain_id)
        )[0],
        link_to_code: link_to_code,
        comments: comments,
        link: link,
        owner: owners.filter((owner) => owner.id === Number(owner_id))[0],
        link_to_documentation: link_to_documentation,
        beheerders: beheerders,
        to_dos: [],
      },
    ]);
  }

  // OneIsClicked code
  const totalStateArray = toggleArrayPO.concat(
    toggleArraySLA,
    toggleArrayOwner,
    toggleArrayBeheerder
  );

  const totalToggleCheck = totalStateArray.map((item) => {
    return item.toggled;
  });

  let howManyToggled = 0;
  for (let i = 0; i < totalToggleCheck.length; i++) {
    if (totalToggleCheck[i] === true) {
      howManyToggled = howManyToggled + 1;
    }
  }

  useEffect(() => {
    if (totalToggleCheck.every((bool) => bool === true)) {
      setOneFilterButtonIsClicked(false);
    }
  }, [totalToggleCheck]);

  //Beheerder filterbutton code
  useEffect(() => {
    setToggleArrayBeheerder(
      beheerders.map((beh) => {
        return { ...beh, toggled: true };
      })
    );
  }, [beheerders]);

  //Owner filterbutton code
  useEffect(() => {
    setToggleArrayOwner(
      owners.map((owner) => {
        return { ...owner, toggled: true };
      })
    );
  }, [owners]);

  //SLA filterbutton code
  useEffect(() => {
    setToggleArraySLA(
      sLAs.map((sla) => {
        return { ...sla, toggled: true };
      })
    );
  }, [sLAs]);

  useEffect(() => {
    setToggleArrayPO(
      product_owners.map((po) => {
        return { ...po, toggled: true };
      })
    );
  }, [product_owners]);

  // State and function for opening and closing filterpopup.
  const [filterPopupIsOpen, setFilterPopupIsOpen] = useState(false);

  function handleFilter() {
    setFilterPopupIsOpen(!filterPopupIsOpen);
  }

  function handleLogin() {
    setLogin(true);
  }

  function logout() {
    sessionStorage.removeItem("token");
    setLogin(false);
    window.location.reload(false);
  }

  // Automatically log out after 15 min
  if (token) {
    setTimeout(logout, 900000);
  }

  return (
    <div className="mainContainer">
      <Login handleLogin={() => handleLogin()} />

      {token ? (
        <>
          <Draggable>
            <div>
              <FilterPopup
                trigger={filterPopupIsOpen}
                close={handleFilter}
                key="Filter_popup"
                // Everything for the filterbuttons
                howManyToggled={howManyToggled}
                toggleArrayBeheerder={toggleArrayBeheerder}
                toggleArrayOwner={toggleArrayOwner}
                toggleArraySLA={toggleArraySLA}
                toggleArrayPO={toggleArrayPO}
                setToggleArrayBeheerder={setToggleArrayBeheerder}
                setToggleArrayOwner={setToggleArrayOwner}
                setToggleArrayPO={setToggleArrayPO}
                setToggleArraySLA={setToggleArraySLA}
                setOneFilterButtonIsClicked={setOneFilterButtonIsClicked}
                oneFilterButtonIsClicked={oneFilterButtonIsClicked}
              />
            </div>
          </Draggable>
          <div className="mainHeader">
            <h1>Qquest IT Landschap</h1>

            <div>
              <button onClick={handleFilter}>Filter</button>
              {/* Admin panel popup */}

              <Popup trigger={<button>Admin panel</button>} modal>
                <AdminPanelPopUp
                  handleAppAddition={handleAppAddition}
                  servers={servers}
                  businessDomains={businessDomains}
                  product_owners={product_owners}
                  beheerder={beheerders}
                  owners={owners}
                  sLAs={sLAs}
                  allApps={allApps}
                />
              </Popup>
              <button onClick={logout}>Logout</button>
            </div>
          </div>
          {businessDomains && (
            <OperationDomain
              servers={servers}
              businessDomains={businessDomains}
              product_owners={product_owners}
              beheerder={beheerders}
              owners={owners}
              sLAs={sLAs}
              toggleArray={toggleArrayPO}
              toggleArraySLA={toggleArraySLA}
              toggleArrayOwner={toggleArrayOwner}
              toggleArrayBeheerder={toggleArrayBeheerder}
              allApps={allApps}
            />
          )}
        </>
      ) : (
        ""
      )}
    </div>
  );
}
