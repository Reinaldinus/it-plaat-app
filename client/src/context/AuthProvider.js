import { createContext, useState } from "react";

const AuthContext = createContext({});

export const AuthProvider = ({ children }) => {
  const [auth, setAuth] = useState({});
  return (
    <AuthContext.Provider value={{ auth, setAuth }}>
      {/* The children are all the components that render because we are logged in */}
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
