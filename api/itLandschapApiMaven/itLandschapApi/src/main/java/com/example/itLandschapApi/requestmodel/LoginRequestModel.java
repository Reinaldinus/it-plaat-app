package com.example.itLandschapApi.requestmodel;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginRequestModel {
    private final String email;
    private final String password;

    public LoginRequestModel(@JsonProperty("email") String email,
                             @JsonProperty("password") String password

    ) {
        this.email = email;
        this.password = password;}

    public String getEmail() {
        return email;
    }



    public String getPassword() {
        return password;
    }
}
