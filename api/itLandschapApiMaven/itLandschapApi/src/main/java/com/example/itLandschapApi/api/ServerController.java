package com.example.itLandschapApi.api;

import com.example.itLandschapApi.dto.CustomRepetitionException;
import com.example.itLandschapApi.dto.CustomUnauthenticatedRuntimeException;
import com.example.itLandschapApi.model.Server;
import com.example.itLandschapApi.security.JwtTokenProvider;
import com.example.itLandschapApi.service.ServerService;
import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/v1/server")
@RestController
public class ServerController {

    private final JwtTokenProvider jwtTokenProvider;
    private final ServerService serverService;

    @Autowired
    public ServerController (ServerService serverService, JwtTokenProvider jwtTokenProvider){
        this.serverService = serverService;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @ExceptionHandler(CustomUnauthenticatedRuntimeException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public Error handleCustomRuntimeException(CustomUnauthenticatedRuntimeException ex) {
        System.out.println(ex.getMessage());
        return new Error(ex.getMessage());
    }

    @ExceptionHandler(CustomRepetitionException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    public Error handleCustomRepetitionException(CustomRepetitionException ex) {
        System.out.println(ex.getMessage());
        return new Error(ex.getMessage());
    }

    @CrossOrigin
    @PostMapping
    public void addServer(HttpServletRequest request, Integer employeeId, @Valid @NonNull @RequestBody Server server){
        String userId = extractUserIdFromToken(request);
        serverService.addServer(server, userId);
    }

    @CrossOrigin
    @GetMapping
    public List<Server> getAllServers(HttpServletRequest request) {
        String userId = extractUserIdFromToken(request);

        return serverService.getAllServers(userId);
    }

    @DeleteMapping(path = "{id}")
    @CrossOrigin
    public void deleteServerById(HttpServletRequest request, @PathVariable("id") Integer id){
        String userId = extractUserIdFromToken(request);

        serverService.deleteServer(id, userId);
    }

    @PutMapping(path = "{id}")
    @CrossOrigin
    public void updateServerById(HttpServletRequest request, @PathVariable("id") Integer id, @Valid @NonNull @RequestBody Server serverToUpdate){
        String userId = extractUserIdFromToken(request);

        serverService.updateServer(id, serverToUpdate, userId);
    }

    private String extractUserIdFromToken(HttpServletRequest request) {
        String token = request.getHeader("Authorization").replace("Bearer ", "");
        String userId;
        try {
            userId = jwtTokenProvider.getUserIdFromToken(token);
        } catch (ExpiredJwtException ex) {
            throw new CustomUnauthenticatedRuntimeException("The token is expired, please log in again", HttpStatus.UNAUTHORIZED);

        }
        if (!jwtTokenProvider.validateToken(token, userId)) {
            throw new CustomUnauthenticatedRuntimeException("The provided JWT token is invalid or malformed. ", HttpStatus.UNAUTHORIZED);
        }
        return userId;
    }
}


