package com.example.itLandschapApi.requestmodel;

import com.example.itLandschapApi.model.Application;
import com.example.itLandschapApi.model.Beheerder;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ApplicationRequestModel {

    private final Integer id;
    private final String appName;
    private final Integer productOwnerId;
    private final String logo;
    private final Integer ServerId;
    private final Integer businessDomainId;
    private final String appDescription;
    private final Integer serviceLevelAgreementId;
    private final String ipAddress;
    private final String linkToCode;
    private final String comments;
    private final String link;
    private final Integer ownerId;
    private final String linkToDocumentation;
    private final List<Beheerder> beheerders;

//Can this also be an application in the constructor.
    public ApplicationRequestModel(@JsonProperty("id") Integer id,
                       @JsonProperty("app_name") String appName,
                       @JsonProperty("product_owner_id")  Integer productOwnerId,
                       @JsonProperty("logo") String logo,
                       @JsonProperty("server_id") Integer ServerId,
                       @JsonProperty("business_domain_id") Integer businessDomainId,
                       @JsonProperty("app_description") String appDescription,
                       @JsonProperty("sla_id") Integer serviceLevelAgreementId,
                       @JsonProperty("ip_address") String ipAddress,
                       @JsonProperty("link_to_code") String linkToCode,
                       @JsonProperty("comments") String comments,
                       @JsonProperty("link") String link,
                       @JsonProperty("owner_id") Integer ownerId,
                       @JsonProperty("link_to_documentation") String linkToDocumentation,
                       @JsonProperty("beheerders") List<Beheerder> beheerderList

    ){
        this.id = id;
        this.appName = appName;
        this.productOwnerId = productOwnerId;
        this.logo = logo;
        this.ServerId = ServerId;
        this.businessDomainId = businessDomainId;
        this.appDescription = appDescription;
        this.serviceLevelAgreementId = serviceLevelAgreementId;
        this.ipAddress = ipAddress;
        this.linkToCode = linkToCode;
        this.comments = comments;
        this.link = link;
        this.ownerId = ownerId;
        this.linkToDocumentation = linkToDocumentation;
        this.beheerders = beheerderList;
    }


    public Integer getId() {
        return id;
    }
    public String getAppName() { return appName; };
    public Integer getProductOwnerId() {return productOwnerId; };
    public String getLogo(){ return logo; };
    public Integer getServerId(){ return ServerId; };
    public Integer getBusinessDomainId(){ return businessDomainId; };
    public String getAppDescription(){ return appDescription; };
    public Integer getServiceLevelAgreementId(){ return serviceLevelAgreementId;};
    public String getIpAddress(){ return ipAddress; };
    public String getComments() {return comments;}
    public String getLinkToCode() {return linkToCode; }
    public String getLink() { return link; }
    public Integer getOwnerId() { return ownerId; }
    public String getLinkToDocumentation() { return linkToDocumentation; }
    public List<Beheerder> getBeheerders(){ return beheerders; }

    public Application toApplication(){
        return new Application(
                appName,
                getProductOwnerId(),
                getLogo(),
                getServerId(),
                getBusinessDomainId(),
                getAppDescription(),
                getServiceLevelAgreementId(),
                getIpAddress(),
                getLinkToCode(),
                getComments(),
                getLink(),
                getOwnerId(),
                getLinkToDocumentation()
        );
    }


}

