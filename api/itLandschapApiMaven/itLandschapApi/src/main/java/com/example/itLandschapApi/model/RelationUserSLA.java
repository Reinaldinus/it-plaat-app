package com.example.itLandschapApi.model;

import java.util.UUID;

public class RelationUserSLA {
    private final UUID userId;
    private final Integer slaId;



    public RelationUserSLA( UUID userId, Integer slaId){
        this.userId = userId;
        this.slaId = slaId;
    }



    public UUID getUserId() {
        return userId;
    }

    public Integer getSlaId(){ return slaId; };
}
