package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.Application;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ApplicationDao {

    int insertApplication(Application application);

    List<Application> selectApplicationsByPosition(Integer businessDomainId, Integer serverId, UUID userId);

    Optional<Application> findApplicationByName(String appName, UUID userId);
    List<Application> findApplicationsByBusinessDomainId(Integer businessDomainId, UUID userId);
    List<Application> findApplicationsByServerId(Integer serverId, UUID userId);
    List<Application> findApplicationsByOwnerId(Integer ownerId, UUID userId);
    List<Application> findApplicationsByPOId(Integer poId, UUID userId);
    List<Application> findApplicationsBySLAId(Integer slaId, UUID userId);
    List<Application> findApplicationsByBeheerderId(Integer beheerderId, UUID userId);

    List<Application> selectAllApplications(UUID userId);

    Optional<Application> selectApplicationAssociatedWithToDoByToDoId(Integer toDoId);

    int deleteApplicationById(Integer id);

    int updateApplicationById(Integer id, Application application);

}
