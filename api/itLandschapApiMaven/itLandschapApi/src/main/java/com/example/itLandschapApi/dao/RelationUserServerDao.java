package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.RelationUserServer;

public interface RelationUserServerDao {
    void insertRelationUserServer(RelationUserServer relationUserServer);

    boolean checkIfServerBelongsToUser(RelationUserServer relationUserServer);

    void deleteRelationUserServer(RelationUserServer relationUserServer);
}
