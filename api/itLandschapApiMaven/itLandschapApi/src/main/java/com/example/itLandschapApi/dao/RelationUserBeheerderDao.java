package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.RelationUserBeheerder;

public interface RelationUserBeheerderDao {

    void insertRelationUserBeheerder(RelationUserBeheerder relationUserBeheerder);

    boolean checkIfBeheerderBelongsToUser(RelationUserBeheerder relationUserBeheerder);

    void deleteRelationBeheerderUser(RelationUserBeheerder relationUserBeheerder);
}
