package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.RelationUserPO;
import com.example.itLandschapApi.model.RelationUserServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("user-server")
public class RelationUserServerDataAccessService implements RelationUserServerDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public RelationUserServerDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void insertRelationUserServer(RelationUserServer relationUserServer) {
        String sql = "INSERT INTO user_server_relation (user_id, server_id)  VALUES (?, ?)";
        jdbcTemplate.update(sql, relationUserServer.getUserId(), relationUserServer.getServerId());
    }

    @Override
    public boolean checkIfServerBelongsToUser(RelationUserServer relationUserServer) {
        String sql = "SELECT * FROM user_server_relation WHERE server_id = ? AND user_id = ? ";

        List<RelationUserServer> relationList = jdbcTemplate.query(sql, new Object[]{relationUserServer.getServerId(), relationUserServer.getUserId()},
                (resultSet, i) -> {
                    UUID userId = UUID.fromString(resultSet.getString("user_id"));
                    Integer serverId = resultSet.getInt("server_id");
                    return new RelationUserServer(userId, serverId);
                });
        return relationList.size() > 0;
    }

    @Override
    public void deleteRelationUserServer(RelationUserServer relationUserServer) {
        String sql = "DELETE FROM user_server_relation WHERE user_id = ? AND server_id = ? ";
        jdbcTemplate.update(sql, relationUserServer.getUserId(), relationUserServer.getServerId());
    }
}
