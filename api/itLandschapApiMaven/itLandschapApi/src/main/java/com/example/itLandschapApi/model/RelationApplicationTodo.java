package com.example.itLandschapApi.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RelationApplicationTodo {

    private final Integer applicationId;
    private final Integer toDoId;



    public RelationApplicationTodo(@JsonProperty("application_id") Integer applicationId,
                                        @JsonProperty("to_do_id") Integer toDoId
    ){
        this.applicationId = applicationId;
        this.toDoId = toDoId;
    }


    public Integer getApplicationId() {
        return applicationId;
    }

    public Integer getToDoId(){ return toDoId; };

}
