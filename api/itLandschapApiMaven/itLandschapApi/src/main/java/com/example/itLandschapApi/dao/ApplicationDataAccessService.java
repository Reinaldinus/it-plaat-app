package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.Application;
import com.example.itLandschapApi.model.Beheerder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.*;

@Repository("postgres")
public class ApplicationDataAccessService implements ApplicationDao {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ApplicationDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertApplication(Application application) {
            String sql = "INSERT INTO applications (app_name, product_owner_id, logo, server_id, " +
                            "business_domain_id, app_description, sla_id, ip_address," +
                            "link_to_code, comments, link, owner_id, link_to_documentation) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); " ;


        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(con ->{
            PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, application.getAppName());
            ps.setInt(2, application.getProductOwnerId());
            ps.setString(3, application.getLogo());
            ps.setInt(4, application.getServerId());
            ps.setInt(5, application.getBusinessDomainId());
            ps.setString(6, application.getAppDescription());
            ps.setInt(7, application.getServiceLevelAgreementId());
            ps.setString(8, application.getIpAddress());
            ps.setString(9, application.getLinkToCode());
            ps.setString(10, application.getComments());
            ps.setString(11, application.getLink());
            ps.setInt(12, application.getOwnerId());
            ps.setString(13, application.getLinkToDocumentation());

            return ps;
        }, keyHolder);

        Map<String, Object>keys = keyHolder.getKeys();
        Integer generatedId = (Integer) keys.get("id");
        return generatedId;
    }

    @Override
    public List<Application> selectApplicationsByPosition(Integer businessDomainId, Integer serverId, UUID userId) {

            String sql = "SELECT * FROM applications JOIN user_application_relation ON " +
                    "applications.id = user_application_relation.application_id WHERE " +
                    "business_domain_id = ? AND server_id = ? AND user_application_relation.user_id = ? ";

            return jdbcTemplate.query(sql, new Object[]{businessDomainId, serverId, userId}, (resultSet, i) -> {
                Integer id = resultSet.getInt("id");
                String appName = resultSet.getString("app_name");
                Integer productOwnerId = resultSet.getInt("product_owner_id");
                String logo = resultSet.getString("logo");
                String appDescription = resultSet.getString("app_description");
                Integer serviceLevelAgreementId = resultSet.getInt("sla_id");
                String ipAddress = resultSet.getString("ip_address");
                String linkToCode = resultSet.getString("link_to_code");
                String comments = resultSet.getString("comments");
                String link = resultSet.getString("link");
                Integer ownerId = resultSet.getInt("owner_id");
                String linkToDocumentation = resultSet.getString("link_to_documentation");


                return new Application(id, appName, productOwnerId, logo, serverId, businessDomainId, appDescription, serviceLevelAgreementId, ipAddress, linkToCode, comments, link, ownerId, linkToDocumentation);
            });
    }

    @Override
    public Optional<Application> findApplicationByName(String appName, UUID userId) {
        final String sql = "SELECT * FROM applications JOIN user_application_relation ON " +
        "applications.id = user_application_relation.application_id WHERE " +
                "user_application_relation.user_id = ? AND applications.app_name = ? ; ";
        List<Application> listApp = jdbcTemplate.query(sql, new Object[]{userId, appName}, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("app_name");
            Integer productOwnerId = resultSet.getInt("product_owner_id");
            String logo = resultSet.getString("logo");
            String appDescription = resultSet.getString("app_description");
            Integer serviceLevelAgreementId = resultSet.getInt("sla_id");
            String ipAddress = resultSet.getString("ip_address");
            String linkToCode = resultSet.getString("link_to_code");
            String comments = resultSet.getString("comments");
            String link = resultSet.getString("link");
            Integer ownerId = resultSet.getInt("owner_id");
            Integer serverId = resultSet.getInt("server_id");
            Integer businessDomainId = resultSet.getInt("business_domain_id");
            String linkToDocumentation = resultSet.getString("link_to_documentation");
            return new Application(id, name, productOwnerId, logo, serverId, businessDomainId, appDescription, serviceLevelAgreementId, ipAddress, linkToCode, comments, link, ownerId, linkToDocumentation);
        });
        return listApp.size() == 0 ? Optional.empty() : Optional.of(listApp.get(0));
    }

    @Override
    public List<Application> findApplicationsByBusinessDomainId(Integer businessDomainId, UUID userId) {
        final String sql = "SELECT * FROM applications JOIN user_application_relation ON " +
                "applications.id = user_application_relation.application_id WHERE " +
                "user_application_relation.user_id = ? AND applications.business_domain_id = ? ; ";
        return jdbcTemplate.query(sql, new Object[]{userId, businessDomainId}, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("app_name");
            Integer productOwnerId = resultSet.getInt("product_owner_id");
            String logo = resultSet.getString("logo");
            String appDescription = resultSet.getString("app_description");
            Integer serviceLevelAgreementId = resultSet.getInt("sla_id");
            String ipAddress = resultSet.getString("ip_address");
            String linkToCode = resultSet.getString("link_to_code");
            String comments = resultSet.getString("comments");
            String link = resultSet.getString("link");
            Integer ownerId = resultSet.getInt("owner_id");
            Integer serverId = resultSet.getInt("server_id");
            String linkToDocumentation = resultSet.getString("link_to_documentation");
            return new Application(id, name, productOwnerId, logo, serverId, businessDomainId, appDescription, serviceLevelAgreementId, ipAddress, linkToCode, comments, link, ownerId, linkToDocumentation);
        });
    }

    @Override
    public List<Application> findApplicationsByServerId(Integer serverId, UUID userId) {
        final String sql = "SELECT * FROM applications JOIN user_application_relation ON " +
                "applications.id = user_application_relation.application_id WHERE " +
                "user_application_relation.user_id = ? AND applications.server_id = ? ; ";
        return jdbcTemplate.query(sql, new Object[]{userId, serverId}, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("app_name");
            Integer productOwnerId = resultSet.getInt("product_owner_id");
            String logo = resultSet.getString("logo");
            String appDescription = resultSet.getString("app_description");
            Integer serviceLevelAgreementId = resultSet.getInt("sla_id");
            String ipAddress = resultSet.getString("ip_address");
            String linkToCode = resultSet.getString("link_to_code");
            String comments = resultSet.getString("comments");
            String link = resultSet.getString("link");
            Integer ownerId = resultSet.getInt("owner_id");
            Integer businessDomainId = resultSet.getInt("business_domain_id");
            String linkToDocumentation = resultSet.getString("link_to_documentation");
            return new Application(id, name, productOwnerId, logo, serverId, businessDomainId, appDescription, serviceLevelAgreementId, ipAddress, linkToCode, comments, link, ownerId, linkToDocumentation);
        });    }

    @Override
    public List<Application> findApplicationsByOwnerId(Integer ownerId, UUID userId) {
        final String sql = "SELECT * FROM applications JOIN user_application_relation ON " +
                "applications.id = user_application_relation.application_id WHERE " +
                "user_application_relation.user_id = ? AND applications.owner_id = ? ; ";
        return jdbcTemplate.query(sql, new Object[]{userId, ownerId}, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("app_name");
            Integer productOwnerId = resultSet.getInt("product_owner_id");
            String logo = resultSet.getString("logo");
            String appDescription = resultSet.getString("app_description");
            Integer serviceLevelAgreementId = resultSet.getInt("sla_id");
            String ipAddress = resultSet.getString("ip_address");
            String linkToCode = resultSet.getString("link_to_code");
            String comments = resultSet.getString("comments");
            String link = resultSet.getString("link");
            Integer serverId = resultSet.getInt("server_id");
            Integer businessDomainId = resultSet.getInt("business_domain_id");
            String linkToDocumentation = resultSet.getString("link_to_documentation");
            return new Application(id, name, productOwnerId, logo, serverId, businessDomainId, appDescription, serviceLevelAgreementId, ipAddress, linkToCode, comments, link, ownerId, linkToDocumentation);
        });     }

    @Override
    public List<Application> findApplicationsByPOId(Integer poId, UUID userId) {
        final String sql = "SELECT * FROM applications JOIN user_application_relation ON " +
                "applications.id = user_application_relation.application_id WHERE " +
                "user_application_relation.user_id = ? AND applications.product_owner_id = ? ; ";
        return jdbcTemplate.query(sql, new Object[]{userId, poId}, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("app_name");
            Integer businessDomainId = resultSet.getInt("business_domain_id");
            String logo = resultSet.getString("logo");
            String appDescription = resultSet.getString("app_description");
            Integer serviceLevelAgreementId = resultSet.getInt("sla_id");
            String ipAddress = resultSet.getString("ip_address");
            String linkToCode = resultSet.getString("link_to_code");
            String comments = resultSet.getString("comments");
            String link = resultSet.getString("link");
            Integer ownerId = resultSet.getInt("owner_id");
            Integer serverId = resultSet.getInt("server_id");
            String linkToDocumentation = resultSet.getString("link_to_documentation");
            return new Application(id, name, poId, logo, serverId, businessDomainId, appDescription, serviceLevelAgreementId, ipAddress, linkToCode, comments, link, ownerId, linkToDocumentation);
        });
    }

    @Override
    public List<Application> findApplicationsBySLAId(Integer slaId, UUID userId) {
        final String sql = "SELECT * FROM applications JOIN user_application_relation ON " +
                "applications.id = user_application_relation.application_id WHERE " +
                "user_application_relation.user_id = ? AND applications.sla_id = ? ; ";
        return jdbcTemplate.query(sql, new Object[]{userId, slaId}, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("app_name");
            Integer businessDomainId = resultSet.getInt("business_domain_id");
            String logo = resultSet.getString("logo");
            String appDescription = resultSet.getString("app_description");
            Integer poId = resultSet.getInt("product_owner_id");
            String ipAddress = resultSet.getString("ip_address");
            String linkToCode = resultSet.getString("link_to_code");
            String comments = resultSet.getString("comments");
            String link = resultSet.getString("link");
            Integer ownerId = resultSet.getInt("owner_id");
            Integer serverId = resultSet.getInt("server_id");
            String linkToDocumentation = resultSet.getString("link_to_documentation");
            return new Application(id, name, poId, logo, serverId, businessDomainId, appDescription, slaId, ipAddress, linkToCode, comments, link, ownerId, linkToDocumentation);
        });    }

    @Override
    public List<Application> findApplicationsByBeheerderId(Integer beheerderId, UUID userId) {
        final String sql = "SELECT * FROM applications " +
                "JOIN user_application_relation ON " +
                "applications.id = user_application_relation.application_id " +
                "JOIN application_beheerder_relation ON application_beheerder_relation.application_id = applications.id " +
                "WHERE user_application_relation.user_id = ? " +
                "AND application_beheerder_relation.beheerder_id = ?; ";
        return jdbcTemplate.query(sql, new Object[]{userId, beheerderId}, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("app_name");
            Integer businessDomainId = resultSet.getInt("business_domain_id");
            String logo = resultSet.getString("logo");
            Integer slaId = resultSet.getInt("sla_id");
            String appDescription = resultSet.getString("app_description");
            Integer poId = resultSet.getInt("product_owner_id");
            String ipAddress = resultSet.getString("ip_address");
            String linkToCode = resultSet.getString("link_to_code");
            String comments = resultSet.getString("comments");
            String link = resultSet.getString("link");
            Integer ownerId = resultSet.getInt("owner_id");
            Integer serverId = resultSet.getInt("server_id");
            String linkToDocumentation = resultSet.getString("link_to_documentation");
            return new Application(id, name, poId, logo, serverId, businessDomainId, appDescription, slaId, ipAddress, linkToCode, comments, link, ownerId, linkToDocumentation);
        });
    }


    @Override
    public List<Application> selectAllApplications(UUID userId) {

        final String sql = "SELECT * FROM applications JOIN user_application_relation ON " +
                "applications.id = user_application_relation.application_id WHERE " +
                "user_application_relation.user_id = ?; ";
        return jdbcTemplate.query(sql, new Object[]{userId}, (resultSet, i) -> {
            Integer id =resultSet.getInt("id");
            String appName = resultSet.getString("app_name");
            Integer productOwnerId = resultSet.getInt("product_owner_id");
            String logo = resultSet.getString("logo");
            Integer serverId = resultSet.getInt("server_id");
            Integer businessDomainId = resultSet.getInt("business_domain_id");
            String appDescription = resultSet.getString("app_description");
            Integer serviceLevelAgreementId = resultSet.getInt("sla_id");
            String ipAddress = resultSet.getString("ip_address");
            String linkToCode = resultSet.getString("link_to_code");
            String comments = resultSet.getString("comments");
            String link = resultSet.getString("link");
            Integer ownerId = resultSet.getInt("owner_id");
            String linkToDocumentation = resultSet.getString("link_to_documentation");
            return new Application(id, appName, productOwnerId, logo, serverId, businessDomainId, appDescription, serviceLevelAgreementId, ipAddress, linkToCode, comments, link, ownerId, linkToDocumentation);
        });

    }

    @Override
    public Optional<Application> selectApplicationAssociatedWithToDoByToDoId(Integer toDoId) {
        final String sql = "SELECT * FROM applications " +
                "JOIN application_todo_relation ON application_todo_relation.application_id = applications.id " +
                "WHERE application_todo_relation.to_do_id = ? ; ";
        List<Application> applicationList =  jdbcTemplate.query(sql, new Object[]{toDoId}, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("app_name");
            Integer businessDomainId = resultSet.getInt("business_domain_id");
            String logo = resultSet.getString("logo");
            Integer slaId = resultSet.getInt("sla_id");
            String appDescription = resultSet.getString("app_description");
            Integer poId = resultSet.getInt("product_owner_id");
            String ipAddress = resultSet.getString("ip_address");
            String linkToCode = resultSet.getString("link_to_code");
            String comments = resultSet.getString("comments");
            String link = resultSet.getString("link");
            Integer ownerId = resultSet.getInt("owner_id");
            Integer serverId = resultSet.getInt("server_id");
            String linkToDocumentation = resultSet.getString("link_to_documentation");
            return new Application(id, name, poId, logo, serverId, businessDomainId, appDescription, slaId, ipAddress, linkToCode, comments, link, ownerId, linkToDocumentation);
        });
        return applicationList.size() == 0 ? Optional.empty() : Optional.of(applicationList.get(0));

    }


    @Override
    public int deleteApplicationById(Integer id) {
            String sql = "DELETE FROM applications WHERE id = ? ";
            jdbcTemplate.update(sql, id);
            return 0;
    }

    @Override
    public int updateApplicationById(Integer id, Application application) {
            String sql = "UPDATE applications " +
                            "SET app_name = ?, " +
                            "product_owner_id = ?, logo = ?, " +
                            "server_id = ?, business_domain_id = ?, app_description = ?, sla_id = ?, " +
                            "ip_address = ?, link_to_code = ?, comments = ?, link = ?, " +
                            "owner_id = ?, link_to_documentation = ? " +
                            "WHERE id = ?;";
        jdbcTemplate.update(sql, application.getAppName(), application.getProductOwnerId(), application.getLogo(),
            application.getServerId(), application.getBusinessDomainId(), application.getAppDescription(),
            application.getServiceLevelAgreementId(), application.getIpAddress(), application.getLinkToCode(),
            application.getComments(), application.getLink(), application.getOwnerId(), application.getLinkToDocumentation(), id);
        return 0;
    }

}

