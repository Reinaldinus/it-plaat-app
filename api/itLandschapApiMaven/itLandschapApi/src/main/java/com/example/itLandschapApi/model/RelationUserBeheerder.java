package com.example.itLandschapApi.model;

import java.util.UUID;

public class RelationUserBeheerder {
    private final UUID userId;
    private final Integer beheerderId;



    public RelationUserBeheerder( UUID userId, Integer beheerderId){
        this.userId = userId;
        this.beheerderId = beheerderId;
    }



    public UUID getUserId() {
        return userId;
    }

    public Integer getBeheerderId(){ return beheerderId; };
}
