package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.RelationUserOwner;

public interface RelationUserOwnerDao {

    void insertRelationUserOwner(RelationUserOwner relationUserOwner);

    boolean checkIfOwnerBelongsToUser(RelationUserOwner relationUserOwner);

    void deleteRelationUserOwner(RelationUserOwner relationUserOwner);
}
