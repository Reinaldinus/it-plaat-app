package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.RelationUserSLA;
import com.example.itLandschapApi.model.RelationUserServer;

public interface RelationUserSLADao {
    void insertRelationUserSLA(RelationUserSLA relationUserSLA);

    boolean checkIfSLABelongsToUser(RelationUserSLA relationUserSLA);

    void deleteRelationUserSLA(RelationUserSLA relationUserSLA);
}
