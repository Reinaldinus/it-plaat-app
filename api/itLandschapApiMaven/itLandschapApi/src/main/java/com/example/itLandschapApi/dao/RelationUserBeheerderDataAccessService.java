package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.RelationUserApplication;
import com.example.itLandschapApi.model.RelationUserBeheerder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("user-beheerder")
public class RelationUserBeheerderDataAccessService implements RelationUserBeheerderDao {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public RelationUserBeheerderDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public void insertRelationUserBeheerder(RelationUserBeheerder relationUserBeheerder) {
        String sql = "INSERT INTO user_beheerder_relation (user_id, beheerder_id)  VALUES (?, ?)";
        jdbcTemplate.update(sql, relationUserBeheerder.getUserId(), relationUserBeheerder.getBeheerderId());
    }

    @Override
    public boolean checkIfBeheerderBelongsToUser(RelationUserBeheerder relationUserBeheerder) {
        String sql = "SELECT * FROM user_beheerder_relation WHERE beheerder_id = ? AND user_id = ? ";

        List<RelationUserBeheerder> relationList = jdbcTemplate.query(sql, new Object[]{relationUserBeheerder.getBeheerderId(), relationUserBeheerder.getUserId()},
                (resultSet, i) -> {
                    UUID userId = UUID.fromString(resultSet.getString("user_id"));
                    Integer beheerderId = resultSet.getInt("beheerder_id");
                    return new RelationUserBeheerder(userId, beheerderId);
                });
        return relationList.size() > 0;    }

    @Override
    public void deleteRelationBeheerderUser(RelationUserBeheerder relationUserBeheerder) {
        String sql = "DELETE FROM user_beheerder_relation WHERE user_id = ? AND beheerder_id = ? ";
        jdbcTemplate.update(sql, relationUserBeheerder.getUserId(), relationUserBeheerder.getBeheerderId());
    }
}
