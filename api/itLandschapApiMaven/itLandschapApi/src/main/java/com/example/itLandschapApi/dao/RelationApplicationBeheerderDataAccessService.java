package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.Owner;
import com.example.itLandschapApi.model.ProductOwner;
import com.example.itLandschapApi.model.RelationApplicationBeheerder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository("relation_app_beheerder")
public class RelationApplicationBeheerderDataAccessService implements RelationApplicationBeheerderDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public RelationApplicationBeheerderDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertRelationApplicationBeheerder(RelationApplicationBeheerder relationApplicationBeheerder) {
        String sql = "INSERT INTO application_beheerder_relation (application_id, beheerder_id) " +
                "VALUES (?, ?)";
        jdbcTemplate.update(sql, relationApplicationBeheerder.getApplicationId(), relationApplicationBeheerder.getBeheerderId());
        return 0;

    }

    @Override
    public List<RelationApplicationBeheerder> selectAllRelationApplicationBeheerder() {
        final String sql = "SELECT * FROM application_beheerder_relation";
        return jdbcTemplate.query(sql, (resultSet, i) -> {
            Integer appId = resultSet.getInt("application_id");
            Integer beheerderId = resultSet.getInt("beheerder_id");


            return new RelationApplicationBeheerder(appId, beheerderId);
        });
    }

    @Override
    public List<RelationApplicationBeheerder> selectRelationApplicationBeheerderByAppId(Integer appId) {
        String sql = "SELECT * FROM application_beheerder_relation WHERE application_id = ? ";
        return jdbcTemplate.query(sql, new Object[]{appId}, (resultSet, i) -> {
            Integer beheerderId = resultSet.getInt("beheerder_id");
            return new RelationApplicationBeheerder(appId, beheerderId);
        });
    }

    @Override
    public int deleteRelationApplicationBeheerderByAppAndBeheerderId(Integer appId, Integer beheerderId) {
        String sql = "DELETE FROM application_beheerder_relation WHERE application_id = ? AND beheerder_id = ? ; ";
        jdbcTemplate.update(sql, appId, beheerderId);
        return 0;
    }

    @Override
    public int deleteAllRelationApplicationBeheerderByApplicationId(Integer appId) {
        String sql = "DELETE FROM application_beheerder_relation WHERE application_id = ? ; ";
        jdbcTemplate.update(sql, appId);
        return 0;
    }

    @Override
    public int updateRelationApplicationBeheerderByAppAndBeheerderId(Integer appId, Integer beheerderId) {
        return 0;
    }
}
