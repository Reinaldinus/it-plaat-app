package com.example.itLandschapApi.model;

import java.util.UUID;

public class RelationUserBusinessDomain {
    private final UUID userId;
    private final Integer bdId;



    public RelationUserBusinessDomain( UUID userId, Integer bdId){
        this.userId = userId;
        this.bdId = bdId;
    }



    public UUID getUserId() {
        return userId;
    }

    public Integer getBdId(){ return bdId; };
}
