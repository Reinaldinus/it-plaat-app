package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.ProductOwner;
import com.example.itLandschapApi.model.RelationApplicationTodo;
import com.example.itLandschapApi.model.ToDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository("app-todo")
public class RelationApplicationToDoDataAccessService implements RelationApplicationToDoDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public RelationApplicationToDoDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertRelationApplicationToDo(RelationApplicationTodo relationApplicationTodo) {
        String sql = "INSERT INTO application_todo_relation (application_id, to_do_id) " +
                "VALUES (?, ?)";
        jdbcTemplate.update(sql, relationApplicationTodo.getApplicationId(), relationApplicationTodo.getToDoId());
        return 0;
    }

    @Override
    public Integer getIdOfApplicationAssociatedWithThisToDo(Integer toDoId) {
        String sql = "SELECT * FROM application_todo_relation WHERE to_do_id = ? ";
        //Specifying the values of it's parameter

        List<RelationApplicationTodo> listRelations = jdbcTemplate.query(sql, new Object[]{toDoId}, (resultSet, i) -> {
            Integer appId = resultSet.getInt("application_id");


            return new RelationApplicationTodo(appId, toDoId);
        });
        return listRelations.size() > 0 ? listRelations.get(0).getApplicationId() : null;    }

    @Override
    public int deleteApplicationToDoRelationByToDoId(Integer toDoId) {
        String sql = "DELETE FROM application_todo_relation WHERE to_do_id = ? ; ";
        jdbcTemplate.update(sql, toDoId);

        return 0;

    }

    @Override
    public int deleteApplicationToDoRelationByAppId(Integer appId) {
        String sql = "DELETE FROM application_todo_relation WHERE application_id = ? ; ";
        jdbcTemplate.update(sql, appId);

        return 0;
    }
}
