package com.example.itLandschapApi.api;

import com.example.itLandschapApi.dto.CustomRepetitionException;
import com.example.itLandschapApi.dto.CustomUnauthenticatedRuntimeException;
import com.example.itLandschapApi.model.ServiceLevelAgreement;
import com.example.itLandschapApi.security.JwtTokenProvider;
import com.example.itLandschapApi.service.ServiceLevelAgreementService;
import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/api/v1/sla")
@RestController
public class ServiceLevelAgreementController {

    private final ServiceLevelAgreementService serviceLevelAgreementService;
    private final JwtTokenProvider jwtTokenProvider;


    @Autowired
    public ServiceLevelAgreementController (HttpServletRequest request, ServiceLevelAgreementService serviceLevelAgreementService, JwtTokenProvider jwtTokenProvider){
        this.serviceLevelAgreementService = serviceLevelAgreementService;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @ExceptionHandler(CustomUnauthenticatedRuntimeException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public Error handleCustomRuntimeException(CustomUnauthenticatedRuntimeException ex) {
        System.out.println(ex.getMessage());
        return new Error(ex.getMessage());
    }

    @ExceptionHandler(CustomRepetitionException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    public Error handleCustomRepetitionException(CustomRepetitionException ex) {
        System.out.println(ex.getMessage());
        return new Error(ex.getMessage());
    }

    @CrossOrigin
    @PostMapping
    public void addServiceLevelAgreement(HttpServletRequest request, Integer employeeId, @Valid @NonNull @RequestBody ServiceLevelAgreement serviceLevelAgreement){
        String userId = extractUserIdFromToken(request);

        serviceLevelAgreementService.addServiceLevelAgreement(serviceLevelAgreement, userId);
    }

    @CrossOrigin
    @GetMapping
    public List<ServiceLevelAgreement> getAllServiceLevelAgreements(HttpServletRequest request) {
        String userId = extractUserIdFromToken(request);

        return serviceLevelAgreementService.getAllServiceLevelAgreements(userId);
    }

    @CrossOrigin
    @GetMapping(path="{id}")
    public Optional<ServiceLevelAgreement> getServiceLevelAgreementById(HttpServletRequest request, @PathVariable("id") Integer id) {
        String userId = extractUserIdFromToken(request);

        return serviceLevelAgreementService.getServiceLevelAgreementById(id, userId);
    }
    @CrossOrigin
    @DeleteMapping(path = "{id}")
    public void deleteServiceLevelAgreementById(HttpServletRequest request, @PathVariable("id") Integer id){
        String userId = extractUserIdFromToken(request);

        serviceLevelAgreementService.deleteServiceLevelAgreement(id, userId);
    }

    @CrossOrigin
    @PutMapping(path = "{id}")
    public void updateServiceLevelAgreementById(HttpServletRequest request, @PathVariable("id") Integer id, @Valid @NonNull @RequestBody ServiceLevelAgreement serviceLevelAgreementToUpdate){
        String userId = extractUserIdFromToken(request);
        serviceLevelAgreementService.updateServiceLevelAgreement(id, serviceLevelAgreementToUpdate, userId);
    }

    private String extractUserIdFromToken(HttpServletRequest request) {
        String token = request.getHeader("Authorization").replace("Bearer ", "");
        String userId;
        try {
            userId = jwtTokenProvider.getUserIdFromToken(token);
        } catch (ExpiredJwtException ex) {
            throw new CustomUnauthenticatedRuntimeException("The token is expired, please log in again", HttpStatus.UNAUTHORIZED);

        }
        if (!jwtTokenProvider.validateToken(token, userId)) {
            throw new CustomUnauthenticatedRuntimeException("The provided JWT token is invalid or malformed. ", HttpStatus.UNAUTHORIZED);
        }
        return userId;
    }
}
