package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.BusinessDomain;
import com.example.itLandschapApi.model.Server;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ServerDao {

    int insertServer(Server server);

    List<Server> selectAllServers(UUID userId);

    Optional<Server> selectServerById(Integer id);
    Optional<Server> selectServerByName(String name, UUID userId);

    int deleteServerById(Integer id);

    int updateServerById(Integer id, Server server);
}
