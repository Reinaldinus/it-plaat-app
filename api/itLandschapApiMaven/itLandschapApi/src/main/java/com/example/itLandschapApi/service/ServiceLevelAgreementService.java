package com.example.itLandschapApi.service;

import com.example.itLandschapApi.dao.ApplicationDao;
import com.example.itLandschapApi.dao.RelationUserSLADao;
import com.example.itLandschapApi.dao.ServiceLevelAgreementDao;
import com.example.itLandschapApi.dto.CustomRepetitionException;
import com.example.itLandschapApi.dto.CustomUnauthenticatedRuntimeException;
import com.example.itLandschapApi.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.annotation.QueryAnnotation;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ServiceLevelAgreementService {
    private final ServiceLevelAgreementDao serviceLevelAgreementDao;
    private final RelationUserSLADao relationUserSLADao;
    private final ApplicationDao applicationDao;

    @Autowired
    public ServiceLevelAgreementService(@Qualifier("slas")ServiceLevelAgreementDao serviceLevelAgreementDao,
                                        @Qualifier("user-sla") RelationUserSLADao relationUserSLADao,
                                        @Qualifier("postgres") ApplicationDao applicationDao){
        this.serviceLevelAgreementDao = serviceLevelAgreementDao;
        this.relationUserSLADao = relationUserSLADao;
        this.applicationDao = applicationDao;
    }

    public int addServiceLevelAgreement(ServiceLevelAgreement serviceLevelAgreement, String userId){
        //Check if there is a sla with the same name
        Optional<ServiceLevelAgreement> existingSLA = serviceLevelAgreementDao.selectServiceLevelAgreementByName(serviceLevelAgreement.getName(), UUID.fromString(userId));
        if(existingSLA.isPresent()) {
            throw new CustomRepetitionException("There is already a sla with this name. You can't create two slas with the same name", HttpStatus.CONFLICT);
        }

        Integer slaId = serviceLevelAgreementDao.insertServiceLevelAgreement(serviceLevelAgreement);
        relationUserSLADao.insertRelationUserSLA(new RelationUserSLA(UUID.fromString(userId), slaId));
        return slaId;
    }

    public List<ServiceLevelAgreement> getAllServiceLevelAgreements(String userId){
        return serviceLevelAgreementDao.selectAllServiceLevelAgreements(UUID.fromString(userId));
    }

    public Optional<ServiceLevelAgreement> getServiceLevelAgreementById(Integer id, String userId){
        if (!relationUserSLADao.checkIfSLABelongsToUser(new RelationUserSLA(UUID.fromString(userId), id))){
            throw new CustomUnauthenticatedRuntimeException("This sla doesn't exist or you don't have permission.", HttpStatus.UNAUTHORIZED);
        }
        return serviceLevelAgreementDao.selectServiceLevelAgreementById(id);
    }


    public void deleteServiceLevelAgreement(Integer id, String userId){
        RelationUserSLA receivedRelation = new RelationUserSLA(UUID.fromString(userId), id);
        if (!relationUserSLADao.checkIfSLABelongsToUser(receivedRelation)){
            throw new CustomUnauthenticatedRuntimeException("This sla doesn't exist or you don't have permission.", HttpStatus.UNAUTHORIZED);
        }
        //Check if SLA has apps and then complain
        List<Application> list = applicationDao.findApplicationsBySLAId(id, UUID.fromString(userId));
        if(list.size() > 0){
            String errorMessage = "";
            for(int i = 0; i < list.size(); i++){
                errorMessage = errorMessage + list.get(i).getAppName();
                if(i != list.size()-1){
                    errorMessage = errorMessage + ", ";
                }
            }
            throw new CustomRepetitionException("This SLA has the following apps associated with it: "+ errorMessage + ". Please delete them or associate it with another SLA before.", HttpStatus.CONFLICT);
        }
        serviceLevelAgreementDao.deleteServiceLevelAgreementById(id);
        relationUserSLADao.deleteRelationUserSLA(receivedRelation);
    }

    public int updateServiceLevelAgreement(Integer id, ServiceLevelAgreement serviceLevelAgreement, String userId){
        //Check if there is a sla with the same name
        Optional<ServiceLevelAgreement> existingSLA = serviceLevelAgreementDao.selectServiceLevelAgreementByName(serviceLevelAgreement.getName(), UUID.fromString(userId));
        if(existingSLA.isPresent() && !existingSLA.get().getId().equals(id)) {
            throw new CustomRepetitionException("There is already a sla with this name. You can't create two slas with the same name", HttpStatus.CONFLICT);
        }
        if (!relationUserSLADao.checkIfSLABelongsToUser(new RelationUserSLA(UUID.fromString(userId), id))){
            throw new CustomUnauthenticatedRuntimeException("This sla doesn't exist or you don't have permission.", HttpStatus.UNAUTHORIZED);
        }
        return serviceLevelAgreementDao.updateServiceLevelAgreementById(id, serviceLevelAgreement);
    }
}
