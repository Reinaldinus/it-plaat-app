package com.example.itLandschapApi.service;

import com.example.itLandschapApi.dao.*;
import com.example.itLandschapApi.dto.CustomRepetitionException;
import com.example.itLandschapApi.dto.CustomUnauthenticatedRuntimeException;
import com.example.itLandschapApi.model.Application;
import com.example.itLandschapApi.model.Beheerder;
import com.example.itLandschapApi.model.RelationUserBeheerder;
import com.example.itLandschapApi.model.ToDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class BeheerderService {
    private final BeheerderDao beheerderDao;
    private final RelationUserBeheerderDao relationUserBeheerderDao;
    private final ApplicationDao applicationDao;
    private final ToDoDao toDoDao;
    private final RelationBeheerderToDoDao relationBeheerderToDoDao;

    @Autowired
    public BeheerderService(@Qualifier("beheerders") BeheerderDao beheerderDao,
                            @Qualifier("user-beheerder") RelationUserBeheerderDao relationUserBeheerderDao,
                            @Qualifier("postgres") ApplicationDao applicationDao,
                            @Qualifier("todos") ToDoDao toDoDao,
                            @Qualifier("relation_beheerder_todo") RelationBeheerderToDoDao relationBeheerderToDoDao) {
        this.beheerderDao = beheerderDao;
        this.relationUserBeheerderDao = relationUserBeheerderDao;
        this.applicationDao = applicationDao;
        this.toDoDao = toDoDao;
        this.relationBeheerderToDoDao = relationBeheerderToDoDao;
    }

    public int addBeheerder(Beheerder beheerder, String userId) {
        //Check if this user already has a beheerder with this exact name
        Optional<Beheerder> existingBeheerder = beheerderDao.findBeheerderByName(UUID.fromString(userId), beheerder.getName());
        if (existingBeheerder.isPresent()) {
            throw new CustomRepetitionException("There is already a beheerder with this name. You can't create two beheerders with the same name", HttpStatus.CONFLICT);
        }

        Integer beheerderId = beheerderDao.insertBeheerder(beheerder);
        relationUserBeheerderDao.insertRelationUserBeheerder(new RelationUserBeheerder(UUID.fromString(userId), beheerderId));
        return beheerderId;
    }

    public Optional<Beheerder> getBeheerderById(Integer id, String userId) {
        if (!relationUserBeheerderDao.checkIfBeheerderBelongsToUser(new RelationUserBeheerder(UUID.fromString(userId), id))) {
            throw new CustomUnauthenticatedRuntimeException("This administrator doesn't exist or you don't have permission.", HttpStatus.UNAUTHORIZED);
        }

        return beheerderDao.selectBeheerderById(id);
    }

    public List<Beheerder> getAllBeheerders(String userId) {
        return beheerderDao.selectAllBeheerders(UUID.fromString(userId));
    }

    public void deleteBeheerder(Integer id, String userId) {
        RelationUserBeheerder receivedRelation = new RelationUserBeheerder(UUID.fromString(userId), id);
        //Check if beheerder is associated with the user
        if (!relationUserBeheerderDao.checkIfBeheerderBelongsToUser(receivedRelation)) {
            throw new CustomUnauthenticatedRuntimeException("This administrator doesn't exist or you don't have permission to delete it.", HttpStatus.UNAUTHORIZED);
        }

        //Check if beheerder is associated with an app
        List<Application> list = applicationDao.findApplicationsByBeheerderId(id, UUID.fromString(userId));
        if (list.size() > 0) {
            String errorMessage = "";
            for (int i = 0; i < list.size(); i++) {
                errorMessage = errorMessage + list.get(i).getAppName();
                if (i != list.size() - 1) {
                    errorMessage = errorMessage + ", ";
                }
            }
            throw new CustomRepetitionException(
                    "This beheerder has the following apps associated with it: "
                            + errorMessage +
                            ". Please delete them before or associate them with another beheerder.",
                    HttpStatus.CONFLICT
            );
        }

        // Check if beheerder os associated with a to do and then complain.
        List<ToDo> toDosOfThisBeheerder = toDoDao.selectToDosByBeheerderId(id);
        if (toDosOfThisBeheerder.size() > 0) {
            String errorMessage = "";
            for (int i = 0; i < toDosOfThisBeheerder.size(); i++) {
                //get application associated with this to do.
                Optional<Application> optionalApp = applicationDao.selectApplicationAssociatedWithToDoByToDoId( toDosOfThisBeheerder.get(i).getId());
                if(optionalApp.isPresent()){
                    errorMessage = errorMessage + optionalApp.get().getAppName();
                    System.out.println(errorMessage);
                    System.out.println(errorMessage);
                }

                if (i != toDosOfThisBeheerder.size() - 1) {
                    errorMessage = errorMessage + ", ";
                }
            }
            throw new CustomRepetitionException("This beheerder is responsible for some tasks in the following apps: " + errorMessage + ". Please delete them or associate them with another beheerder before.", HttpStatus.CONFLICT);
        }

        beheerderDao.deleteBeheerderById(id);
        relationUserBeheerderDao.deleteRelationBeheerderUser(receivedRelation);
    }

    public void deleteBeheerderAndToDoRelation(Integer id, String userId){
        RelationUserBeheerder receivedRelation = new RelationUserBeheerder(UUID.fromString(userId), id);
        //Check if beheerder is associated with the user
        if (!relationUserBeheerderDao.checkIfBeheerderBelongsToUser(receivedRelation)) {
            throw new CustomUnauthenticatedRuntimeException("This administrator doesn't exist or you don't have permission to delete it.", HttpStatus.UNAUTHORIZED);
        }

        //Check if beheerder is associated with an app
        List<Application> list = applicationDao.findApplicationsByBeheerderId(id, UUID.fromString(userId));
        if (list.size() > 0) {
            String errorMessage = "";
            for (int i = 0; i < list.size(); i++) {
                errorMessage = errorMessage + list.get(i).getAppName();
                if (i != list.size() - 1) {
                    errorMessage = errorMessage + ", ";
                }
            }
            throw new CustomRepetitionException(
                    "This beheerder has the following apps associated with it: "
                            + errorMessage +
                            ". Please delete them before or associate them with another beheerder.",
                    HttpStatus.CONFLICT
            );
        }
        beheerderDao.deleteBeheerderById(id);
        relationUserBeheerderDao.deleteRelationBeheerderUser(receivedRelation);
        relationBeheerderToDoDao.deleteAllRelationBeheerderToDoByBeheerderId(id);
    }

    public int updateBeheerder(Integer id, Beheerder beheerder, String userId) {
        //Check if tjere is already a beheerder with this name
        Optional<Beheerder> existingBeheerder = beheerderDao.findBeheerderByName(UUID.fromString(userId), beheerder.getName());
        if (existingBeheerder.isPresent() && !existingBeheerder.get().getId().equals(id)) {
            throw new CustomRepetitionException("There is already a beheerder with this name. You can't create two beheerders with the same name", HttpStatus.CONFLICT);
        }
        RelationUserBeheerder receivedRelation = new RelationUserBeheerder(UUID.fromString(userId), id);
        if (!relationUserBeheerderDao.checkIfBeheerderBelongsToUser(receivedRelation)) {
            throw new CustomUnauthenticatedRuntimeException("This administrator doesn't exist or you don't have permission to modify it.", HttpStatus.UNAUTHORIZED);

        }
        return beheerderDao.updateBeheerderById(id, beheerder);
    }
}
