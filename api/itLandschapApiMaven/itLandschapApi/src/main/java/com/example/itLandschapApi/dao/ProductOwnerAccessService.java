package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.ProductOwner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.*;


@Repository("productOwners")
public class ProductOwnerAccessService implements ProductOwnerDao {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductOwnerAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertProductOwner(ProductOwner productOwner) {
        String sql = "INSERT INTO product_owners (name) VALUES (?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(con ->{
            PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1,productOwner.getName());

            return ps;
        }, keyHolder);

        Map<String, Object> keys = keyHolder.getKeys();
        Integer generatedId = (Integer) keys.get("id");
        return generatedId;

    }

    @Override
    public List<ProductOwner> selectAllProductOwners(UUID userId) {
        final String sql = "SELECT * FROM product_owners JOIN user_po_relation ON " +
                "product_owners.id = user_po_relation.po_id  WHERE " +
                "user_po_relation.user_id = ? ";
        return jdbcTemplate.query(sql, new Object[]{userId}, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("name");


            return new ProductOwner(id, name);

        });
    }

    @Override
    public Optional<ProductOwner> selectProductOwnerById(Integer id) {
        final String sql = "SELECT * FROM product_owners JOIN user_po_relation ON " +
                "product_owners.id = user_po_relation.po_id  WHERE product_owners.id = ? ";
        //Specifying the values of it's parameter

        List<ProductOwner> listPO = jdbcTemplate.query(sql, new Object[]{id}, (resultSet, i) -> {
            Integer productOwnerId = resultSet.getInt("id");
            String name = resultSet.getString("name");


            return new ProductOwner(productOwnerId, name);
        });
        return listPO.size() == 0 ? Optional.empty() : Optional.of(listPO.get(0));
    }

    @Override
    public Optional<ProductOwner> selectProductOwnerByName(String name, UUID userId) {
        final String sql = "SELECT * FROM product_owners JOIN user_po_relation ON " +
                "product_owners.id = user_po_relation.po_id  WHERE " +
                "user_po_relation.user_id = ? AND product_owners.name = ?";
        List<ProductOwner> listPO = jdbcTemplate.query(sql, new Object[]{userId, name}, (resultSet, i) -> {
            Integer productOwnerId = resultSet.getInt("id");
            return new ProductOwner(productOwnerId, name);
        });
        return listPO.size() == 0 ? Optional.empty() : Optional.of(listPO.get(0));
    }

    @Override
    public int deleteProductOwnerById(Integer id) {
        String sql = "DELETE FROM product_owners WHERE id = ? ";
        jdbcTemplate.update(sql, id);
        return 0;
    }

    @Override
    public int updateProductOwnerById(Integer id, ProductOwner productOwner) {
        String sql = "UPDATE product_owners SET name = ? WHERE id = ?;";
        jdbcTemplate.update(sql, productOwner.getName(), id);

        return 0;

    }
}
