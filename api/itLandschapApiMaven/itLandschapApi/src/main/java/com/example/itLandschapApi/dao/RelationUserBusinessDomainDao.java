package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.RelationUserBeheerder;
import com.example.itLandschapApi.model.RelationUserBusinessDomain;

public interface RelationUserBusinessDomainDao {
    void insertRelationUserBusinessDomain(RelationUserBusinessDomain relationUserBusinessDomain);

    boolean checkIfBusinessDomainBelongsToUser(RelationUserBusinessDomain relationUserBusinessDomain);

    void deleteRelationUserBusinessDomain(RelationUserBusinessDomain relationUserBusinessDomain);
}
