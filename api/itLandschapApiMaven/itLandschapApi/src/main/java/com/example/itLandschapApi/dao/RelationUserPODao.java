package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.RelationUserOwner;
import com.example.itLandschapApi.model.RelationUserPO;

public interface RelationUserPODao {

    void insertRelationUserPO(RelationUserPO relationUserPO);

    boolean checkIfPOBelongsToUser(RelationUserPO relationUserPO);

    void deleteRelationUserPO(RelationUserPO relationUserPO);

}
