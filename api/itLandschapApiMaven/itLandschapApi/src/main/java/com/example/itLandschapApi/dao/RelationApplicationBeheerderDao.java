package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.ProductOwner;
import com.example.itLandschapApi.model.RelationApplicationBeheerder;
import com.example.itLandschapApi.model.ServiceLevelAgreement;

import java.util.List;

public interface RelationApplicationBeheerderDao {
    int insertRelationApplicationBeheerder(RelationApplicationBeheerder relationApplicationBeheerder);


   List<RelationApplicationBeheerder> selectAllRelationApplicationBeheerder();

    List<RelationApplicationBeheerder> selectRelationApplicationBeheerderByAppId(Integer appId);


    int deleteRelationApplicationBeheerderByAppAndBeheerderId(Integer appId, Integer beheerderId);
    int deleteAllRelationApplicationBeheerderByApplicationId(Integer appId);

    int updateRelationApplicationBeheerderByAppAndBeheerderId(Integer appId, Integer beheerderId);
}
