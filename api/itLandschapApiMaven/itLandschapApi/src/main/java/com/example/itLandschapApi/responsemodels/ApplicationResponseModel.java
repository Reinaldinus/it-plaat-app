package com.example.itLandschapApi.responsemodels;

import com.example.itLandschapApi.model.*;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.security.Provider;
import java.util.ArrayList;
import java.util.List;


public class ApplicationResponseModel {
    @JsonProperty("id")
    private final Integer id;
    @JsonProperty("app_name")
    private final String appName;
    @JsonProperty("product_owner")
    private ProductOwner productOwner;
    @JsonProperty("logo")
    private final String logo;

    @JsonProperty("server")
    private Server server;
    @JsonProperty("business_domain")
    private BusinessDomain businessDomain;
    @JsonProperty("app_description")
    private final String appDescription;
    @JsonProperty("sla")
    private ServiceLevelAgreement serviceLevelAgreement;
    @JsonProperty("ip_address")
    private final String ipAddress;
    @JsonProperty("link_to_code")
    private final String linkToCode;
    @JsonProperty("comments")
    private final String comments;
    @JsonProperty("link")
    private final String link;
    @JsonProperty("owner")
    private Owner owner;

    @JsonProperty("link_to_documentation")
    private final String linkToDocumentation;

    @JsonProperty("beheerders")
    private final List<Beheerder> beheerders;

    @JsonProperty("to_dos")
    private final List<ToDo> toDos;


    public ApplicationResponseModel(Application application
    ) {
        this.id = application.getId();
        this.appName = application.getAppName();
        this.productOwner = null;
        this.logo = application.getLogo();
        this.server = null;
        this.businessDomain = null;
        this.appDescription = application.getAppDescription();
        this.serviceLevelAgreement = null;
        this.ipAddress = application.getIpAddress();
        this.linkToCode = application.getLinkToCode();
        this.comments = application.getComments();
        this.link = application.getLink();
        this.owner = null;
        this.linkToDocumentation = application.getLinkToDocumentation();
        this.beheerders = new ArrayList<>();
        this.toDos = new ArrayList<>();
    }


    public ApplicationResponseModel addProductOwner(ProductOwner po) {
        productOwner = po;
        return this;
    }

    public ApplicationResponseModel addServer(Server server) {
        this.server = server;
        return this;
    }

    public ApplicationResponseModel addBusinessDomain(BusinessDomain bd) {
        this.businessDomain = bd;
        return this;
    }

    public ApplicationResponseModel addServiceLevelAgreement(ServiceLevelAgreement sla) {
        this.serviceLevelAgreement = sla;
        return this;
    }

    public ApplicationResponseModel addOwner(Owner owner) {
        this.owner = owner;
        return this;
    }


    public ApplicationResponseModel(Integer id,
                                    String appName,
                                    ProductOwner productOwner,
                                    String logo,
                                    Server server,
                                    BusinessDomain businessDomain,
                                    String appDescription,
                                    ServiceLevelAgreement serviceLevelAgreement,
                                    String ipAddress,
                                    String linkToCode,
                                    String comments,
                                    String link,
                                    Owner owner,
                                    String linkToDocumentation
    ) {
        this.id = id;
        this.appName = appName;
        this.productOwner = productOwner;
        this.logo = logo;
        this.server = server;
        this.businessDomain = businessDomain;
        this.appDescription = appDescription;
        this.serviceLevelAgreement = serviceLevelAgreement;
        this.ipAddress = ipAddress;
        this.linkToCode = linkToCode;
        this.comments = comments;
        this.link = link;
        this.owner = owner;
        this.linkToDocumentation = linkToDocumentation;
        this.beheerders = new ArrayList<>();
        this.toDos = new ArrayList<>();
    }


    public Integer getId() {
        return id;
    }

    public String getAppName() {
        return appName;
    }

    ;

    public ProductOwner getProductOwner() {
        return productOwner;
    }

    public void setProductOwner(ProductOwner productOwner) {
        this.productOwner = productOwner;
    }

    public String getLogo() {
        return logo;
    }



    public Server getServer() {
        return server;
    }

    public void setServer(Server server){
        this.server = server;
    }

    public BusinessDomain getBusinessDomain() {
        return businessDomain;
    }

    public void setBusinessDomain(BusinessDomain businessDomain) {
        this.businessDomain = businessDomain;
    }

    public String getAppDescription() {
        return appDescription;
    }

    ;

    public ServiceLevelAgreement getServiceLevelAgreement() {
        return serviceLevelAgreement;
    }

    public void setServiceLevelAgreement(ServiceLevelAgreement serviceLevelAgreement) {
        this.serviceLevelAgreement = serviceLevelAgreement;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    ;

    public String getComments() {
        return comments;
    }

    public String getLinkToCode() {
        return linkToCode;
    }

    public String getLink() {
        return link;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getLinkToDocumentation() {
        return linkToDocumentation;
    }

    public List<Beheerder> getBeheerders() {
        return beheerders;
    }

    public List<ToDo> getToDos() {
        return toDos;
    }

    public void addBeheerdersToApp(Beheerder beheerder) {
        beheerders.add(beheerder);
    }

    public void addToDosToApp(ToDo toDo) {
        toDos.add(toDo);
    }

}


