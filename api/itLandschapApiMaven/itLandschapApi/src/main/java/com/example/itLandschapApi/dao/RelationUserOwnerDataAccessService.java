package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.RelationUserBeheerder;
import com.example.itLandschapApi.model.RelationUserBusinessDomain;
import com.example.itLandschapApi.model.RelationUserOwner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("user-owner")
public class RelationUserOwnerDataAccessService implements RelationUserOwnerDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public RelationUserOwnerDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void insertRelationUserOwner(RelationUserOwner relationUserOwner) {
        String sql = "INSERT INTO user_owner_relation (user_id, owner_id)  VALUES (?, ?)";
        jdbcTemplate.update(sql, relationUserOwner.getUserId(), relationUserOwner.getOwnerId());
    }

    @Override
    public boolean checkIfOwnerBelongsToUser(RelationUserOwner relationUserOwner) {
        String sql = "SELECT * FROM user_owner_relation WHERE owner_id = ? AND user_id = ? ";

        List<RelationUserOwner> relationList = jdbcTemplate.query(sql, new Object[]{relationUserOwner.getOwnerId(), relationUserOwner.getUserId()},
                (resultSet, i) -> {
                    UUID userId = UUID.fromString(resultSet.getString("user_id"));
                    Integer ownerId = resultSet.getInt("owner_id");
                    return new RelationUserOwner(userId, ownerId);
                });
        return relationList.size() > 0;
    }

    @Override
    public void deleteRelationUserOwner(RelationUserOwner relationUserOwner) {
        String sql = "DELETE FROM user_owner_relation WHERE user_id = ? AND owner_id = ? ";
        jdbcTemplate.update(sql, relationUserOwner.getUserId(), relationUserOwner.getOwnerId());
    }
}
