package com.example.itLandschapApi.dto;

import org.springframework.http.HttpStatus;

public class CustomUnauthenticatedRuntimeException extends RuntimeException {
    private final HttpStatus statusCode;

    public CustomUnauthenticatedRuntimeException(String message, HttpStatus statusCode) {
        super(message);
        this.statusCode = statusCode;
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }
}

