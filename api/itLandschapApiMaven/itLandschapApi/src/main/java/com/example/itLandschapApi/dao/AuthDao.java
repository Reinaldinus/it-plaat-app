package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.User;
import com.example.itLandschapApi.requestmodel.LoginRequestModel;

import java.util.Optional;
import java.util.UUID;

public interface AuthDao {

    public UUID createUser(User user);

    public Optional<User> findUserByUsername(String username);

    public Optional<User> selectUserByEmail(String email);
}
