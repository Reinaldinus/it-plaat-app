package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.Application;
import com.example.itLandschapApi.model.RelationApplicationTodo;
import com.example.itLandschapApi.model.ToDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository("todos")
public class ToDoDataAccessService implements ToDoDao {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ToDoDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertToDo(ToDo toDo) {

        String sql = "INSERT INTO to_do (title, comment, deadline, is_done) " +
                "VALUES (?, ?, ?, ?);  ";
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(con -> {
            PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, toDo.getTitle());
            ps.setString(2, toDo.getComment());
            ps.setDate(3, toDo.getDeadline());
            ps.setBoolean(4, toDo.isItDone());

            return ps;
        }, keyHolder);

        Map<String, Object> keys = keyHolder.getKeys();
        Integer generatedId = (Integer) keys.get("id");
        return generatedId;

    }

    @Override
    public List<ToDo> selectToDosByAppId(Integer appId) {
        String sql = "SELECT id, title, comment, deadline, is_done FROM to_do " +
                "JOIN application_todo_relation ON to_do.id = to_do_id WHERE application_id = ? ;";

        return jdbcTemplate.query(sql, new Object[]{appId}, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");
            String title = resultSet.getString("title");
            String comment = resultSet.getString("comment");
            Date deadline = resultSet.getDate("deadline");
            boolean isDone = resultSet.getBoolean("is_done");

            ToDo newTodo = new ToDo(id, title, comment, deadline);

            if (isDone == true) {
                newTodo.completeTask();
            }

            return newTodo;
        });
    }

    @Override
    public List<ToDo> selectToDosByBeheerderId(Integer beheerderId) {
            String sql = "SELECT id, title, comment, deadline, is_done FROM to_do " +
                    "JOIN beheerder_todo_relation ON to_do.id = to_do_id " +
                    "WHERE beheerder_todo_relation.beheerder_id = ? ";


        return jdbcTemplate.query(sql, new Object[]{beheerderId}, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");
            String title = resultSet.getString("title");
            String comment = resultSet.getString("comment");
            Date deadline = resultSet.getDate("deadline");
            boolean isDone = resultSet.getBoolean("is_done");

            ToDo newTodo = new ToDo(id, title, comment, deadline);

            if (isDone == true) {
                newTodo.completeTask();
            }

            return newTodo;

        });


    }




    @Override
    public int deleteToDoById(Integer id) {
        String sql = "DELETE FROM to_do WHERE id = ? ";
        jdbcTemplate.update(sql, id);
        return 0;
    }

    @Override
    public int updateToDoById(Integer id, ToDo toDo) {
        String sql = "UPDATE to_do SET title = ?, " +
                "comment = ?, deadline = ?," +
                "is_done = ? " +
                "WHERE id = ? ;";
        jdbcTemplate.update(sql, toDo.getTitle(), toDo.getComment(), toDo.getDeadline(), toDo.isItDone(), id);
        return 0;

    }

    @Override
    public int markToDoDone(Integer toDoId) {
        String sql = "UPDATE to_do SET is_done = true " +
                "WHERE id = ?;";
        jdbcTemplate.update(sql, toDoId);
        return 0;
    }

    @Override
    public int markToDoUndone(Integer toDoId) {
        String sql = "UPDATE to_do SET is_done = false " +
                "WHERE id = ?;";
        jdbcTemplate.update(sql, toDoId);
        return 0;

    }

}

