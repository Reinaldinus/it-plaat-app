package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.Owner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.*;

@Repository("owners")
public class OwnerDataAccessService implements OwnerDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public OwnerDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertOwner(Owner owner) {
        String sql = "INSERT INTO owners (name) VALUES (?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(con ->{
            PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, owner.getName());

            return ps;
        }, keyHolder);

        Map<String, Object> keys = keyHolder.getKeys();
        Integer generatedId = (Integer) keys.get("id");
        return generatedId;
    }

    @Override
    public List<Owner> selectAllOwners(UUID userId) {
        final String sql = "SELECT * FROM owners JOIN user_owner_relation ON " +
                "owners.id = user_owner_relation.owner_id WHERE " +
                "user_owner_relation.user_id = ? ";
        return jdbcTemplate.query(sql,  new Object[]{userId}, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("name");


            return new Owner(id, name);
        });


    }

    @Override
    public Optional<Owner> selectOwnerById(Integer id) {
        String sql = "SELECT * FROM owners WHERE id = ? ";
        List<Owner> listOwner =   jdbcTemplate.query(sql, new Object[]{id}, (resultSet, i) -> {
            String name = resultSet.getString("name");
            return new Owner(id, name);
        });
        return listOwner.size() == 0 ? Optional.empty() : Optional.of(listOwner.get(0));

    }

    @Override
    public Optional<Owner> selectOwnerByName(String name, UUID userId) {
        final String sql = "SELECT * FROM owners JOIN user_owner_relation ON " +
                "owners.id = user_owner_relation.owner_id WHERE " +
                "user_owner_relation.user_id = ? AND owners.name = ? ";
        List<Owner> listOwner = jdbcTemplate.query(sql,  new Object[]{userId, name}, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");


            return new Owner(id, name);
        });
        return listOwner.size() == 0 ? Optional.empty() : Optional.of(listOwner.get(0));

    }

    @Override
    public int deleteOwnerById(Integer id) {
        String sql = "DELETE FROM owners WHERE id = ? ";
        jdbcTemplate.update(sql, id);
        return 0;
    }

    @Override
    public int updateOwnerById(Integer id, Owner owner) {
        String sql = "UPDATE owners SET name = ? WHERE id = ?;";
        jdbcTemplate.update(sql, owner.getName(), id);
        return 0;
    }
}