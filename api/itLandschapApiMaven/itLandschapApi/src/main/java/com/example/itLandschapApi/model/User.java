package com.example.itLandschapApi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.lang.NonNull;
import org.springframework.validation.annotation.Validated;


import java.util.UUID;
@ConfigurationProperties
@Validated
public class User {
    private final UUID id;
    @NotBlank
    private final String username;
    @Email
    @NotBlank
    private final String email;
    @NotBlank
    private String password;



    public User(@JsonProperty("id") UUID id,
                     @JsonProperty("username")  String username,
                     @JsonProperty("email") String email,
                     @JsonProperty("password") String password
    ){
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
    }


    public UUID getId() {
        return id;
    }

    public String getUsername(){ return username; };
    public String getEmail(){ return email; };
    public String getPassword(){ return password; };

    public void setPassword(String password){ this.password = password;}

}