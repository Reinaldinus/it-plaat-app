package com.example.itLandschapApi.service;

import com.example.itLandschapApi.dao.ApplicationDao;
import com.example.itLandschapApi.dao.ProductOwnerDao;
import com.example.itLandschapApi.dao.RelationUserPODao;
import com.example.itLandschapApi.dto.CustomRepetitionException;
import com.example.itLandschapApi.dto.CustomUnauthenticatedRuntimeException;
import com.example.itLandschapApi.model.Application;
import com.example.itLandschapApi.model.ProductOwner;
import com.example.itLandschapApi.model.RelationUserPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductOwnerService {
    private final ProductOwnerDao productOwnerDao;
    private final RelationUserPODao relationUserPODao;
    private final ApplicationDao applicationDao;


    @Autowired
    public ProductOwnerService(@Qualifier("productOwners")ProductOwnerDao productOwnerDao,
                               @Qualifier("user-po") RelationUserPODao relationUserPODao,
                               @Qualifier("postgres") ApplicationDao applicationDao){
        this.productOwnerDao = productOwnerDao;
        this.relationUserPODao = relationUserPODao;
        this.applicationDao = applicationDao;
    }

    public int addProductOwner(ProductOwner productOwner, String userId){
        //Check if there is a po with the same name
        Optional<ProductOwner> existingPO = productOwnerDao.selectProductOwnerByName(productOwner.getName(), UUID.fromString(userId));
        if(existingPO.isPresent()) {
            throw new CustomRepetitionException("There is already a po with this name. You can't create two po with the same name", HttpStatus.CONFLICT);
        }

        Integer poId = productOwnerDao.insertProductOwner(productOwner);
        relationUserPODao.insertRelationUserPO(new RelationUserPO(UUID.fromString(userId), poId));
        return poId;
    }

    public List<ProductOwner> getAllProductOwners(String userId){
        return productOwnerDao.selectAllProductOwners(UUID.fromString(userId));
    }

    public Optional<ProductOwner> getProductOwnerById(Integer id, String userId){
        if (!relationUserPODao.checkIfPOBelongsToUser(new RelationUserPO(UUID.fromString(userId), id))){
            throw new CustomUnauthenticatedRuntimeException("This product owner doesn't exist or you don't have permission.", HttpStatus.UNAUTHORIZED);
        }

        return productOwnerDao.selectProductOwnerById(id);
    }


    public void deleteProductOwner(Integer id, String userId){
        RelationUserPO receivedRelation = new RelationUserPO(UUID.fromString(userId), id);

        if (!relationUserPODao.checkIfPOBelongsToUser(new RelationUserPO(UUID.fromString(userId), id))){
            throw new CustomUnauthenticatedRuntimeException("This product owner doesn't exist or you don't have permission.", HttpStatus.UNAUTHORIZED);
        }

        //Check if PO has apps and then complain
        List<Application> list = applicationDao.findApplicationsByPOId(id, UUID.fromString(userId));
        if(list.size() > 0){
            String errorMessage = "";
            for(int i = 0; i < list.size(); i++){
                errorMessage = errorMessage + list.get(i).getAppName();
                if(i != list.size()-1){
                    errorMessage = errorMessage + ", ";
                }
            }
            throw new CustomRepetitionException("This po has the following apps associated with it: "+ errorMessage + ". Please delete them or associate it with another po before.", HttpStatus.CONFLICT);
        }

        productOwnerDao.deleteProductOwnerById(id);
        relationUserPODao.deleteRelationUserPO(receivedRelation);
    }

    public int updateProductOwner(Integer id, ProductOwner productOwner, String userId){
        //Check if there is a po with the same name
        Optional<ProductOwner> existingPO = productOwnerDao.selectProductOwnerByName(productOwner.getName(), UUID.fromString(userId));
        if(existingPO.isPresent() && !existingPO.get().getId().equals(id)) {
            throw new CustomRepetitionException("There is already a po with this name. You can't create two po with the same name", HttpStatus.CONFLICT);
        }
        if (!relationUserPODao.checkIfPOBelongsToUser(new RelationUserPO(UUID.fromString(userId), id))){
            throw new CustomUnauthenticatedRuntimeException("This product owner doesn't exist or you don't have permission.", HttpStatus.UNAUTHORIZED);
        }
        return productOwnerDao.updateProductOwnerById(id, productOwner);
    }
}
