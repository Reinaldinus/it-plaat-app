package com.example.itLandschapApi.dto;

import org.springframework.http.HttpStatus;

public class CustomWrongEmailException extends RuntimeException {
    private final HttpStatus statusCode;

    public CustomWrongEmailException(String message, HttpStatus statusCode) {
        super(message);
        this.statusCode = statusCode;
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }
}

