package com.example.itLandschapApi.service;

import com.example.itLandschapApi.dao.*;
import com.example.itLandschapApi.dto.CustomUnauthenticatedRuntimeException;
import com.example.itLandschapApi.model.*;
import com.example.itLandschapApi.requestmodel.ToDosRequestModel;
import com.example.itLandschapApi.responsemodels.ToDosResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class ToDoService {

    private final ToDoDao toDoDao;
    private final RelationApplicationToDoDao relationApplicationToDoDao;
    private final RelationBeheerderToDoDao relationBeheerderToDoDao;
    private final RelationUserApplicationDao relationUserApplicationDao;
    private final RelationUserBeheerderDao relationUserBeheerderDao;

    @Autowired
    public ToDoService(@Qualifier("todos") ToDoDao toDoDao,
                       @Qualifier("app-todo") RelationApplicationToDoDao relationApplicationToDoDao,
                       @Qualifier("relation_beheerder_todo") RelationBeheerderToDoDao relationBeheerderToDoDao,
                       @Qualifier("user-application") RelationUserApplicationDao relationUserApplicationDao,
                       @Qualifier("user-beheerder") RelationUserBeheerderDao relationUserBeheerderDao) {
        this.toDoDao = toDoDao;
        this.relationApplicationToDoDao = relationApplicationToDoDao;
        this.relationBeheerderToDoDao = relationBeheerderToDoDao;
        this.relationUserApplicationDao = relationUserApplicationDao;
        this.relationUserBeheerderDao = relationUserBeheerderDao;
    }

    public int addToDo(ToDosRequestModel toDosRequestModel, Integer appId, String userId) {
        //Check if this application is associated with your user,
        if(!relationUserApplicationDao.checkIfAppBelongsToUser(new RelationUserApplication(UUID.fromString(userId), appId))){
            throw new CustomUnauthenticatedRuntimeException("You don't have permission to modify the to dos of this application", HttpStatus.UNAUTHORIZED);
        }

        //I insert a to do
        ToDo toDo = toDosRequestModel.toToDo();
        int toDoId = toDoDao.insertToDo(toDo);
        RelationApplicationTodo relationApplicationTodo = new RelationApplicationTodo(appId, toDoId);
        relationApplicationToDoDao.insertRelationApplicationToDo(relationApplicationTodo);
        for (Beheerder beheerder : toDosRequestModel.getBeheerders()) {
            //check if the beheerder belongs to user
            if(!relationUserBeheerderDao.checkIfBeheerderBelongsToUser(new RelationUserBeheerder(UUID.fromString(userId), beheerder.getId()))){
                throw new CustomUnauthenticatedRuntimeException("Application added but you don't have permission link it to this beheerder", HttpStatus.UNAUTHORIZED);
            }
            RelationBeheerderToDo relationBeheerderToDo = new RelationBeheerderToDo(beheerder.getId(), toDoId);
            relationBeheerderToDoDao.insertRelationBeheerderToDo(relationBeheerderToDo);
        }

        return toDoId;
    }


    public List<ToDosResponseModel> getToDosByAppId(Integer appId, String userId) {
        //Check if this application is associated with your user,
        if(!relationUserApplicationDao.checkIfAppBelongsToUser(new RelationUserApplication(UUID.fromString(userId), appId))){
            throw new CustomUnauthenticatedRuntimeException("You don't have permission to modify the to dos of this application", HttpStatus.UNAUTHORIZED);
        }
        //I get the To Dos
        List<ToDo> toDoList = toDoDao.selectToDosByAppId(appId);
        List<ToDosResponseModel> resultList = new ArrayList<>();
        for (ToDo todo : toDoList) {

            List<Beheerder> beheerderList = relationBeheerderToDoDao.selectBeheerdersOfToDoByToDoId(todo.getId());
            ToDosResponseModel responseModel = new ToDosResponseModel(todo.getId(), todo.getTitle(), todo.getComment(), todo.getDeadline(), todo.isItDone(), beheerderList);
            resultList.add(responseModel);
        }

        return resultList;
    }

    public List<ToDo> getToDosByBeheerderId(Integer beheerderId, String userId) {
        if(!relationUserBeheerderDao.checkIfBeheerderBelongsToUser(new RelationUserBeheerder(UUID.fromString(userId), beheerderId))){
            throw new CustomUnauthenticatedRuntimeException("You don't have permission to modify the to dos of this beheerder", HttpStatus.UNAUTHORIZED);
        }
        return toDoDao.selectToDosByBeheerderId(beheerderId);
    }

    public int deleteToDo(Integer toDoId, String userId) {
        //Check if this application is associated with your user,
        Integer appId = relationApplicationToDoDao.getIdOfApplicationAssociatedWithThisToDo(toDoId);
        if(appId == null){
            throw new CustomUnauthenticatedRuntimeException("This to do is not associated to any app. You can't modify it", HttpStatus.UNAUTHORIZED);
        }
        if(!relationUserApplicationDao.checkIfAppBelongsToUser(new RelationUserApplication(UUID.fromString(userId), appId))){
            throw new CustomUnauthenticatedRuntimeException("Application updated but you don't have permission link it to this beheerder", HttpStatus.UNAUTHORIZED);
        }
        toDoDao.deleteToDoById(toDoId);
        relationApplicationToDoDao.deleteApplicationToDoRelationByToDoId(toDoId);
        relationBeheerderToDoDao.deleteRelationBeheerderToDoByToDoId(toDoId);
        return 0;
    }

    public int updateToDo(Integer toDoId, ToDosRequestModel toDosRequestModel, String userId) {
        //check if app is associated with to do.
        Integer appId = relationApplicationToDoDao.getIdOfApplicationAssociatedWithThisToDo(toDoId);
        if(appId == null){
            throw new CustomUnauthenticatedRuntimeException("This to do is not associated to any app. You can't modify it", HttpStatus.UNAUTHORIZED);
        }
        if(!relationUserApplicationDao.checkIfAppBelongsToUser(new RelationUserApplication(UUID.fromString(userId), appId))){
            throw new CustomUnauthenticatedRuntimeException("Application updated but you don't have permission link it to this beheerder", HttpStatus.UNAUTHORIZED);
        }

        //update todo
        ToDo toDo = toDosRequestModel.toToDo();
        //update Todo
        toDoDao.updateToDoById(toDoId, toDo);
        System.out.println(toDo.getComment());
        //get the list of beheerders that are new (where not in the db).
        List<Beheerder> oldBeheerders =  relationBeheerderToDoDao.selectBeheerdersOfToDoByToDoId(toDoId);
        List<Beheerder> beheerdersToAdd = selectNewBeheerdersToAdd(toDosRequestModel.getBeheerders(), oldBeheerders);
        //Add the new relations beheerder to do to DB
        for(Beheerder beheerder: beheerdersToAdd){
            //Check if the beheerder is associated with the user
            if(!relationUserBeheerderDao.checkIfBeheerderBelongsToUser(new RelationUserBeheerder(UUID.fromString(userId), beheerder.getId()))){
                throw new CustomUnauthenticatedRuntimeException("You don't have permission to modify the to dos of this beheerder", HttpStatus.UNAUTHORIZED);
            }
            RelationBeheerderToDo relationBeheerderToDo = new RelationBeheerderToDo(beheerder.getId(), toDoId);
            relationBeheerderToDoDao.insertRelationBeheerderToDo(relationBeheerderToDo);
        }
        //get the list of beheerders that were removed.
        List<Beheerder> beheerdersToRemove = selectBeheerdersToDelete(toDosRequestModel.getBeheerders(), oldBeheerders);
        //Add the beheerders to remove from the relation db
        for(Beheerder beheerder: beheerdersToRemove){
            //Check if the beheerder is associated with the user.
            if(!relationUserBeheerderDao.checkIfBeheerderBelongsToUser(new RelationUserBeheerder(UUID.fromString(userId), beheerder.getId()))){
                throw new CustomUnauthenticatedRuntimeException("You don't have permission to modify the to dos of this beheerder", HttpStatus.UNAUTHORIZED);
            }
            relationBeheerderToDoDao.deleteAllRelationBeheerderToDoByBeheerderAndToDoId(beheerder.getId(), toDoId);
        }
        return toDoId;
    }


    //get beheeerders ids
    //loop through them to see if which one we add and which one we remove.


    public int markToDoDone(Integer toDoId, String userId) {
        Integer appId = relationApplicationToDoDao.getIdOfApplicationAssociatedWithThisToDo(toDoId);
        if(appId == null){
            throw new CustomUnauthenticatedRuntimeException("This to do is not associated to any app. You can't modify it", HttpStatus.UNAUTHORIZED);
        }
        if(!relationUserApplicationDao.checkIfAppBelongsToUser(new RelationUserApplication(UUID.fromString(userId), appId))){
            throw new CustomUnauthenticatedRuntimeException("You don't have permission to modify the to dos of this application", HttpStatus.UNAUTHORIZED);
        }
        return toDoDao.markToDoDone(toDoId);
    }

    public int markToDoUndone(Integer toDoId, String userId) {
        Integer appId = relationApplicationToDoDao.getIdOfApplicationAssociatedWithThisToDo(toDoId);
        if(appId == null){
            throw new CustomUnauthenticatedRuntimeException("This to do is not associated to any app. You can't modify it", HttpStatus.UNAUTHORIZED);
        }
        if(!relationUserApplicationDao.checkIfAppBelongsToUser(new RelationUserApplication(UUID.fromString(userId), appId))){
            throw new CustomUnauthenticatedRuntimeException("You don't have permission to modify the to dos of this application", HttpStatus.UNAUTHORIZED);
        }
        return toDoDao.markToDoUndone(toDoId);
    }

    private List<Beheerder> selectNewBeheerdersToAdd(List<Beheerder> newBeheerders, List<Beheerder> oldBeheerders) {
        //If for some reason the to do was empty we add eveyone.
        List<Beheerder> resultList = new ArrayList<>();
        if (oldBeheerders.isEmpty()){
            System.out.println("here");
            for(Beheerder beheerder: newBeheerders){
                resultList.add(beheerder);
            }
        }
        for (int i = 0; i < newBeheerders.size(); i++) {
            for (int x = 0; x < oldBeheerders.size(); x++) {
                if (newBeheerders.get(i).getId() == oldBeheerders.get(x).getId()) {
                    break;
                } else if (x == oldBeheerders.size() - 1) {
                    resultList.add(newBeheerders.get(i));
                }

            }
        }
        return resultList;
    }

    private List<Beheerder> selectBeheerdersToDelete(List<Beheerder> newBeheerders, List<Beheerder> oldBeheerders) {
        List<Beheerder> resultList = new ArrayList<>();
        for (int i = 0; i < oldBeheerders.size(); i++) {
            for (int x = 0; x < newBeheerders.size(); x++) {
                if (oldBeheerders.get(i).getId() == newBeheerders.get(x).getId()) {
                    break;
                } else if (x == newBeheerders.size() - 1) {
                    resultList.add(oldBeheerders.get(i));
                }

            }
        }
        return resultList;
    }
}
