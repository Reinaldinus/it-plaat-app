package com.example.itLandschapApi.service;

import com.example.itLandschapApi.dao.ApplicationDao;
import com.example.itLandschapApi.dao.RelationUserServerDao;
import com.example.itLandschapApi.dao.ServerDao;
import com.example.itLandschapApi.dto.CustomRepetitionException;
import com.example.itLandschapApi.dto.CustomUnauthenticatedRuntimeException;
import com.example.itLandschapApi.model.Application;
import com.example.itLandschapApi.model.RelationUserServer;
import com.example.itLandschapApi.model.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ServerService {

    private final ServerDao serverDao;
    private final RelationUserServerDao relationUserServerDao;
    private final ApplicationDao applicationDao;

    @Autowired
    public ServerService(@Qualifier("servers")ServerDao serverDao,
                         @Qualifier("user-server")RelationUserServerDao relationUserServerDao,
                         @Qualifier("postgres") ApplicationDao applicationDao){
        this.serverDao = serverDao;
        this.relationUserServerDao = relationUserServerDao;
        this.applicationDao = applicationDao;
    }

    public int addServer(Server server, String userId){
        //Check if there is a po with the same name
        Optional<Server> existingServer = serverDao.selectServerByName(server.getName(), UUID.fromString(userId));
        if(existingServer.isPresent()) {
            throw new CustomRepetitionException("There is already a server with this name. You can't create two server with the same name", HttpStatus.CONFLICT);
        }
        Integer serverId = serverDao.insertServer(server);
        relationUserServerDao.insertRelationUserServer(new RelationUserServer(UUID.fromString(userId), serverId));
        return serverId;
    }

    public List<Server> getAllServers(String userId){
        return serverDao.selectAllServers(UUID.fromString(userId));
    }

    public void deleteServer(Integer id, String userId){
        RelationUserServer receivedRelation = new RelationUserServer(UUID.fromString(userId), id);

        if (!relationUserServerDao.checkIfServerBelongsToUser(new RelationUserServer(UUID.fromString(userId), id))){
            throw new CustomUnauthenticatedRuntimeException("This server doesn't exist or you don't have permission.", HttpStatus.UNAUTHORIZED);
        }
        //Check if Server has apps and then complain
        List<Application> list = applicationDao.findApplicationsByServerId(id, UUID.fromString(userId));
        if(list.size() > 0){
            String errorMessage = "";
            for(int i = 0; i < list.size(); i++){
                errorMessage = errorMessage + list.get(i).getAppName();
                if(i != list.size()-1){
                    errorMessage = errorMessage + ", ";
                }
            }
            throw new CustomRepetitionException("This server has the following apps associated with it: "+ errorMessage + ". Please delete them or associate it with another server before.", HttpStatus.CONFLICT);
        }
        serverDao.deleteServerById(id);
        relationUserServerDao.deleteRelationUserServer(receivedRelation);
    }

    public int updateServer(Integer id, Server server,String userId){
        //Check if there is a po with the same name
        Optional<Server> existingServer = serverDao.selectServerByName(server.getName(), UUID.fromString(userId));
        if(existingServer.isPresent() && !existingServer.get().getId().equals(id)) {
            throw new CustomRepetitionException("There is already a server with this name. You can't create two server with the same name", HttpStatus.CONFLICT);
        }
        if (!relationUserServerDao.checkIfServerBelongsToUser(new RelationUserServer(UUID.fromString(userId), id))){
            throw new CustomUnauthenticatedRuntimeException("This server doesn't exist or you don't have permission.", HttpStatus.UNAUTHORIZED);
        }
        return serverDao.updateServerById(id, server);
    }
}
