package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.ServiceLevelAgreement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.*;

@Repository("slas")

public class ServiceLevelAgreementDataAccessService implements ServiceLevelAgreementDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ServiceLevelAgreementDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertServiceLevelAgreement(ServiceLevelAgreement serviceLevelAgreement) {
        String sql = "INSERT INTO service_level_agreement (name) VALUES (?)";

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(con ->{
            PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1,serviceLevelAgreement.getName());

            return ps;
        }, keyHolder);

        Map<String, Object> keys = keyHolder.getKeys();
        Integer generatedId = (Integer) keys.get("id");
        return generatedId;

    }

    @Override
    public List<ServiceLevelAgreement> selectAllServiceLevelAgreements(UUID userId) {
        final String sql = "SELECT * FROM service_level_agreement JOIN user_sla_relation ON " +
                "service_level_agreement.id = user_sla_relation.sla_id WHERE " +
                "user_sla_relation.user_id = ? ;";
        return jdbcTemplate.query(sql,new Object[]{userId}, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("name");


            return new ServiceLevelAgreement(id, name);
        });

    }


    @Override
    public Optional<ServiceLevelAgreement> selectServiceLevelAgreementById(Integer id) {
        String sql = "SELECT * FROM service_level_agreement WHERE id = ? ";
        //Specifying the values of it's parameter

        List<ServiceLevelAgreement> listSLA = jdbcTemplate.query(sql, new Object[]{id}, (resultSet, i) -> {
            String name = resultSet.getString("name");

            return new ServiceLevelAgreement(id, name);
        });
        return listSLA.size() == 0 ? Optional.empty() : Optional.of(listSLA.get(0));

}

    @Override
    public Optional<ServiceLevelAgreement> selectServiceLevelAgreementByName(String name, UUID userId) {
        final String sql = "SELECT * FROM service_level_agreement JOIN user_sla_relation ON " +
                "service_level_agreement.id = user_sla_relation.sla_id WHERE " +
                "user_sla_relation.user_id = ? AND service_level_agreement.name = ? ;";

        List<ServiceLevelAgreement> listSLA = jdbcTemplate.query(sql, new Object[]{userId, name}, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");

            return new ServiceLevelAgreement(id, name);
        });
        return listSLA.size() == 0 ? Optional.empty() : Optional.of(listSLA.get(0));
    }

    @Override
    public int deleteServiceLevelAgreementById(Integer id) {
        String sql = "DELETE FROM service_level_agreement WHERE id = ? ";
        jdbcTemplate.update(sql, id);
        return 0;

    }

    @Override
    public int updateServiceLevelAgreementById(Integer id, ServiceLevelAgreement serviceLevelAgreement) {
        String sql = "UPDATE service_level_agreement SET name = ? " + "WHERE id = ?;";
        jdbcTemplate.update(sql, serviceLevelAgreement.getName(), id);
        return 0;


    }
}
