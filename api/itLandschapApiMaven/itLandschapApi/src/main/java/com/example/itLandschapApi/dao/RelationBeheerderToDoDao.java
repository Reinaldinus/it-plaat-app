package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.Beheerder;
import com.example.itLandschapApi.model.RelationBeheerderToDo;

import java.util.List;

public interface RelationBeheerderToDoDao {

    int insertRelationBeheerderToDo(RelationBeheerderToDo relationBeheerderToDo);

    List<RelationBeheerderToDo> selectAllRelationBeheerderToDo();

    List<RelationBeheerderToDo> selectRelationBeheerderToDoByBeheerderId(Integer beheerderId);
    List<Beheerder> selectBeheerdersOfToDoByToDoId(Integer toDoId);

    int deleteAllRelationBeheerderToDoByBeheerderAndToDoId(Integer beheerderId, Integer toDoId);


    int deleteRelationBeheerderToDoByToDoId(Integer toDoId);

    int deleteAllRelationBeheerderToDoByBeheerderId(Integer beheerderId);

    int updateRelationBeheerderToDoByBeheerderAndToDoId(Integer beheerderId, Integer toDoId);
}
