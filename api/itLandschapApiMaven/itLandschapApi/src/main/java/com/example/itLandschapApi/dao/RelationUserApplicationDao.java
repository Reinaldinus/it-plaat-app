package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.RelationUserApplication;

public interface RelationUserApplicationDao {

    void insertRelationUserApplication(RelationUserApplication relationUserApplication);

    boolean checkIfAppBelongsToUser(RelationUserApplication relationUserApplication);

    void deleteRelationUserApplication(RelationUserApplication relationUserApplication);
}
