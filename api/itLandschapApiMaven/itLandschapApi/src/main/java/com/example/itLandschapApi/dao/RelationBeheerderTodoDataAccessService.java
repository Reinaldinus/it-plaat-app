package com.example.itLandschapApi.dao;


import com.example.itLandschapApi.model.Beheerder;
import com.example.itLandschapApi.model.RelationBeheerderToDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("relation_beheerder_todo")

public class RelationBeheerderTodoDataAccessService implements RelationBeheerderToDoDao {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public RelationBeheerderTodoDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertRelationBeheerderToDo(RelationBeheerderToDo relationBeheerderToDo) {
        String sql = "INSERT INTO beheerder_todo_relation (beheerder_id, to_do_id) " +
                "VALUES (?, ?)";
        jdbcTemplate.update(sql, relationBeheerderToDo.getBeheerderId(), relationBeheerderToDo.getToDoId());
//Specifying the values of it's parameter
        return 0;
    }

    @Override
    public List<RelationBeheerderToDo> selectAllRelationBeheerderToDo() {
        final String sql = "SELECT * FROM beheerder_todo_relation";
        return jdbcTemplate.query(sql, (resultSet, i) -> {
            Integer beheerderId = resultSet.getInt("beheerder_id");
            Integer toDoId = resultSet.getInt("to_do_id");


            return new RelationBeheerderToDo(beheerderId, toDoId);
        });
    }

    @Override
    public List<RelationBeheerderToDo> selectRelationBeheerderToDoByBeheerderId(Integer beheerderId) {
        String sql = "SELECT * FROM beheerder_todo_relation WHERE beheerder_id = ? ";

        return jdbcTemplate.query(sql, new Object[]{beheerderId}, (resultSet, i) -> {
            Integer toDoId = resultSet.getInt("to_do_id");


            return new RelationBeheerderToDo(beheerderId, toDoId);
        });


    }

    @Override
    public List<Beheerder> selectBeheerdersOfToDoByToDoId(Integer toDoId) {
        String sql = "SELECT * FROM beheerder_todo_relation JOIN beheerders " +
                "ON beheerders.id = beheerder_id WHERE to_do_id = ? ";

        return jdbcTemplate.query(sql, new Object[]{toDoId}, (resultSet, i) -> {

            Integer beheerderId = resultSet.getInt("id");
            String name = resultSet.getString("name");

            return new Beheerder(beheerderId, name);
        });
    }


    @Override
    public int deleteAllRelationBeheerderToDoByBeheerderAndToDoId(Integer beheerderId, Integer toDoId) {
        String sql = "DELETE FROM beheerder_todo_relation WHERE beheerder_id = ? AND to_do_id = ? ; ";
        jdbcTemplate.update(sql, beheerderId, toDoId);
        return 0;
    }

    @Override
    public int deleteRelationBeheerderToDoByToDoId(Integer toDoId) {
        String sql = "DELETE FROM beheerder_todo_relation WHERE to_do_id = ? ; ";
        jdbcTemplate.update(sql, toDoId);
        return 0;
    }

    @Override
    public int deleteAllRelationBeheerderToDoByBeheerderId(Integer beheerderId) {
        String sql = "DELETE FROM beheerder_todo_relation WHERE beheerder_id = ? ; ";
        jdbcTemplate.update(sql, beheerderId);
        return 0;
    }

    @Override
    public int updateRelationBeheerderToDoByBeheerderAndToDoId(Integer beheerderId, Integer toDoId) {
        return 0;
    }
}
