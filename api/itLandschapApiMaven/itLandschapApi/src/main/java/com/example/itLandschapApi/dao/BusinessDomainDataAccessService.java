package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.BusinessDomain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Repository("domains")
public class BusinessDomainDataAccessService implements BusinessDomainDao {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public BusinessDomainDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertBusinessDomain(BusinessDomain businessDomain) {
        String sql = "INSERT INTO business_domain (name) VALUES (?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(con ->{
            PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1,businessDomain.getName());

            return ps;
        }, keyHolder);

        Map<String, Object> keys = keyHolder.getKeys();
        Integer generatedId = (Integer) keys.get("id");
        return generatedId;

    }


    @Override
    public List<BusinessDomain> selectAllBusinessDomains(UUID userId) {
        final String sql = "SELECT * FROM business_domain JOIN user_bd_relation ON " +
                "business_domain.id = user_bd_relation.bd_id WHERE " +
                "user_bd_relation.user_id = ? ";
        return jdbcTemplate.query(sql, new Object[]{userId}, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("name");


            return new BusinessDomain(id, name);
        });

    }

    @Override
    public Optional<BusinessDomain> selectBusinessDomainById(Integer id) {
        String sql = "SELECT * FROM business_domain WHERE id = ? ";
        //Specifying the values of it's parameter

        List<BusinessDomain> listBD = jdbcTemplate.query(sql, new Object[]{id}, (resultSet, i) -> {
            String name = resultSet.getString("name");

            return new BusinessDomain(id, name);
        });
        return listBD.size() == 0 ? Optional.empty() : Optional.of(listBD.get(0));
    }

    @Override
    public Optional<BusinessDomain> findBusinessDomainByName(String name, UUID userId) {
        final String sql = "SELECT * FROM business_domain JOIN user_bd_relation ON " +
                "business_domain.id = user_bd_relation.bd_id WHERE " +
                "user_bd_relation.user_id = ? AND business_domain.name = ? ";

        List<BusinessDomain> listBD = jdbcTemplate.query(sql, new Object[]{userId, name}, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");

            return new BusinessDomain(id, name);
        });
        return listBD.size() == 0 ? Optional.empty() : Optional.of(listBD.get(0));
    }

    @Override
    public int deleteBusinessDomainById(Integer id) {
        String sql = "DELETE FROM business_domain WHERE id = ? ";
        jdbcTemplate.update(sql, id);
        return 0;
    }

    @Override
    public int updateBusinessDomainById(Integer id, BusinessDomain businessDomain) {
        String sql = "UPDATE business_domain SET name = ? WHERE id = ?;";
        jdbcTemplate.update(sql, businessDomain.getName(), id);
        return 0;

    }
}

