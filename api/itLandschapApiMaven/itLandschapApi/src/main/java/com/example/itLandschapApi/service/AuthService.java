package com.example.itLandschapApi.service;

import com.example.itLandschapApi.dao.AuthDao;
import com.example.itLandschapApi.dto.CustomRepetitionException;
import com.example.itLandschapApi.dto.CustomUnauthenticatedRuntimeException;
import com.example.itLandschapApi.model.User;
import com.example.itLandschapApi.requestmodel.LoginRequestModel;
import com.example.itLandschapApi.responsemodels.LoginResponseModel;
import com.example.itLandschapApi.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.mindrot.jbcrypt.BCrypt;


import java.util.Optional;

@Service
public class AuthService {
    private final AuthDao authDao;
    private static final int SALT_LENGTH = 16;
    private final JwtTokenProvider jwtTokenProvider;



    @Autowired
    public AuthService(@Qualifier("login") AuthDao authDao, JwtTokenProvider jwtTokenProvider){
        this.authDao = authDao;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    public LoginResponseModel register(User user){
        //Check if there is a user with this email
        Optional<User> foundOptionalUser = authDao.selectUserByEmail(user.getEmail());
        if(foundOptionalUser.isPresent()) {
            throw new CustomRepetitionException("There is already a user with this email", HttpStatus.CONFLICT);
        }
        //Check if there is a user with this username
        Optional<User> foundOptionalUsernameUser = authDao.findUserByUsername(user.getUsername());
        if(foundOptionalUsernameUser.isPresent()) {
            throw new CustomRepetitionException("There is already a user with this username", HttpStatus.CONFLICT);
        }
        //Save the unhashed password to log in.
        String unhashedPassword = user.getPassword();
        user.setPassword(hashPassword(user.getPassword()));
        authDao.createUser(user);
        LoginRequestModel loginRequestModel = new LoginRequestModel(user.getEmail(),unhashedPassword);
        return logIn(loginRequestModel);
    }

    public LoginResponseModel logIn(LoginRequestModel loginRequestModel){
        Optional<User> optionalUser = authDao.selectUserByEmail(loginRequestModel.getEmail());
        User user = null;
        if (optionalUser.isPresent()) {
            user = optionalUser.get();
        }
        if (user == null){
            throw new CustomUnauthenticatedRuntimeException("Invalid email", HttpStatus.UNAUTHORIZED);
        } else if(!checkPassword(loginRequestModel.getPassword(), user.getPassword())){
            throw new CustomUnauthenticatedRuntimeException("Invalid password", HttpStatus.UNAUTHORIZED);
        }


        String token = jwtTokenProvider.generateToken(user.getId().toString());
        return new LoginResponseModel(token);
    }

    private String hashPassword(String password) {
        String salt = BCrypt.gensalt(SALT_LENGTH);
        return BCrypt.hashpw(password, salt);

    }

    private static boolean checkPassword(String password, String hashedPassword) {
        return BCrypt.checkpw(password, hashedPassword);
    }


}
