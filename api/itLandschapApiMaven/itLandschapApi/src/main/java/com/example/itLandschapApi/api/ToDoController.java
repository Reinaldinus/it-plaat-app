package com.example.itLandschapApi.api;

import com.example.itLandschapApi.dto.CustomUnauthenticatedRuntimeException;
import com.example.itLandschapApi.model.ToDo;
import com.example.itLandschapApi.requestmodel.ToDosRequestModel;
import com.example.itLandschapApi.responsemodels.ToDosResponseModel;
import com.example.itLandschapApi.security.JwtTokenProvider;
import com.example.itLandschapApi.service.ToDoService;
import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/v1/todo")
@RestController
public class ToDoController {

    private final ToDoService toDoService;
    private final JwtTokenProvider jwtTokenProvider;


    @Autowired
    public ToDoController (ToDoService toDoService, JwtTokenProvider jwtTokenProvider){
        this.toDoService = toDoService;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @ExceptionHandler(CustomUnauthenticatedRuntimeException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public Error handleCustomRuntimeException(CustomUnauthenticatedRuntimeException ex) {
        System.out.println(ex.getMessage());
        return new Error(ex.getMessage());
    }
    @CrossOrigin
    @PostMapping(path="{application_id}")
    @ResponseBody
    public int addToDo(HttpServletRequest request, @Valid @NonNull @RequestBody ToDosRequestModel toDosRequestModel, @PathVariable("application_id") Integer appId){
        String userId = extractUserIdFromToken(request);

        return toDoService.addToDo(toDosRequestModel, appId, userId);
    }

    @CrossOrigin
    @GetMapping(path="{application_id}")
    public List<ToDosResponseModel> getToDosByAppId(HttpServletRequest request, @PathVariable("application_id") Integer appId) {
        String userId = extractUserIdFromToken(request);

        return toDoService.getToDosByAppId(appId, userId);
    }

    @CrossOrigin
    @GetMapping(path="beheerder/{beheerder_id}")
    public List<ToDo> getToDosByBeheerderId(HttpServletRequest request, @PathVariable("beheerder_id") Integer beheerderId) {
        String userId = extractUserIdFromToken(request);

        return toDoService.getToDosByBeheerderId(beheerderId, userId);
    }

    @CrossOrigin
    @DeleteMapping(path = "{id}")
    public void deleteToDoById(HttpServletRequest request, @PathVariable("id") Integer id){
        String userId = extractUserIdFromToken(request);

        toDoService.deleteToDo(id, userId);
    }

    @CrossOrigin
    @PutMapping(path = "{to_do_id}")
    public void updateToDo(HttpServletRequest request, @PathVariable("to_do_id") Integer toDoId, @Valid @NonNull @RequestBody ToDosRequestModel toDosRequestModel){
        String userId = extractUserIdFromToken(request);

        toDoService.updateToDo(toDoId, toDosRequestModel, userId);
    }

    @CrossOrigin
    @PutMapping(path = "done/{to_do_id}")
    public void markToDoDone(HttpServletRequest request, @PathVariable("to_do_id") Integer toDoId){
        String userId = extractUserIdFromToken(request);

        toDoService.markToDoDone(toDoId, userId);
    }

    @CrossOrigin
    @PutMapping(path = "undo/{to_do_id}")
    public void markToDoUndone(HttpServletRequest request, @PathVariable("to_do_id") Integer toDoId){
        String userId = extractUserIdFromToken(request);

        toDoService.markToDoUndone(toDoId, userId);
    }

    private String extractUserIdFromToken(HttpServletRequest request) {
        String token = request.getHeader("Authorization").replace("Bearer ", "");
        String userId;
        try {
            userId = jwtTokenProvider.getUserIdFromToken(token);
        } catch (ExpiredJwtException ex) {
            throw new CustomUnauthenticatedRuntimeException("The token is expired, please log in again", HttpStatus.UNAUTHORIZED);

        }
        if (!jwtTokenProvider.validateToken(token, userId)) {
            throw new CustomUnauthenticatedRuntimeException("The provided JWT token is invalid or malformed. ", HttpStatus.UNAUTHORIZED);
        }
        return userId;
    }


}


