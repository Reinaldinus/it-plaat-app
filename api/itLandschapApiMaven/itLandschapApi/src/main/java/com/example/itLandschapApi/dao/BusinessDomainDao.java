package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.BusinessDomain;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BusinessDomainDao {

    int insertBusinessDomain(BusinessDomain businessDomain);

    List<BusinessDomain> selectAllBusinessDomains(UUID userId);
    Optional<BusinessDomain> selectBusinessDomainById(Integer id);

    Optional<BusinessDomain> findBusinessDomainByName(String name, UUID userId);

    int deleteBusinessDomainById(Integer id);

    int updateBusinessDomainById(Integer id, BusinessDomain businessDomain);

}
