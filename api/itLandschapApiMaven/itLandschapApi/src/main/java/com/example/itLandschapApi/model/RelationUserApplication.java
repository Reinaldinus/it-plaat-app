package com.example.itLandschapApi.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class RelationUserApplication {


    private final UUID userId;
    private final Integer appId;



    public RelationUserApplication( UUID userId, Integer appId
    ){
        this.userId = userId;
        this.appId = appId;
    }



    public UUID getUserId() {
        return userId;
    }

    public Integer getAppId(){ return appId; };
}
