package com.example.itLandschapApi.model;

import java.util.UUID;

public class RelationUserPO {

    private final UUID userId;
    private final Integer poId;



    public RelationUserPO( UUID userId, Integer poId){
        this.userId = userId;
        this.poId = poId;
    }



    public UUID getUserId() {
        return userId;
    }

    public Integer getPoId(){ return poId; };
}
