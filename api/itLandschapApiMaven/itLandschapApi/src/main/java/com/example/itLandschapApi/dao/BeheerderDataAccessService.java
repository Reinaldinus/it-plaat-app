package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.dao.BeheerderDao;
import com.example.itLandschapApi.model.Beheerder;
import com.example.itLandschapApi.model.BusinessDomain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.*;

@Repository("beheerders")
public class BeheerderDataAccessService implements BeheerderDao {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public BeheerderDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertBeheerder(Beheerder beheerder) {
        String sql = "INSERT INTO beheerders (name) VALUES (?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(con ->{
            PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, beheerder.getName());
            return ps;
        }, keyHolder);

        Map<String, Object> keys = keyHolder.getKeys();
        Integer generatedId = (Integer) keys.get("id");
        return generatedId;
    }

    @Override
    public Optional<Beheerder> selectBeheerderById(Integer id) {
        String sql = "SELECT * FROM beheerders WHERE id = ? ";
        //Specifying the values of it's parameter

        List<Beheerder> listBeheerder = jdbcTemplate.query(sql, new Object[]{id}, (resultSet, i) -> {
            String name = resultSet.getString("name");

            return new Beheerder(id, name);
        });
        return listBeheerder.size() == 0 ? Optional.empty() : Optional.of(listBeheerder.get(0));

    }

    @Override
    public List<Beheerder> selectAllBeheerders(UUID userId) {
        final String sql = "SELECT * FROM beheerders JOIN user_beheerder_relation ON " +
                "beheerders.id = user_beheerder_relation.beheerder_id WHERE user_beheerder_relation.user_id = ? ";
        return jdbcTemplate.query(sql, new Object[]{userId}, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("name");


            return new Beheerder(id, name);
        });
    }

    @Override
    public Optional<Beheerder> findBeheerderByName(UUID userId, String name) {
        String sql = "SELECT * FROM beheerders JOIN user_beheerder_relation ON " +
                "beheerders.id = user_beheerder_relation.beheerder_id WHERE user_beheerder_relation.user_id = ? " +
                "AND beheerders.name = ? ;";
        //Specifying the values of it's parameter

        List<Beheerder> listBeheerder = jdbcTemplate.query(sql, new Object[]{userId, name}, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");

            return new Beheerder(id, name);
        });
        return listBeheerder.size() == 0 ? Optional.empty() : Optional.of(listBeheerder.get(0));
    }

    @Override
    public int deleteBeheerderById(Integer id) {
        String sql = "DELETE FROM beheerders WHERE id = ? ";
        jdbcTemplate.update(sql, id);
        return 0;

    }

    @Override
    public int updateBeheerderById(Integer id, Beheerder beheerder) {
        String sql = "UPDATE beheerders SET name = ? WHERE id = ?;";
        jdbcTemplate.update(sql, beheerder.getName(), id);
        return 0;
    }
}
