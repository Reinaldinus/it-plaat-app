package com.example.itLandschapApi.service;

import com.example.itLandschapApi.dao.ApplicationDao;
import com.example.itLandschapApi.dao.BusinessDomainDao;
import com.example.itLandschapApi.dao.RelationUserBusinessDomainDao;
import com.example.itLandschapApi.dto.CustomRepetitionException;
import com.example.itLandschapApi.dto.CustomUnauthenticatedRuntimeException;
import com.example.itLandschapApi.model.Application;
import com.example.itLandschapApi.model.BusinessDomain;
import com.example.itLandschapApi.model.RelationUserBusinessDomain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class BusinessDomainService {

    private final BusinessDomainDao businessDomainDao;
    private final RelationUserBusinessDomainDao relationUserBusinessDomainDao;
    private final ApplicationDao applicationDao;

    @Autowired
    public BusinessDomainService(@Qualifier("domains") BusinessDomainDao businessDomainDao,
                                 @Qualifier("user-bd") RelationUserBusinessDomainDao relationUserBusinessDomainDao,
                                 @Qualifier("postgres")ApplicationDao applicationDao){
        this.businessDomainDao = businessDomainDao;
        this.relationUserBusinessDomainDao = relationUserBusinessDomainDao;
        this.applicationDao = applicationDao;
    }

    public int addBusinessDomain(BusinessDomain businessDomain, String userId){
        //Check if there is already a businessdomain with this name
        Optional<BusinessDomain> existingBD = businessDomainDao.findBusinessDomainByName(businessDomain.getName(), UUID.fromString(userId));
        if(existingBD.isPresent() ) {
            throw new CustomRepetitionException("There is already a business domain with this name. You can't create two business domains with the same name", HttpStatus.CONFLICT);
        }

        Integer businessDomainId =  businessDomainDao.insertBusinessDomain(businessDomain);
        relationUserBusinessDomainDao.insertRelationUserBusinessDomain(new RelationUserBusinessDomain(UUID.fromString(userId), businessDomainId));
        return businessDomainId;
    }

    public List<BusinessDomain> getAllBusinessDomains(String userId){
        return businessDomainDao.selectAllBusinessDomains(UUID.fromString(userId));
    }

    public void deleteBusinessDomain(Integer id, String userId){
        RelationUserBusinessDomain receivedRelation = new RelationUserBusinessDomain(UUID.fromString(userId), id);
        if(!relationUserBusinessDomainDao.checkIfBusinessDomainBelongsToUser(receivedRelation)){
            throw new CustomUnauthenticatedRuntimeException("This business domain doesn't exist or you don't have permission to delete it.", HttpStatus.UNAUTHORIZED);
        }

        //Check if BD has apps and then complain
        List<Application> list = applicationDao.findApplicationsByBusinessDomainId(id, UUID.fromString(userId));
        if(list.size() > 0){
            String errorMessage = "";
            for(int i = 0; i < list.size(); i++){
                errorMessage = errorMessage + list.get(i).getAppName();
                if(i != list.size()-1){
                    errorMessage = errorMessage + ", ";
                }
            }
            throw new CustomRepetitionException("This business domain has the following apps associated with it: "+ errorMessage + ". Please delete them before.", HttpStatus.CONFLICT);
        }

        businessDomainDao.deleteBusinessDomainById(id);
        relationUserBusinessDomainDao.deleteRelationUserBusinessDomain(receivedRelation);
    }

    public int updateBusinessDomain(Integer id, BusinessDomain businessDomain, String userId){
        Optional<BusinessDomain> existingBD = businessDomainDao.findBusinessDomainByName(businessDomain.getName(), UUID.fromString(userId));
        if(existingBD.isPresent() && !existingBD.get().getId().equals(id)) {
            throw new CustomRepetitionException("There is already a business domain with this name. You can't create two business domains with the same name", HttpStatus.CONFLICT);
        }

        RelationUserBusinessDomain receivedRelation = new RelationUserBusinessDomain(UUID.fromString(userId), id);
        if(!relationUserBusinessDomainDao.checkIfBusinessDomainBelongsToUser(receivedRelation)){
            throw new CustomUnauthenticatedRuntimeException("This business domain doesn't exist or you don't have permission to modify it.", HttpStatus.UNAUTHORIZED);

        }
        return businessDomainDao.updateBusinessDomainById(id, businessDomain);
    }
}
