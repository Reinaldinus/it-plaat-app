package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.User;
import com.example.itLandschapApi.requestmodel.LoginRequestModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Repository("login")
public class AuthDataAccessService implements AuthDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public AuthDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public UUID createUser(User user) {

        String sql = "INSERT INTO users (username, email, password) VALUES (?, ?, ?)";

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(con -> {
            PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getEmail());
            ps.setString(3, user.getPassword());

            return ps;
        }, keyHolder);

        Map<String, Object> keys = keyHolder.getKeys();
        UUID generatedId = (UUID) keys.get("id");
        return generatedId;


    }



    @Override
    public Optional<User> findUserByUsername(String username) {
        String sql = "SELECT * FROM users WHERE username = ? ; ";
        //Specifying the values of it's parameter

        List<User> listUser = jdbcTemplate.query(sql, new Object[]{username}, (resultSet, i) -> {
            UUID id = UUID.fromString(resultSet.getObject("id").toString());
            String email = resultSet.getString("email");
            String password = resultSet.getString("password");


            return new User(id, username, email, password);
        });
        return listUser.size() == 0 ? Optional.empty() : Optional.of(listUser.get(0));
    }

    @Override
    public Optional<User> selectUserByEmail(String email) {
        String sql = "SELECT * FROM users WHERE email = ? ; ";
        //Specifying the values of it's parameter

        List<User> listUser = jdbcTemplate.query(sql, new Object[]{email}, (resultSet, i) -> {
            UUID id = UUID.fromString(resultSet.getObject("id").toString());
            String username = resultSet.getString("username");
            String password = resultSet.getString("password");


            return new User(id, username, email, password);
        });
        return listUser.size() == 0 ? Optional.empty() : Optional.of(listUser.get(0));
    }
}
