package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.RelationApplicationTodo;
import com.example.itLandschapApi.model.ToDo;

import java.util.List;

public interface RelationApplicationToDoDao {
    int insertRelationApplicationToDo(RelationApplicationTodo relationApplicationTodo);
    Integer getIdOfApplicationAssociatedWithThisToDo(Integer toDoId);

    int deleteApplicationToDoRelationByToDoId(Integer toDoId);
    int deleteApplicationToDoRelationByAppId(Integer appId);

}
