package com.example.itLandschapApi.service;

import com.example.itLandschapApi.dao.ApplicationDao;
import com.example.itLandschapApi.dao.OwnerDao;
import com.example.itLandschapApi.dao.RelationUserOwnerDao;
import com.example.itLandschapApi.dto.CustomRepetitionException;
import com.example.itLandschapApi.dto.CustomUnauthenticatedRuntimeException;
import com.example.itLandschapApi.model.Application;
import com.example.itLandschapApi.model.Owner;
import com.example.itLandschapApi.model.RelationUserOwner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class OwnerService {
    private final OwnerDao ownerDao;
    private final RelationUserOwnerDao relationUserOwnerDao;
    private final ApplicationDao applicationDao;

    @Autowired
    public OwnerService(@Qualifier("owners")OwnerDao ownerDao,
                        @Qualifier("user-owner") RelationUserOwnerDao relationUserOwnerDao,
                        @Qualifier("postgres") ApplicationDao applicationDao){
        this.ownerDao = ownerDao;
        this.relationUserOwnerDao = relationUserOwnerDao;
        this.applicationDao = applicationDao;
    }

    public int addOwner(Owner owner, String userId){
        //Check if there is already an Owner
        Optional<Owner> existingOwner = ownerDao.selectOwnerByName(owner.getName(), UUID.fromString(userId));
        if(existingOwner.isPresent()) {
            throw new CustomRepetitionException("There is already a owner with this name. You can't create two owner with the same name", HttpStatus.CONFLICT);
        }

        Integer ownerId =  ownerDao.insertOwner(owner);
        relationUserOwnerDao.insertRelationUserOwner(new RelationUserOwner(UUID.fromString(userId), ownerId));
        return ownerId;
    }

    public List<Owner> getAllOwners(String userId){
        return ownerDao.selectAllOwners(UUID.fromString(userId));
    }

    public Optional<Owner> getOwnerById(Integer id, String userId){
        if (!relationUserOwnerDao.checkIfOwnerBelongsToUser(new RelationUserOwner(UUID.fromString(userId), id))){
            throw new CustomUnauthenticatedRuntimeException("This owner doesn't exist or you don't have permission.", HttpStatus.UNAUTHORIZED);
        }

        return ownerDao.selectOwnerById(id);
    }

    public void deleteOwner(Integer id, String userId){
        RelationUserOwner receivedRelation = new RelationUserOwner(UUID.fromString(userId), id);
        if (!relationUserOwnerDao.checkIfOwnerBelongsToUser(receivedRelation)){
            throw new RuntimeException("This owner doesn't exist or you don't have permission.");
        }

        //Check if Owner has apps and then complain
        List<Application> list = applicationDao.findApplicationsByOwnerId(id, UUID.fromString(userId));
        if(list.size() > 0){
            String errorMessage = "";
            for(int i = 0; i < list.size(); i++){
                errorMessage = errorMessage + list.get(i).getAppName();
                if(i != list.size()-1){
                    errorMessage = errorMessage + ", ";
                }
            }
            throw new CustomRepetitionException("This owner has the following apps associated with it: "+ errorMessage + ". Please delete them or associate it with another owner before.", HttpStatus.CONFLICT);
        }


        ownerDao.deleteOwnerById(id);
        relationUserOwnerDao.deleteRelationUserOwner(receivedRelation);
    }

    public int updateOwner(Integer id, Owner owner, String userId){
        //Check if there is already an Owner
        Optional<Owner> existingOwner = ownerDao.selectOwnerByName(owner.getName(), UUID.fromString(userId));
        if(existingOwner.isPresent() && !existingOwner.get().getId().equals(id)) {
            throw new CustomRepetitionException("There is already a owner with this name. You can't create two owner with the same name", HttpStatus.CONFLICT);
        }

        if (!relationUserOwnerDao.checkIfOwnerBelongsToUser(new RelationUserOwner(UUID.fromString(userId), id))){
            throw new CustomUnauthenticatedRuntimeException("This owner doesn't exist or you don't have permission.", HttpStatus.UNAUTHORIZED);
        }

        return ownerDao.updateOwnerById(id, owner);
    }
}
