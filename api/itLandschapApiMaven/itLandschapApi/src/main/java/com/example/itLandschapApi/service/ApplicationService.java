package com.example.itLandschapApi.service;

import com.example.itLandschapApi.dao.*;
import com.example.itLandschapApi.dto.CustomRepetitionException;
import com.example.itLandschapApi.dto.CustomUnauthenticatedRuntimeException;
import com.example.itLandschapApi.model.*;
import com.example.itLandschapApi.requestmodel.ApplicationRequestModel;
import com.example.itLandschapApi.responsemodels.ApplicationResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ApplicationService {

    private final ApplicationDao applicationDao;
    private final ProductOwnerDao productOwnerDao;
    private final ServerDao serverDao;
    private final BusinessDomainDao businessDomainDao;
    private final ServiceLevelAgreementDao serviceLevelAgreementDao;
    private final OwnerDao ownerDao;
    private final RelationApplicationBeheerderDao relationApplicationBeheerderDao;
    private final ToDoDao toDoDao;
    private final BeheerderDao beheerderDao;
    private final RelationApplicationToDoDao relationApplicationToDoDao;
    private final RelationUserApplicationDao relationUserApplicationDao;
    private final RelationUserPODao relationUserPODao;
    private final RelationUserOwnerDao relationuserOwnerDao;
    private final RelationUserBeheerderDao relationUserBeheerderDao;
    private final RelationUserBusinessDomainDao relationUserBusinessDomainDao;
    private final RelationUserServerDao relationUserServerDao;
    private final RelationUserSLADao relationUserSLADao;

    @Autowired
    public ApplicationService(@Qualifier("postgres") ApplicationDao applicationDao,
                              @Qualifier("productOwners") ProductOwnerDao productOwnerDao,
                              @Qualifier("servers") ServerDao serverDao,
                              @Qualifier("domains") BusinessDomainDao businessDomainDao,
                              @Qualifier("slas") ServiceLevelAgreementDao serviceLevelAgreementDao,
                              @Qualifier("owners") OwnerDao ownerDao,
                              @Qualifier("relation_app_beheerder") RelationApplicationBeheerderDao relationApplicationBeheerderDao,
                              @Qualifier("beheerders") BeheerderDao beheerderDao,
                              @Qualifier("todos") ToDoDao toDoDao,
                              @Qualifier("app-todo") RelationApplicationToDoDao relationApplicationToDoDao,
                              @Qualifier("user-application")RelationUserApplicationDao relationUserApplicationDao,
                              @Qualifier("user-po") RelationUserPODao relationUserPODao,
                              @Qualifier("user-owner") RelationUserOwnerDao relationuserOwnerDao,
                              @Qualifier("user-beheerder") RelationUserBeheerderDao relationUserBeheerderDao,
                              @Qualifier("user-bd") RelationUserBusinessDomainDao relationUserBusinessDomainDao,
                              @Qualifier("user-server") RelationUserServerDao relationUserServerDao,
                              @Qualifier("user-sla") RelationUserSLADao relationUserSLADao) {
        this.applicationDao = applicationDao;
        this.productOwnerDao = productOwnerDao;
        this.serverDao = serverDao;
        this.businessDomainDao = businessDomainDao;
        this.serviceLevelAgreementDao = serviceLevelAgreementDao;
        this.ownerDao = ownerDao;
        this.relationApplicationBeheerderDao = relationApplicationBeheerderDao;
        this.beheerderDao = beheerderDao;
        this.toDoDao = toDoDao;
        this.relationApplicationToDoDao = relationApplicationToDoDao;
        this.relationUserApplicationDao = relationUserApplicationDao;
        this.relationUserPODao = relationUserPODao;
        this.relationuserOwnerDao = relationuserOwnerDao;
        this.relationUserBeheerderDao = relationUserBeheerderDao;
        this.relationUserBusinessDomainDao = relationUserBusinessDomainDao;
        this.relationUserServerDao = relationUserServerDao;
        this.relationUserSLADao = relationUserSLADao;
    }

    public int addApplication(ApplicationRequestModel applicationRequestModel, String userId) {
        //Check that there is no applications with the same name.
        checkIfThereIsAnAppWithTheSameName(null, applicationRequestModel, userId);

        verifyThatAllTheInfoBelongsToUser(userId, applicationRequestModel);


        int appId = applicationDao.insertApplication(applicationRequestModel.toApplication());
        relationUserApplicationDao.insertRelationUserApplication(new RelationUserApplication(UUID.fromString(userId), appId));

        applicationRequestModel.getBeheerders().stream().forEach(beheerder -> {
            relationApplicationBeheerderDao.insertRelationApplicationBeheerder(
                    new RelationApplicationBeheerder(appId, beheerder.getId())
            );
        });

        return appId;
    }

    public List<ApplicationResponseModel> getAllApplications(String userId) {
        List<Application> applicationList = applicationDao.selectAllApplications(UUID.fromString(userId));
        List<ApplicationResponseModel> responseModelList = attachAllExtraInfo(applicationList);
        return responseModelList;

    }


    public List<ApplicationResponseModel> getApplicationsByBusinessDomainIdAndServerId(Integer businessDomainId, Integer serverId, String userId) {
        List<Application> applicationList = applicationDao.selectApplicationsByPosition(businessDomainId, serverId, UUID.fromString(userId));
        List<ApplicationResponseModel> responseModelList = attachAllExtraInfo(applicationList);
        return responseModelList;


    }

    public int deleteApplication(Integer id, String userId) {
        RelationUserApplication receivedRelation = new RelationUserApplication(UUID.fromString(userId), id);
        //Check that the app belongs to the user
        if(!relationUserApplicationDao.checkIfAppBelongsToUser(receivedRelation)){
           throw new CustomUnauthenticatedRuntimeException("You don't have permission to delete this app", HttpStatus.UNAUTHORIZED);
        }

        //Delete app
        applicationDao.deleteApplicationById(id);
        relationApplicationBeheerderDao.deleteAllRelationApplicationBeheerderByApplicationId(id);
        //Check what to dos are related to this app
        List<ToDo> toDoList = toDoDao.selectToDosByAppId(id);
        //delete all those to dos
        for(ToDo todo : toDoList){
            toDoDao.deleteToDoById(todo.getId());
        }
        relationApplicationToDoDao.deleteApplicationToDoRelationByAppId(id);
        relationUserApplicationDao.deleteRelationUserApplication(receivedRelation);
        //Delete user app relation
        return 0;
    }

    public int updateApplication(Integer id, ApplicationRequestModel applicationRequestModel, String userId) {
        RelationUserApplication receivedRelation = new RelationUserApplication(UUID.fromString(userId), id);
        //Check that the app belongs to the user
        if(!relationUserApplicationDao.checkIfAppBelongsToUser(receivedRelation)){
            throw new CustomUnauthenticatedRuntimeException("You don't have permission to update this app", HttpStatus.UNAUTHORIZED);
        }

        //Check that there is no applications with the same name.
        checkIfThereIsAnAppWithTheSameName(id, applicationRequestModel, userId);

        verifyThatAllTheInfoBelongsToUser(userId, applicationRequestModel);


        //update application
        applicationDao.updateApplicationById(id, applicationRequestModel.toApplication());
        //I get the list of beheerders that where associated with this app.
        List<Beheerder> oldBeheerders = getBeheerderListWithAppId(id);

        //get the list of beheerders that are new (where not in the db).
        List<Beheerder> beheerdersToAdd = selectNewBeheerdersToAdd(applicationRequestModel.getBeheerders(), oldBeheerders);
        //Add the new relations beheerder to do to DB
        for (Beheerder beheerder : beheerdersToAdd) {
            RelationApplicationBeheerder relationApplicationBeheerder = new RelationApplicationBeheerder(id, beheerder.getId());
            relationApplicationBeheerderDao.insertRelationApplicationBeheerder(relationApplicationBeheerder);
        }
        //get the list of beheerders that were removed.
        List<Beheerder> beheerdersToRemove = selectBeheerdersToDelete(applicationRequestModel.getBeheerders(), oldBeheerders);
        //remove the beheerders to remove from the relation db
        for (Beheerder beheerder : beheerdersToRemove) {
            relationApplicationBeheerderDao.deleteRelationApplicationBeheerderByAppAndBeheerderId(id, beheerder.getId());
        }
        return id;
    }



    private List<ApplicationResponseModel> attachAllExtraInfo(List<Application> applicationList) {
        List<ApplicationResponseModel> responseModelList = new ArrayList<>();
        for (Application application : applicationList) {
            //Make an applicationModel from application. Change constructor.
            ApplicationResponseModel applicationResponseModel = new ApplicationResponseModel(application);

            //Look for the product owner
            Optional<ProductOwner> optionalPO = productOwnerDao.selectProductOwnerById(application.getProductOwnerId());
            if (optionalPO.isPresent()) {
                applicationResponseModel.addProductOwner(optionalPO.get());
            }

            //Look for the server
            Optional<Server> optionalServer = serverDao.selectServerById(application.getServerId());
            if (optionalServer.isPresent()) {
                applicationResponseModel.addServer(optionalServer.get());
            }

            //Look for businessDomain
            Optional<BusinessDomain> optionalBD = businessDomainDao.selectBusinessDomainById(application.getBusinessDomainId());
            if (optionalBD.isPresent()) {
                applicationResponseModel.addBusinessDomain(optionalBD.get());
            }

            //Look for sla
            Optional<ServiceLevelAgreement> optionalSLA = serviceLevelAgreementDao.selectServiceLevelAgreementById(application.getServiceLevelAgreementId());
            if (optionalSLA.isPresent()) {
                applicationResponseModel.addServiceLevelAgreement(optionalSLA.get());
            }

            //Look for Owner
            Optional<Owner> optionalOwner = ownerDao.selectOwnerById(application.getOwnerId());
            if (optionalOwner.isPresent()) {
                applicationResponseModel.addOwner(optionalOwner.get());
            }

            //Look for beheerders
            List<Beheerder> beheerderList = getBeheerderListWithAppId(application.getId());
            //Look for Todos
            List<ToDo> toDos = toDoDao.selectToDosByAppId(application.getId());

            for (Beheerder beheerder : beheerderList) {
                applicationResponseModel.addBeheerdersToApp(beheerder);
            }
            for (ToDo toDo : toDos) {
                applicationResponseModel.addToDosToApp(toDo);
            }
            responseModelList.add(applicationResponseModel);

        }
        return responseModelList;
    }

    private List<Beheerder> selectNewBeheerdersToAdd(List<Beheerder> newBeheerders, List<Beheerder> oldBeheerders) {
        //If for some reason the to do was empty we add eveyone.
        List<Beheerder> resultList = new ArrayList<>();
        if (oldBeheerders.size() == 0) {
            for (Beheerder beheerder : newBeheerders) {
                resultList.add(beheerder);
            }
        }
        for (int i = 0; i < newBeheerders.size(); i++) {
            for (int x = 0; x < oldBeheerders.size(); x++) {
                if (newBeheerders.get(i).getId() == oldBeheerders.get(x).getId()) {
                    break;
                } else if (x == oldBeheerders.size() - 1) {
                    resultList.add(newBeheerders.get(i));
                }

            }
        }
        return resultList;
    }

    private List<Beheerder> selectBeheerdersToDelete(List<Beheerder> newBeheerders, List<Beheerder> oldBeheerders) {
        List<Beheerder> resultList = new ArrayList<>();
        for (int i = 0; i < oldBeheerders.size(); i++) {
            for (int x = 0; x < newBeheerders.size(); x++) {
                if (oldBeheerders.get(i).getId() == newBeheerders.get(x).getId()) {
                    break;
                } else if (x == newBeheerders.size() - 1) {
                    resultList.add(oldBeheerders.get(i));
                }

            }
        }
        return resultList;
    }

    private List<Beheerder> getBeheerderListWithAppId(int appId){
        List<RelationApplicationBeheerder> beheerderRelationList = relationApplicationBeheerderDao.selectRelationApplicationBeheerderByAppId(appId);
        List<Beheerder> beheerderList = new ArrayList<>();

        for (RelationApplicationBeheerder relationApplicationBeheerder : beheerderRelationList) {
            Beheerder beheerder;
            Optional<Beheerder> optionalBeheerder = beheerderDao.selectBeheerderById(relationApplicationBeheerder.getBeheerderId());
            if (optionalBeheerder.isPresent()) {
                beheerder = optionalBeheerder.get();
                beheerderList.add(beheerder);
            }
        }
        return beheerderList;
    }

    private void verifyThatAllTheInfoBelongsToUser(String userId, ApplicationRequestModel applicationRequestModel){
        //Check if the PO belongs to the user
        if(!relationUserPODao.checkIfPOBelongsToUser(new RelationUserPO(UUID.fromString(userId), applicationRequestModel.getProductOwnerId()))){
            throw new CustomUnauthenticatedRuntimeException("You don't have access to this Product Owner or this Product Owner doesn't exist.", HttpStatus.UNAUTHORIZED);
        }

        //Check if the Server belongs to the user
        if(!relationUserServerDao.checkIfServerBelongsToUser(new RelationUserServer(UUID.fromString(userId), applicationRequestModel.getServerId()))){
            throw new CustomUnauthenticatedRuntimeException("You don't have access to this server or this server doesn't exist.", HttpStatus.UNAUTHORIZED);
        }

        //Check if the BD belongs to the user
        if(!relationUserBusinessDomainDao.checkIfBusinessDomainBelongsToUser(new RelationUserBusinessDomain(UUID.fromString(userId), applicationRequestModel.getBusinessDomainId()))){
            throw new CustomUnauthenticatedRuntimeException("You don't have access to this business domain or this business domain doesn't exist.", HttpStatus.UNAUTHORIZED);

        }

        //Check if the SLA belongs to the user
        if(!relationUserSLADao.checkIfSLABelongsToUser(new RelationUserSLA(UUID.fromString(userId), applicationRequestModel.getServiceLevelAgreementId()))){
            throw new CustomUnauthenticatedRuntimeException("You don't have access to this sla or this sla doesn't exist.", HttpStatus.UNAUTHORIZED);

        }

        //Check if the Owner belongs to the user
        if(!relationuserOwnerDao.checkIfOwnerBelongsToUser(new RelationUserOwner(UUID.fromString(userId), applicationRequestModel.getOwnerId()))){
            throw new CustomUnauthenticatedRuntimeException("You don't have access to this owner or this owner doesn't exist.", HttpStatus.UNAUTHORIZED);

        }

        applicationRequestModel.getBeheerders().stream().forEach(beheerder -> {
            if(!relationUserBeheerderDao.checkIfBeheerderBelongsToUser(new RelationUserBeheerder(UUID.fromString(userId), beheerder.getId()))){
                throw new CustomUnauthenticatedRuntimeException("You don't have access to one of these beheerders or one of this beheerders doesn't exist.", HttpStatus.UNAUTHORIZED);

            }
        });
    }

    private void checkIfThereIsAnAppWithTheSameName(Integer id, ApplicationRequestModel applicationRequestModel, String userId) {
        Optional<Application> existingApp = applicationDao.findApplicationByName(applicationRequestModel.getAppName(), UUID.fromString(userId));
        if(existingApp.isPresent() && !existingApp.get().getId().equals(id)) {
            throw new CustomRepetitionException("There is already an application with this name. You can't create two applications with the same name", HttpStatus.CONFLICT);
        }
    }
}
