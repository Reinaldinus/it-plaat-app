package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.ToDo;

import java.util.List;

public interface ToDoDao {

    int insertToDo(ToDo toDo);

    List<ToDo> selectToDosByAppId(Integer appId);
    List<ToDo> selectToDosByBeheerderId(Integer beheerderId);



    int deleteToDoById(Integer id);

    int updateToDoById(Integer id, ToDo toDo);
    
    int markToDoDone(Integer toDoId);
    int markToDoUndone(Integer toDoId);
}
