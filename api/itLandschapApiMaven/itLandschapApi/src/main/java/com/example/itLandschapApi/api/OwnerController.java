package com.example.itLandschapApi.api;

import com.example.itLandschapApi.dto.CustomRepetitionException;
import com.example.itLandschapApi.dto.CustomUnauthenticatedRuntimeException;
import com.example.itLandschapApi.model.Owner;
import com.example.itLandschapApi.security.JwtTokenProvider;
import com.example.itLandschapApi.service.OwnerService;
import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/api/v1/owner")
@RestController
public class OwnerController {

    private final OwnerService ownerService;
    private final JwtTokenProvider jwtTokenProvider;


    @Autowired
    public OwnerController (OwnerService ownerService,  JwtTokenProvider jwtTokenProvider){
        this.ownerService = ownerService;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @ExceptionHandler(CustomUnauthenticatedRuntimeException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public Error handleCustomRuntimeException(CustomUnauthenticatedRuntimeException ex) {
        System.out.println(ex.getMessage());
        return new Error(ex.getMessage());
    }
    @CrossOrigin
    @PostMapping
    public void addOwner(HttpServletRequest request, @Valid @NonNull @RequestBody Owner owner){
        String userId = extractUserIdFromToken(request);

        ownerService.addOwner(owner, userId);
    }
    @ExceptionHandler(CustomRepetitionException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    public Error handleCustomRepetitionException(CustomRepetitionException ex) {
        System.out.println(ex.getMessage());
        return new Error(ex.getMessage());
    }

    @CrossOrigin
    @GetMapping
    public List<Owner> getAllOwners(HttpServletRequest request) {
        String userId = extractUserIdFromToken(request);
        return ownerService.getAllOwners(userId);
    }

    @CrossOrigin
    @GetMapping(path="{id}")
    public Optional<Owner> getOwnerById(HttpServletRequest request, @PathVariable("id") Integer id) {
        String userId = extractUserIdFromToken(request);

        return ownerService.getOwnerById(id, userId);
    }

    @CrossOrigin
    @DeleteMapping(path = "{id}")
    public void deleteOwnerById(HttpServletRequest request, @PathVariable("id") Integer id){
        String userId = extractUserIdFromToken(request);

        ownerService.deleteOwner(id, userId);
    }

    @PutMapping(path = "{id}")
    public void updateOwnerById(HttpServletRequest request, @PathVariable("id") Integer id, @Valid @NonNull @RequestBody Owner ownerToUpdate){
        String userId = extractUserIdFromToken(request);

        ownerService.updateOwner(id, ownerToUpdate, userId);
    }

    private String extractUserIdFromToken(HttpServletRequest request) {
        String token = request.getHeader("Authorization").replace("Bearer ", "");
        String userId;
        try {
            userId = jwtTokenProvider.getUserIdFromToken(token);
        } catch (ExpiredJwtException ex) {
            throw new CustomUnauthenticatedRuntimeException("The token is expired, please log in again", HttpStatus.UNAUTHORIZED);

        }
        if (!jwtTokenProvider.validateToken(token, userId)) {
            throw new CustomUnauthenticatedRuntimeException("The provided JWT token is invalid or malformed. ", HttpStatus.UNAUTHORIZED);
        }
        return userId;
    }
}


