package com.example.itLandschapApi.model;

import java.util.UUID;

public class RelationUserOwner {

    private final UUID userId;
    private final Integer ownerId;



    public RelationUserOwner( UUID userId, Integer ownerId){
        this.userId = userId;
        this.ownerId = ownerId;
    }



    public UUID getUserId() {
        return userId;
    }

    public Integer getOwnerId(){ return ownerId; };
}
