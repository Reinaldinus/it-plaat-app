package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.RelationUserBeheerder;
import com.example.itLandschapApi.model.RelationUserBusinessDomain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("user-bd")
public class RelationUserBusinessDomainDataAccessService implements RelationUserBusinessDomainDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public RelationUserBusinessDomainDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void insertRelationUserBusinessDomain(RelationUserBusinessDomain relationUserBusinessDomain) {
        String sql = "INSERT INTO user_bd_relation (user_id, bd_id)  VALUES (?, ?)";
        jdbcTemplate.update(sql, relationUserBusinessDomain.getUserId(), relationUserBusinessDomain.getBdId());
    }

    @Override
    public boolean checkIfBusinessDomainBelongsToUser(RelationUserBusinessDomain relationUserBusinessDomain) {
        String sql = "SELECT * FROM user_bd_relation WHERE bd_id = ? AND user_id = ? ";

        List<RelationUserBeheerder> relationList = jdbcTemplate.query(sql, new Object[]{relationUserBusinessDomain.getBdId(), relationUserBusinessDomain.getUserId()},
                (resultSet, i) -> {
                    UUID userId = UUID.fromString(resultSet.getString("user_id"));
                    Integer bdId = resultSet.getInt("bd_id");
                    return new RelationUserBeheerder(userId, bdId);
                });
        return relationList.size() > 0;        }

    @Override
    public void deleteRelationUserBusinessDomain(RelationUserBusinessDomain relationUserBusinessDomain) {
        String sql = "DELETE FROM user_bd_relation WHERE user_id = ? AND bd_id = ? ";
        jdbcTemplate.update(sql, relationUserBusinessDomain.getUserId(), relationUserBusinessDomain.getBdId());
    }
}
