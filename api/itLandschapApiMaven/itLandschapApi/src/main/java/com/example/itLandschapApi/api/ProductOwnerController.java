package com.example.itLandschapApi.api;

import com.example.itLandschapApi.dto.CustomRepetitionException;
import com.example.itLandschapApi.dto.CustomUnauthenticatedRuntimeException;
import com.example.itLandschapApi.model.ProductOwner;
import com.example.itLandschapApi.security.JwtTokenProvider;
import com.example.itLandschapApi.service.ProductOwnerService;
import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/api/v1/product_owner")
@RestController
public class ProductOwnerController {
    private final ProductOwnerService productOwnerService;
    private final JwtTokenProvider jwtTokenProvider;


    @Autowired
    public ProductOwnerController (ProductOwnerService productOwnerService, JwtTokenProvider jwtTokenProvider){
        this.productOwnerService = productOwnerService;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @ExceptionHandler(CustomUnauthenticatedRuntimeException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public Error handleCustomRuntimeException(CustomUnauthenticatedRuntimeException ex) {
        System.out.println(ex.getMessage());
        return new Error(ex.getMessage());
    }

    @ExceptionHandler(CustomRepetitionException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    public Error handleCustomRepetitionException(CustomRepetitionException ex) {
        System.out.println(ex.getMessage());
        return new Error(ex.getMessage());
    }

    @CrossOrigin
    @PostMapping
    public void addProductOwner(HttpServletRequest request, Integer id, @Valid @NonNull @RequestBody ProductOwner productOwner){
        String userId = extractUserIdFromToken(request);

        productOwnerService.addProductOwner(productOwner, userId);
    }

    @CrossOrigin
    @GetMapping
    public List<ProductOwner> getAllProductOwners(HttpServletRequest request) {
        String userId = extractUserIdFromToken(request);

        return productOwnerService.getAllProductOwners(userId);
    }

    @CrossOrigin
    @GetMapping(path="{id}")
    public Optional<ProductOwner> getProductOwnerById(HttpServletRequest request, @PathVariable("id") Integer id) {
        String userId = extractUserIdFromToken(request);

        return productOwnerService.getProductOwnerById(id, userId);
    }

    @CrossOrigin
    @DeleteMapping(path = "{id}")
    public void deleteProductOwnerById(HttpServletRequest request, @PathVariable("id") Integer id){
        String userId = extractUserIdFromToken(request);

        productOwnerService.deleteProductOwner(id, userId);
    }

    @PutMapping(path = "{id}")
    public void updateProductOwnerById(HttpServletRequest request, @PathVariable("id") Integer id, @Valid @NonNull @RequestBody ProductOwner productOwnerToUpdate){
        String userId = extractUserIdFromToken(request);

        productOwnerService.updateProductOwner(id, productOwnerToUpdate, userId);
    }

    private String extractUserIdFromToken(HttpServletRequest request) {
        String token = request.getHeader("Authorization").replace("Bearer ", "");
        String userId;
        try {
            userId = jwtTokenProvider.getUserIdFromToken(token);
        } catch (ExpiredJwtException ex) {
            throw new CustomUnauthenticatedRuntimeException("The token is expired, please log in again", HttpStatus.UNAUTHORIZED);

        }
        if (!jwtTokenProvider.validateToken(token, userId)) {
            throw new CustomUnauthenticatedRuntimeException("The provided JWT token is invalid or malformed. ", HttpStatus.UNAUTHORIZED);
        }
        return userId;
    }
}
