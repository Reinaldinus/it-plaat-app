package com.example.itLandschapApi.model;

import java.util.UUID;

public class RelationUserServer {

    private final UUID userId;
    private final Integer serverId;



    public RelationUserServer( UUID userId, Integer serverId){
        this.userId = userId;
        this.serverId = serverId;
    }



    public UUID getUserId() {
        return userId;
    }

    public Integer getServerId(){ return serverId; };
}
