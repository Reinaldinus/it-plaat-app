package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.Beheerder;
import com.example.itLandschapApi.model.ProductOwner;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BeheerderDao {


    int insertBeheerder(Beheerder beheerder);

    Optional<Beheerder> selectBeheerderById(Integer id);
    List<Beheerder> selectAllBeheerders(UUID userId);

    Optional<Beheerder> findBeheerderByName(UUID userId, String name);

    int deleteBeheerderById(Integer id);

    int updateBeheerderById(Integer id, Beheerder beheerder);
}
