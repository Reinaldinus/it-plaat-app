package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.RelationUserPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("user-po")
public class RelationUserPODataAccessServer  implements RelationUserPODao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public RelationUserPODataAccessServer(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void insertRelationUserPO(RelationUserPO relationUserPO) {
        String sql = "INSERT INTO user_po_relation (user_id, po_id)  VALUES (?, ?)";
        jdbcTemplate.update(sql, relationUserPO.getUserId(), relationUserPO.getPoId());
    }

    @Override
    public boolean checkIfPOBelongsToUser(RelationUserPO relationUserPO) {
        String sql = "SELECT * FROM user_po_relation WHERE po_id = ? AND user_id = ? ";

        List<RelationUserPO> relationList = jdbcTemplate.query(sql, new Object[]{relationUserPO.getPoId(), relationUserPO.getUserId()},
                (resultSet, i) -> {
                    UUID userId = UUID.fromString(resultSet.getString("user_id"));
                    Integer poId = resultSet.getInt("po_id");
                    return new RelationUserPO(userId, poId);
                });
        return relationList.size() > 0;    }

    @Override
    public void deleteRelationUserPO(RelationUserPO relationUserPO) {
        String sql = "DELETE FROM user_po_relation WHERE user_id = ? AND po_id = ? ";
        jdbcTemplate.update(sql, relationUserPO.getUserId(), relationUserPO.getPoId());
    }
}
