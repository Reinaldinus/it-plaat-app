package com.example.itLandschapApi.api;

import com.example.itLandschapApi.dto.CustomRepetitionException;
import com.example.itLandschapApi.dto.CustomUnauthenticatedRuntimeException;
import com.example.itLandschapApi.requestmodel.ApplicationRequestModel;
import com.example.itLandschapApi.responsemodels.ApplicationResponseModel;
import com.example.itLandschapApi.security.JwtTokenProvider;
import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;
import com.example.itLandschapApi.service.ApplicationService;


import java.util.List;


@RequestMapping("/api/v1/application")
@RestController
public class ApplicationController {

    private final ApplicationService applicationService;
    private final JwtTokenProvider jwtTokenProvider;

    @Autowired
    public ApplicationController(ApplicationService applicationService, JwtTokenProvider jwtTokenProvider) {
        this.applicationService = applicationService;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @ExceptionHandler(CustomUnauthenticatedRuntimeException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public Error handleCustomRuntimeException(CustomUnauthenticatedRuntimeException ex) {
        System.out.println(ex.getMessage());
        return new Error(ex.getMessage());
    }

    @ExceptionHandler(CustomRepetitionException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    public Error handleCustomRepetitionException(CustomRepetitionException ex) {
        System.out.println(ex.getMessage());
        return new Error(ex.getMessage());
    }

    @CrossOrigin
    @PostMapping
    @ResponseBody
    public int addApplication(HttpServletRequest request, @Valid @NonNull @RequestBody ApplicationRequestModel applicationRequestModel) {
        String userId = extractUserIdFromToken(request);
        //We return the id of the app
        return applicationService.addApplication(applicationRequestModel, userId);
    }


    @CrossOrigin
    @GetMapping
    public ResponseEntity<?> getAllApplications(HttpServletRequest request) {
        String userId = extractUserIdFromToken(request);
        return ResponseEntity.ok(applicationService.getAllApplications(userId));
    }

    @CrossOrigin
    @GetMapping(path = "{business_domain_id}/{server_id}")
    public List<ApplicationResponseModel> getApplicationsByPosition(HttpServletRequest request, @PathVariable("business_domain_id") Integer businessDomainId, @PathVariable("server_id") Integer serverId) {
        String userId = extractUserIdFromToken(request);
        return applicationService.getApplicationsByBusinessDomainIdAndServerId(businessDomainId, serverId, userId);
    }

    @CrossOrigin
    @DeleteMapping(path = "{id}")
    public void deleteApplicationById(HttpServletRequest request, @PathVariable("id") Integer id) {
        String userId = extractUserIdFromToken(request);
        applicationService.deleteApplication(id, userId);
    }

    @CrossOrigin
    @PutMapping(path = "{id}")
    public void updateApplication(HttpServletRequest request, @PathVariable("id") Integer id, @Valid @NonNull @RequestBody ApplicationRequestModel applicationRequestModel) {
        String userId = extractUserIdFromToken(request);
        applicationService.updateApplication(id, applicationRequestModel, userId);
    }


    private String extractUserIdFromToken(HttpServletRequest request) {
        String token = request.getHeader("Authorization").replace("Bearer ", "");
        String userId;
        try {
            userId = jwtTokenProvider.getUserIdFromToken(token);
        } catch (ExpiredJwtException ex) {
            throw new CustomUnauthenticatedRuntimeException("The token is expired, please log in again", HttpStatus.UNAUTHORIZED);

        }
        if (!jwtTokenProvider.validateToken(token, userId)) {
            throw new CustomUnauthenticatedRuntimeException("The provided JWT token is invalid or malformed. ", HttpStatus.UNAUTHORIZED);
        }
        return userId;
    }

}


