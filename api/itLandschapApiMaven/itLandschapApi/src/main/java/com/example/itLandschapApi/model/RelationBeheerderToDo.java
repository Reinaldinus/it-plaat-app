package com.example.itLandschapApi.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RelationBeheerderToDo {


    private final Integer beheerderId;
    private final Integer toDoId;



    public RelationBeheerderToDo(@JsonProperty("beheerder_id") Integer beheerderId,
                                   @JsonProperty("to_do_id") Integer toDoId
    ){
        this.beheerderId = beheerderId;
        this.toDoId = toDoId;
    }



    public Integer getBeheerderId() {
        return beheerderId;
    }

    public Integer getToDoId(){ return toDoId; };
}
