package com.example.itLandschapApi.api;

import com.example.itLandschapApi.dto.CustomUnauthenticatedRuntimeException;
import com.example.itLandschapApi.dto.CustomWrongEmailException;
import com.example.itLandschapApi.model.User;
import com.example.itLandschapApi.requestmodel.LoginRequestModel;
import com.example.itLandschapApi.responsemodels.LoginResponseModel;
import com.example.itLandschapApi.service.AuthService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import java.util.regex.Pattern;

@RequestMapping("/api/auth")
@RestController
public class AuthController {

    private final AuthService authService;
    private static final Pattern EMAIL = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private static final Pattern PASSWORD = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!+@$%^&(){}]).{8,32}$");

    @Autowired
    public AuthController(AuthService authService){
        this.authService = authService;
    }

    @ExceptionHandler(CustomWrongEmailException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Error handleCustomRuntimeException(CustomWrongEmailException ex) {
        System.out.println(ex.getMessage());
        return new Error(ex.getMessage());
    }

    @ExceptionHandler(CustomUnauthenticatedRuntimeException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public Error handleCustomRuntimeException(CustomUnauthenticatedRuntimeException ex) {
        System.out.println(ex.getMessage());
        return new Error(ex.getMessage());
    }

    @CrossOrigin
    @PostMapping(path={"/login"})
    @ResponseBody
    public LoginResponseModel logIn(@Valid @NonNull @RequestBody LoginRequestModel loginRequestModel){
        //We will return the id of the app??
        return authService.logIn(loginRequestModel);
    }

    @CrossOrigin
    @PostMapping(path={"/register"})
    @ResponseBody
    public LoginResponseModel register(@Valid @NonNull @RequestBody User user){
        if(!isEmail(user.getEmail())){
            throw new CustomWrongEmailException("Please, enter a valid email", HttpStatus.BAD_REQUEST);
        }
        if(!isAGoodPassword(user.getPassword())){
            throw new CustomWrongEmailException("A password should be at least 8 characters long and contain at least one Capital letter, one special character (*.!+@$%^&(){}), one number and no spaces", HttpStatus.BAD_REQUEST);
        }
        return authService.register(user);
    }


    private boolean isEmail(String email) {
        return EMAIL.matcher(email).matches();
    }

    private boolean isAGoodPassword(String password){
        return PASSWORD.matcher(password).matches();
    }


}
