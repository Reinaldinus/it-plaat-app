package com.example.itLandschapApi.api;

import com.example.itLandschapApi.dto.CustomRepetitionException;
import com.example.itLandschapApi.dto.CustomUnauthenticatedRuntimeException;
import com.example.itLandschapApi.model.BusinessDomain;
import com.example.itLandschapApi.security.JwtTokenProvider;
import com.example.itLandschapApi.service.BusinessDomainService;
import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/v1/business_domain")
@RestController
public class BusinessDomainController {

    private final BusinessDomainService businessDomainService;
    private final JwtTokenProvider jwtTokenProvider;

    @Autowired
    public BusinessDomainController (BusinessDomainService businessDomainService,   JwtTokenProvider jwtTokenProvider){
        this.businessDomainService = businessDomainService;
        this.jwtTokenProvider = jwtTokenProvider;
    }
    @ExceptionHandler(CustomUnauthenticatedRuntimeException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public Error handleCustomRuntimeException(CustomUnauthenticatedRuntimeException ex) {
        System.out.println(ex.getMessage());
        return new Error(ex.getMessage());
    }

    @ExceptionHandler(CustomRepetitionException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    public Error handleCustomRepetitionException(CustomRepetitionException ex) {
        System.out.println(ex.getMessage());
        return new Error(ex.getMessage());
    }

    @CrossOrigin
    @PostMapping
    public void addBusinessDomain(HttpServletRequest request, Integer employeeId, @Valid @NonNull @RequestBody BusinessDomain businessDomain){
        String userId = extractUserIdFromToken(request);
        businessDomainService.addBusinessDomain(businessDomain, userId);
    }
    @CrossOrigin
    @GetMapping
    public List<BusinessDomain> getAllBusinessDomains(HttpServletRequest request) {
        String userId = extractUserIdFromToken(request);

        return businessDomainService.getAllBusinessDomains(userId);
    }

    @CrossOrigin
    @DeleteMapping(path = "{id}")
    public void deleteBusinessDomainById(HttpServletRequest request, @PathVariable("id") Integer id){
        String userId = extractUserIdFromToken(request);

        businessDomainService.deleteBusinessDomain(id, userId);
    }

    @CrossOrigin
    @PutMapping(path = "{id}")
    public void updateBusinessDomainById(HttpServletRequest request, @PathVariable("id") Integer id, @Valid @NonNull @RequestBody BusinessDomain businessDomainToUpdate){
        String userId = extractUserIdFromToken(request);

        businessDomainService.updateBusinessDomain(id, businessDomainToUpdate, userId);
    }

    private String extractUserIdFromToken(HttpServletRequest request) {
        String token = request.getHeader("Authorization").replace("Bearer ", "");
        String userId;
        try {
            userId = jwtTokenProvider.getUserIdFromToken(token);
        } catch (ExpiredJwtException ex) {
            throw new CustomUnauthenticatedRuntimeException("The token is expired, please log in again", HttpStatus.UNAUTHORIZED);

        }
        if (!jwtTokenProvider.validateToken(token, userId)) {
            throw new CustomUnauthenticatedRuntimeException("The provided JWT token is invalid or malformed. ", HttpStatus.UNAUTHORIZED);
        }
        return userId;
    }
}


