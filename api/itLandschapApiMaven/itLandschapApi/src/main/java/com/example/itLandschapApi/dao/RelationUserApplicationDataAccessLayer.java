package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.Beheerder;
import com.example.itLandschapApi.model.RelationUserApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository("user-application")
public class RelationUserApplicationDataAccessLayer implements RelationUserApplicationDao{

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public RelationUserApplicationDataAccessLayer(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void insertRelationUserApplication(RelationUserApplication relationUserApplication) {
        String sql = "INSERT INTO user_application_relation (user_id, application_id)  VALUES (?, ?)";
        jdbcTemplate.update(sql, relationUserApplication.getUserId(), relationUserApplication.getAppId());
    }

    public boolean checkIfAppBelongsToUser(RelationUserApplication relationUserApplication) {
        String sql = "SELECT * FROM user_application_relation WHERE application_id = ? AND user_id = ? ";

        List<RelationUserApplication> relationList = jdbcTemplate.query(sql, new Object[]{relationUserApplication.getAppId(), relationUserApplication.getUserId()},
                (resultSet, i) -> {
            UUID userId = UUID.fromString(resultSet.getString("user_id"));
            Integer appId = resultSet.getInt("application_id");
            return new RelationUserApplication(userId, appId);
        });
      return relationList.size() > 0;
    }

    @Override
    public void deleteRelationUserApplication(RelationUserApplication relationUserApplication) {
        String sql = "DELETE FROM user_application_relation WHERE user_id = ? AND application_id = ? ";
        jdbcTemplate.update(sql, relationUserApplication.getUserId(), relationUserApplication.getAppId());
    }
}
