package com.example.itLandschapApi.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RelationApplicationBeheerder {


    private final Integer applicationId;
    private final Integer beheerderId;



    public RelationApplicationBeheerder(@JsonProperty("application_id") Integer applicationId,
                                 @JsonProperty("beheerder_id") Integer beheerderId
    ){
        this.applicationId = applicationId;
        this.beheerderId = beheerderId;
    }


    public Integer getApplicationId() {
        return applicationId;
    }

    public Integer getBeheerderId(){ return beheerderId; };

}
