package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.Application;
import com.example.itLandschapApi.model.ProductOwner;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProductOwnerDao {


    int insertProductOwner(ProductOwner productOwner);


    List<ProductOwner> selectAllProductOwners(UUID userId);

    Optional<ProductOwner> selectProductOwnerById(Integer id);
    Optional<ProductOwner> selectProductOwnerByName(String name, UUID userId);


    int deleteProductOwnerById(Integer id);

    int updateProductOwnerById(Integer id, ProductOwner productOwner);
}
