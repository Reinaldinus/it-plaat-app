package com.example.itLandschapApi.api;

import com.example.itLandschapApi.dto.CustomRepetitionException;
import com.example.itLandschapApi.dto.CustomUnauthenticatedRuntimeException;
import com.example.itLandschapApi.model.Beheerder;
import com.example.itLandschapApi.security.JwtTokenProvider;
import com.example.itLandschapApi.service.BeheerderService;
import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/api/v1/beheerder")
@RestController
public class BeheerderController {
    private final BeheerderService beheerderService;
    private final JwtTokenProvider jwtTokenProvider;

    @Autowired
    public BeheerderController (BeheerderService beheerderService, JwtTokenProvider jwtTokenProvider){
        this.beheerderService = beheerderService;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @ExceptionHandler(CustomUnauthenticatedRuntimeException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public Error handleCustomRuntimeException(CustomUnauthenticatedRuntimeException ex) {
        System.out.println(ex.getMessage());
        return new Error(ex.getMessage());
    }

    @ExceptionHandler(CustomRepetitionException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    public Error handleCustomRepetitionException(CustomRepetitionException ex) {
        System.out.println(ex.getMessage());
        return new Error(ex.getMessage());
    }

    @CrossOrigin
    @PostMapping
    public void addBeheerder(HttpServletRequest request, Integer id, @Valid @NonNull @RequestBody Beheerder beheerder){
        String userId = extractUserIdFromToken(request);

        beheerderService.addBeheerder(beheerder, userId);
    }

    @CrossOrigin
    @GetMapping
    public List<Beheerder> getAllBeheerders(HttpServletRequest request) {
        String userId = extractUserIdFromToken(request);

        return beheerderService.getAllBeheerders(userId);
    }

    @CrossOrigin
    @GetMapping(path="{id}")
    public Optional<Beheerder> getBeheerderById(HttpServletRequest request, @PathVariable("id") Integer id) {
        String userId = extractUserIdFromToken(request);

        return beheerderService.getBeheerderById(id, userId);
    }

    @CrossOrigin
    @DeleteMapping(path = "{id}")
    public void deleteBeheerderById(HttpServletRequest request, @PathVariable("id") Integer id){
        String userId = extractUserIdFromToken(request);
        beheerderService.deleteBeheerder(id, userId);
    }

    @CrossOrigin
    @DeleteMapping(path = "force/{id}")
    public void deleteBeheerderAndRelationsBeheerderToDoById(HttpServletRequest request, @PathVariable("id") Integer id){
        String userId = extractUserIdFromToken(request);
        beheerderService.deleteBeheerderAndToDoRelation(id, userId);
    }

    @PutMapping(path = "{id}")
    public void updateBeheerderById(HttpServletRequest request, @PathVariable("id") Integer id, @Valid @NonNull @RequestBody Beheerder beheerderToUpdate){
        String userId = extractUserIdFromToken(request);
        beheerderService.updateBeheerder(id, beheerderToUpdate, userId);
    }

    private String extractUserIdFromToken(HttpServletRequest request) {
        String token = request.getHeader("Authorization").replace("Bearer ", "");
        String userId;
        try {
            userId = jwtTokenProvider.getUserIdFromToken(token);
        } catch (ExpiredJwtException ex) {
            throw new CustomUnauthenticatedRuntimeException("The token is expired, please log in again", HttpStatus.UNAUTHORIZED);

        }
        if (!jwtTokenProvider.validateToken(token, userId)) {
            throw new CustomUnauthenticatedRuntimeException("The provided JWT token is invalid or malformed. ", HttpStatus.UNAUTHORIZED);
        }
        return userId;
    }
}
