package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.Beheerder;
import com.example.itLandschapApi.model.Owner;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OwnerDao {

    int insertOwner(Owner owner);

    Optional<Owner> selectOwnerById(Integer id);
    Optional<Owner> selectOwnerByName(String name, UUID userId);

    List<Owner> selectAllOwners(UUID userId);

    int deleteOwnerById(Integer id);

    int updateOwnerById(Integer id, Owner owner);
}
