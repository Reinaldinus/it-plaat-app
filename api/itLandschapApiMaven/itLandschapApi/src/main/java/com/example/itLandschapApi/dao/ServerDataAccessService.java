package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Repository("servers")
public class ServerDataAccessService implements ServerDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ServerDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertServer(Server server) {
        String sql = "INSERT INTO server (name)  VALUES (?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(con ->{
            PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1,server.getName());

            return ps;
        }, keyHolder);

        Map<String, Object> keys = keyHolder.getKeys();
        Integer generatedId = (Integer) keys.get("id");
        return generatedId;

    }

    @Override
    public List<Server> selectAllServers(UUID userId) {
        final String sql = "SELECT * FROM server JOIN user_server_relation ON " +
                "server.id = user_server_relation.server_id WHERE " +
                "user_server_relation.user_id = ? ";
        return jdbcTemplate.query(sql, new Object[]{userId}, (resultSet, i) -> {
            Integer id = resultSet.getInt("id");
            String name = resultSet.getString("name");


            return new Server(id, name);
        });


    }

    @Override
    public Optional<Server> selectServerById(Integer id) {
        String sql = "SELECT * FROM server WHERE id = ? ";
        //Specifying the values of it's parameter

        List<Server> listServer = jdbcTemplate.query(sql, new Object[]{id}, (resultSet, i) -> {
            Integer serverId = resultSet.getInt("id");
            String name = resultSet.getString("name");


            return new Server(serverId, name);
        });
        return listServer.size() == 0 ? Optional.empty() : Optional.of(listServer.get(0));    }

    @Override
    public Optional<Server> selectServerByName(String name, UUID userId) {
        final String sql = "SELECT * FROM server JOIN user_server_relation ON " +
                "server.id = user_server_relation.server_id WHERE " +
                "user_server_relation.user_id = ? AND server.name = ? ";
        List<Server> listServer = jdbcTemplate.query(sql, new Object[]{userId, name}, (resultSet, i) -> {
            Integer serverId = resultSet.getInt("id");
            return new Server(serverId, name);
        });
        return listServer.size() == 0 ? Optional.empty() : Optional.of(listServer.get(0));
    }

    @Override
    public int deleteServerById(Integer id) {
        String sql = "DELETE FROM server WHERE id = ? ";
        jdbcTemplate.update(sql, id);
        return 0;

    }

    @Override
    public int updateServerById(Integer id, Server server) {
        String sql = "UPDATE server SET name = ? WHERE id = ?;";
        jdbcTemplate.update(sql, server.getName(), id);
        return 0;
    }
}
