package com.example.itLandschapApi.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Date;

public class ToDo {

    private final Integer id;
    private final String title;
    private final String comment;
    private final Date deadline;
    private boolean isDone;


    public ToDo(@JsonProperty("id") Integer id,
                @JsonProperty("title") String title,
                @JsonProperty("comment") String comment,
                @JsonProperty("deadline") Date deadline

    ) {
        this.id = id;
        this.title = title;
        this.comment = comment;
        this.deadline = deadline;
        this.isDone = false;
    }

    public ToDo(
            @JsonProperty("title") String title,
            @JsonProperty("comment") String comment,
            @JsonProperty("deadline") Date deadline

    ) {
        this.id = null;
        this.title = title;
        this.comment = comment;
        this.deadline = deadline;
        this.isDone = false;
    }


    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    ;

    public String getComment() {
        return comment;
    }

    ;

    public Date getDeadline() {
        return deadline;
    }

    public void completeTask() {
        this.isDone = true;
    }

    public boolean isItDone() {
        return isDone;
    }

}
