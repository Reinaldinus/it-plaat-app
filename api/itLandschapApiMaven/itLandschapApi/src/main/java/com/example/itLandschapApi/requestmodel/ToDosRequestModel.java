package com.example.itLandschapApi.requestmodel;

import com.example.itLandschapApi.model.Beheerder;
import com.example.itLandschapApi.model.ToDo;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Date;
import java.util.List;

public class ToDosRequestModel {
    private final Integer id;
    private final String title;
    private final String comment;
    private final Date deadline;
    private boolean isDone;
    private final List<Beheerder> beheerders;


    public ToDosRequestModel(@JsonProperty("id") Integer id,
                             @JsonProperty("title") String title,
                             @JsonProperty("comment") String comment,
                             @JsonProperty("deadline") Date deadline,
                             @JsonProperty("beheerders") List<Beheerder> beheerders

    ) {
        this.id = id;
        this.title = title;
        this.comment = comment;
        this.deadline = deadline;
        this.isDone = isDone;
        this.beheerders = beheerders;
    }


    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }



    public String getComment() {
        return comment;
    }



    public Date getDeadline() {
        return deadline;
    }



    public boolean getIsDone() {
        return isDone;
    }

    ;
    public List<Beheerder> getBeheerders() {
        return beheerders;
    }

    public ToDo toToDo(){
        return new ToDo(
                getTitle(),
                getComment(),
                getDeadline()
        );
    }

}
