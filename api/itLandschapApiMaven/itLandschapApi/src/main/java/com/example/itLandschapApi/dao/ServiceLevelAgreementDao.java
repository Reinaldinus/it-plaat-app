package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.Beheerder;
import com.example.itLandschapApi.model.Server;
import com.example.itLandschapApi.model.ServiceLevelAgreement;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ServiceLevelAgreementDao {

    int insertServiceLevelAgreement(ServiceLevelAgreement serviceLevelAgreement);

    Optional<ServiceLevelAgreement> selectServiceLevelAgreementById(Integer id);
    Optional<ServiceLevelAgreement> selectServiceLevelAgreementByName(String name, UUID userId);

    List<ServiceLevelAgreement> selectAllServiceLevelAgreements(UUID userId);

    int deleteServiceLevelAgreementById(Integer id);

    int updateServiceLevelAgreementById(Integer id, ServiceLevelAgreement serviceLevelAgreement);
}
