package com.example.itLandschapApi.dao;

import com.example.itLandschapApi.model.RelationUserSLA;
import com.example.itLandschapApi.model.RelationUserServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("user-sla")
public class RelationUserSLADataAccessService implements RelationUserSLADao {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public RelationUserSLADataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void insertRelationUserSLA(RelationUserSLA relationUserSLA) {
        String sql = "INSERT INTO user_sla_relation (user_id, sla_id)  VALUES (?, ?)";
        jdbcTemplate.update(sql, relationUserSLA.getUserId(), relationUserSLA.getSlaId());
    }

    @Override
    public boolean checkIfSLABelongsToUser(RelationUserSLA relationUserSLA) {
        String sql = "SELECT * FROM user_sla_relation WHERE sla_id = ? AND user_id = ? ";

        List<RelationUserSLA> relationList = jdbcTemplate.query(
                sql,
                new Object[]{relationUserSLA.getSlaId(),
                        relationUserSLA.getUserId()},
                (resultSet, i) -> {
                    UUID userId = UUID.fromString(resultSet.getString("user_id"));
                    Integer slaId = resultSet.getInt("sla_id");
                    return new RelationUserSLA(userId, slaId);
                });
        return relationList.size() > 0;    }

    @Override
    public void deleteRelationUserSLA(RelationUserSLA relationUserSLA) {
        String sql = "DELETE FROM user_sla_relation WHERE user_id = ? AND sla_id = ? ";
        jdbcTemplate.update(sql, relationUserSLA.getUserId(), relationUserSLA.getSlaId());
    }
}
