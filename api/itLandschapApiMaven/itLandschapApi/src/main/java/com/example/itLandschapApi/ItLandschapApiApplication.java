package com.example.itLandschapApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItLandschapApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItLandschapApiApplication.class, args);
	}

}
